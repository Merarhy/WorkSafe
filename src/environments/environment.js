"use strict";
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
exports.__esModule = true;
exports.environment = {
    production: false,
    hmr: false,
    //apiUrl: 'http://dev-api-sdr.future.com.mx/v1/',
    //domainUrl: 'http://dev-sdr.future.com.mx',
    apiUrl: 'http://localhost:3000/v1/',
    //socketUrl: 'http://localhost:3000',
    domainUrl: 'http://localhost:4200/'
};
