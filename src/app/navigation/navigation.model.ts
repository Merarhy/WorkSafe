import { FuseNavigationModelInterface } from '../core/components/navigation/navigation.model';

export class FuseNavigationModel implements FuseNavigationModelInterface
{
    public model: any[];

    constructor()
    {
        this.model = [
          
            {
                'id'   : 'dashboard',
                'title': 'Dashboard',
                'type' : 'item',
                'icon' : 'dashboard',
                'url'  : 'admin',
               // 'url'  : 'access',
            },
            
           
            {
                'id'   : 'kiosko',
                'title': 'Maps',
                'type' : 'item',
                'icon' : 'map',
                'url'  : 'kiosko'
            }

        
        ];
    }
}
