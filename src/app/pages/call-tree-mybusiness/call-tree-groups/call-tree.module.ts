import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { NewGroupFormDialogComponent } from './new-group-form/new-group-form.component';
import { RouterModule, Routes} from '@angular/router';
import { MainSdrCallTreeComponent } from '../main-call-tree.component';
import { SdrNotificacionesComponent } from './notificaciones/notificaciones.component';


@NgModule({
    declarations: [
      MainSdrCallTreeComponent,
      SdrNotificacionesComponent,
      NewGroupFormDialogComponent,
    ],
    imports: [
     
        SharedModule,
        RouterModule
    ],
    exports: [],

    entryComponents: [NewGroupFormDialogComponent],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class CallTreeModules
{}