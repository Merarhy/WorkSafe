import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Group } from '../../group.model';
import { LoginService } from '../../../../pages/login/login.service';
import { environment } from '../../../../../environments/environment';

@Component({
    selector     : 'fuse-new-group-form-dialog',
    templateUrl  : './new-group-form.component.html',
    styleUrls    : ['./new-group-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class NewGroupFormDialogComponent  implements OnInit
{


    event: CalendarEvent;
    dialogTitle: string;
    contactForm: FormGroup;
    action: string;
    contact: Group;

    constructor(
        public dialogRef: MatDialogRef<NewGroupFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
    )
    {

        this.action = data.action;
        
        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Editar Grupo';
            this.contact = data.contact;
        }
        else
        {
            this.dialogTitle = 'Nuevo Grupo';
            this.contact  = new Group({});


        }
        this.contactForm = this.createContactForm();
    }

    ngOnInit()
    {
    }

    createContactForm()
    {
        
        return this.formBuilder.group({
            name    : [this.contact.name],
            description: [this.contact.description],
            notifications  : [this.contact.notifications]
        });
    }

    addExtradata(){
        if ( !this.contact.notifications ) {
            this.contact.notifications = {};
        }
        this.contact.notifications.push({});
    
    }

    tryDeleteGroup( groupdata ) {
        if ( confirm('¿Estás seguro de borrar el grupo?') ){
            this.dialogRef.close(['delete', groupdata ]);
        }
    }

    valideVoz(){
        if (this.action === 'edit'){
            this.contact.notifications.recording = false
        }else{
            this.contact.notifications.recording = false
        }
    }

    valideVoz2(){
        if (this.action === 'edit'){
            this.contact.notifications.call = false
        }else{
            this.contact.notifications.call = false
        }
    }
        
}
