import { NotificationsService } from '../../call-tree.notifications.service';
import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatTableDataSource , MatPaginator, MatSort} from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../core/components/confirm-dialog/confirm-dialog.component';
import { MatSnackBar } from '@angular/material';
import * as _ from 'lodash';
import * as moment from 'moment';
import { fuseAnimations } from '../../../../core/animations';
import { FormControl, FormGroup } from '@angular/forms';
import { NavigationEnd, NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { LoginService } from '../../../../pages/login/login.service';
import { CallTreeService } from '../../call-tree.service';
import { MyUsersService } from '../../../my-business/my-users.service';
import { Group } from '../../group.model';


@Component({
  selector: 'fuse-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.scss'],
  animations: fuseAnimations,
  providers: [ MyUsersService, CallTreeService, NotificationsService]
})

export class SdrNotificacionesComponent implements OnInit {


  avatarPath: String = environment.apiUrl + 'avatars/';
  users: any[];
  show: Boolean = true;
  dialogRef: any;

  notif_sms: Boolean;
  notif_emai: Boolean;
  notif_audio: Boolean;

  group: any;
  action_mennu: Boolean = false;  
  errorMessage: String = '';

  group_id: String;
  url_fileserve: any;

  current_email: Group = new Group({
    configemail:  {
      head: '',
      message: '',
      response: {},
    }

  });

  current_sms: Group =  new Group({
    configsms:  {
      message_sms: '',
      response_sms: {},
    }

  });


  selectedFiles: FileList;
  

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private usersService: MyUsersService,
    private callTreeService: CallTreeService,
    private loginService: LoginService,
    private notificationsService: NotificationsService
  ) { 
    this.route.params.subscribe(
      params => {
          this.group_id = params['id'];          
      });
      
  }

  ngOnInit() {
    

    this._loadGroup();
    
  }

  _loadGroup(){
    this.callTreeService.getGroup(this.group_id).subscribe(
      group => {
        this.group = group; 
        
        this.usersService.getUserBusinnessById(this.group._business).subscribe(
          users => {
            this.users = users;   
          },
          e => { alert('Error loading business data'); }
        );

      
        if (this.group.configemail){
          this.current_email.configemail.head = this.group.configemail.head;
          this.current_email.configemail.message = this.group.configemail.message;
          this.current_email.configemail.response.option1 = this.group.configemail.response.option1;
          this.current_email.configemail.response.option2 = this.group.configemail.response.option2;
          this.current_email.configemail.response.option3 = this.group.configemail.response.option3;
        }
        if ( this.group.configsms ){
          this.current_sms.configsms.message_sms = this.group.configsms.message_sms;
          this.current_sms.configsms.response_sms.option_1 = this.group.configsms.response_sms.option_1;
          this.current_sms.configsms.response_sms.option_2 = this.group.configsms.response_sms.option_2;
          this.current_sms.configsms.response_sms.option_3 = this.group.configsms.response_sms.option_3;
        }
        if (this.group.audio){
              this.Url();
        }else{
          this.url_fileserve = '';
        }
      },
      e => { this.errorMessage = e; }  
    );
  }


  selectedUsers() {
    if ( !this.group ) {
      return [];
    }
    return this.users.filter(el => {
      return this.group.users.indexOf( el._id ) !== -1;
    });
  }

  unselectedUsers() {  
    if ( !this.group ) {
      return [];
    }   
    return this.users.filter(el => {
      return this.group.users.indexOf( el._id ) === -1;
    });
  }

  selectUser(user_id: String) {
    if (this.group_id &&  user_id){
      this.callTreeService.addContact(this.group_id, user_id).subscribe(
        res => {
          if ( res && res.nModified ) {
            this.group.users.push( user_id );
          } else {
            alert('Error adding user');
          }
        },
        e => { alert('Error adding users'); }
      );
    }
  }
  

  unselectUser(user_id: String) {
    this.callTreeService.removeContact(this.group_id, user_id).subscribe(
      res => {
        if (res) {
          this.group.users.splice(this.group.users.indexOf(user_id), 1);
        } else {
          alert('Error adding users');
        }
      },
      e => { alert('Error adding user'); }
    );
  }

  _saveNotifictionEmail(){
    this.notificationsService.addEmail(this.group_id, this.current_email).subscribe(
      (res) => {
        if (res.message){
          this.snackBar.open(res.message, 'Cerrar', {
            duration: 3000
          });
        }else{
          if (this.current_email){
            this.snackBar.open('Se a creado la configuración de Email', 'Cerrar', {
              duration: 3000
            });
          } else{
              this.snackBar.open('Ha ocurrido un error al crear el Email', 'Cerrar', {
              duration: 3000
            });
          } 
        }

      }
    );

  }

  _saveNotifictionSMS(){     
    this.notificationsService.addSMS(this.group_id, this.current_sms).subscribe(
      sms => { 
        if (sms){ 
          this.snackBar.open('Se a creado la configuración de SMS', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.snackBar.open('Ha ocurrido un error al crear el SMS', 'Cerrar', {
            duration: 3000
          });
        }
      },
      e => { this.errorMessage = e; }
    );

  }
 
  Url(){
    this.url_fileserve = this.notificationsService.fileServeUrl(this.group_id);   
  }
  
 

  detectAudio(event) {
    this.selectedFiles = event.target.files;
  }
  
  uploadAudio() {
    const file = this.selectedFiles.item(0);
    const req = ( file.size <= 14000000 ) ? true : false; 
    const req2 = ( file.type === 'audio/wav' ) ? true :  false;
    if (req  && req2 ){
      this.show = false;  
      this.notificationsService.addAuido(this.group_id, file).subscribe(
        audio   => {  
          // this.url_fileserve = this.notificationsService.fileServeUrl(audio.data.audio.file); 
          this.show = true;
          this.snackBar.open('Se a cargado el audio correctamente', 'Cerrar', {
            duration: 3000
          });
        },
        err  => { 
          this.snackBar.open('Error al cargar el audio', 'Cerrar', {
            duration: 3000
          });
        }
      );    
    }else{
      alert('El audio solo permite 14 MB y formato wav');
    }
   

  }
  



}
