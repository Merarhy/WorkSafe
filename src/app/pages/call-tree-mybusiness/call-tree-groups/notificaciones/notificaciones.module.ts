import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { SdrNotificacionesComponent } from './notificaciones.component';

@NgModule({
    declarations: [
        SdrNotificacionesComponent
    ],
    imports     : [
     
        SharedModule
    ],
    exports     : [
        SdrNotificacionesComponent
    ]
})

export class NotificacionesModule
{

}