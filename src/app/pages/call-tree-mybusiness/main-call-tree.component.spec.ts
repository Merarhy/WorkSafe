import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCallTreeComponent } from './main-call-tree.component';

describe('MainCallTreeComponent', () => {
  let component: MainCallTreeComponent;
  let fixture: ComponentFixture<MainCallTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainCallTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCallTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
