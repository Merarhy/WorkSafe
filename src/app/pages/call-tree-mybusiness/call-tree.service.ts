import { Injectable } from '@angular/core';
import { Group } from './group.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';
import { GroupedObservable } from 'rxjs/operator/groupBy';

@Injectable()
export class CallTreeService {

    requestOptions: RequestOptions;

    constructor(private http: Http, loginService: LoginService){
        const headers = new Headers({'Authorization': loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    
    getAll(id: String): Observable<Group[]> {
        return this.http.get(`${environment.apiUrl}calltree/all/groups/${id}`, this.requestOptions)
            .map( mapGroups );
    }

 
    getGroup( _id: String ): Observable<any> {
        return this.http.get(`${environment.apiUrl}calltree/groups/${_id}`, this.requestOptions)
            .map( extractData );
    }

    saveOrUpdate(businessId: string, group: Group): Observable<any> {
        if (group._id)
            return this.http.patch(`${environment.apiUrl}calltree/${group._business}/groups/${group._id}`, group, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}calltree/${businessId}/groups`, group, this.requestOptions).map( extractData );
    }
   
    delete(group: Group): Observable<any> {
        if(group._id)
            return this.http.delete(`${environment.apiUrl}/calltree/groups/${group._id}`, this.requestOptions).map( extractData );
    }

    addContact(group_id: String, user_id: String): Observable<any> {
        return this.http.post(`${environment.apiUrl}calltree/groups/${group_id}/contact/${user_id}`, {}, this.requestOptions).map( extractData );
    }

    removeContact(group_id: String, user_id: String): Observable<any> {
        return this.http.delete(`${environment.apiUrl}calltree/groups/${group_id}/contact/${user_id}`, this.requestOptions).map( extractData );
    }
}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toGroup);
    } catch (error) {}
    return body || { };
}


function mapGroups(response: Response): Group[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toGroup);
}


function toGroup(r: any): Group{
    let group = <Group>({
        _id: r._id,
        name: r.name,
        description: r.description,
        _business: r._business,
        notifications: r.notifications,
        users: r.users,
    });
    return group;
}
