import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { MainSdrCallTreeComponent } from './main-call-tree.component';
import { NewGroupFormDialogComponent } from '../call-tree-business/call-tree-groups/new-group-form/new-group-form.component';


@NgModule({
    declarations: [
      MainSdrCallTreeComponent,
      NewGroupFormDialogComponent

    ],
    imports     : [
     
        SharedModule,
        RouterModule
    ],
    exports     : [
      MainSdrCallTreeComponent,
    ],

})

export class CallTreeModule
{

}