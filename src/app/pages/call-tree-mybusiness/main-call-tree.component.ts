import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { CallTreeSdrComponent } from './call-tree-groups/call-tree.component';

@Component({
  selector: 'fuse-main-call-tree',
  templateUrl: './main-call-tree.component.html',
  styleUrls: ['./main-call-tree.component.scss']
})
export class MainSdrCallTreeComponent implements OnInit {

  constructor(
    private loginService: LoginService,
  ) { }

  ngOnInit() {
  }

}
