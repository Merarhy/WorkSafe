import { Injectable } from '@angular/core';
import { Role } from './roles.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class RolesService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    getAll() : Observable<Role[]> {
        return this.http.get(`${environment.apiUrl}roles`, this.requestOptions)
            .map( mapRoles );
    }

    saveOrUpdate(role: Role) : Observable<any> {
        if(role._id)
            return this.http.patch(`${environment.apiUrl}roles/${role._id}`, role, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}roles`, role, this.requestOptions).map( extractData );
    }

    delete(role: Role) : Observable<any> {
        if(role._id)
            return this.http.delete(`${environment.apiUrl}roles/${role._id}`, this.requestOptions).map( extractData );
    }
    
}

function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toRole);
    } catch (error) {}
    return body || { };
}


function mapRoles(response:Response): Role[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toRole);
}


function toRole(r:any): Role{
    let role = <Role>({
        _id: r._id,
        name: r.name,
        slug: r.slug,
        permissions: r.permissions,
    });
    return role;
}