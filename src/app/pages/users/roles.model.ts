export class Role {
    _id: string;
    name: string;
    slug: string;
    permissions: string[];
  
    constructor(model: any = null) {
      if(model._id)
        this._id = model._id;
      this.name = model.name;
      this.slug = model.slug;
      this.permissions = model.permissions;
    }
  }
  