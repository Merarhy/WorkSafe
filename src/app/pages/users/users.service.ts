import { Injectable } from '@angular/core';
import { User } from './users.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class UsersService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    getAll() : Observable<User[]> {
        return this.http.get(`${environment.apiUrl}users`, this.requestOptions)
            .map( mapUsers );
    }
    
    saveOrUpdate(user: User) : Observable<any> {
        if(user._id)
            return this.http.patch(`${environment.apiUrl}users/${user._id}`, user, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}users`, user, this.requestOptions).map( extractData );
    }

    delete(user: User) : Observable<any> {
        if(user._id)
            return this.http.delete(`${environment.apiUrl}users/${user._id}`, this.requestOptions).map( extractData );
    }

    searchUser(email: String): Observable<User[]> {
        return this.http.get(`${environment.apiUrl}users/search/${email}`, this.requestOptions)
            .map( mapUsers );
    }

    
}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toUser);
    } catch (error) {}
    return body || { };
}


function mapUsers(response:Response): User[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toUser);
}


function toUser(r:any): User{
    let user = <User>({
        _id: r._id,
        firstname: r.firstname,
        lastname: r.lastname,
        email: r.email,
        extradata: r.extradata,
        role: r.role,
        avatar: r.avatar,
        created_at: r.created_at,
    });
    return user;
}