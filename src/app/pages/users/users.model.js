"use strict";
exports.__esModule = true;
var User = /** @class */ (function () {
    function User(model) {
        if (model === void 0) { model = null; }
        if (model._id)
            this._id = model._id;
        this.firstname = model.firstname;
        this.lastname = model.lastname;
        this.email = model.email;
        this.role = model.role;
        if (model.extradata)
            this.extradata = model.extradata;
        if (model.avatar)
            this.avatar = model.avatar;
    }
    return User;
}());
exports.User = User;
