import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { IdToRolePipeModule } from '../../../pipes/idtorole.module';
import { RolesUsersComponent } from './../roles-users/roles-users.component';


@NgModule({
    declarations: [
         RolesUsersComponent,
    ],
    imports     : [
        IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
         RolesUsersComponent
    ]
})

export class RolesUsersModule
{

}
