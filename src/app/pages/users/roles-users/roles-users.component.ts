import {Component, OnInit, ViewChild, Pipe, PipeTransform} from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import {Role} from '../roles.model';
import {MatSnackBar} from '@angular/material';
import { fuseAnimations } from '../../../core/animations';
import {RolesService} from '../roles.service';
import {Permission} from '../permissions.model';
import {PermissionsService} from '../permissions.service';
import {LoginService} from '../../../pages/login/login.service';
import * as _ from 'lodash';

@Component({
  selector: 'fuse-roles-users',
  templateUrl: './roles-users.component.html',
  styleUrls: ['./roles-users.component.scss'],
  animations: fuseAnimations,
  providers: [RolesService, PermissionsService]
})
export class RolesUsersComponent implements OnInit {
  
  permissions: any[];
  roles: any[];
  errorMessage: String = '';

  current_role: Role;
  constructor(
    private rolesService: RolesService,
    private permissionsService: PermissionsService,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit() {

    this.permissionsService.getAll().subscribe(
      per => { this.permissions = per; },
      error => { this.errorMessage = error; }
    );

    this.rolesService.getAll().subscribe(
      roles => { this.roles = roles; this.setFirstRol(); },
      error => { this.errorMessage = error; }
    );
  }


  sendUsers(){
    this.router.navigate(['users']);
  }
  

  /**
   * 
   *  Roles functions
   * 
   */
  setFirstRol(){
    if ( this.roles.length > 0 ) {
      this.current_role = _.cloneDeep(this.roles[0]);
    }
    else {
      this.newRol();
    }
  }

  newRol(){
    this.current_role = new Role({
      name: '',
      permissions: []
    });
  }

  addOrRemovePermission(checked, idRole){
    // If checked
    if ( checked ){
      if ( this.current_role['permissions'].indexOf(idRole) < 0 ) {
        this.current_role['permissions'].push(idRole);
      }
    } else {
      this.current_role['permissions'].splice(this.current_role['permissions'].indexOf(idRole), 1);
    }
  }

  saveRole(){
    this.rolesService
          .saveOrUpdate(this.current_role)
          .subscribe(
            (res) => {
              if ( res.message ){
                this.snackBar.open(res.message, 'Cerrar', {
                  duration: 3000
                });
              } else {
                if ( !this.roles.find(el => el._id === res._id ) ){
                  this.roles.push(res);
                } else {
                  this.roles[this.roles.map( el => el._id ).indexOf(this.current_role._id)] = res;
                }
                this.setActiveRole(res);
                this.snackBar.open('El rol ha sido guardado', 'Cerrar', {
                  duration: 3000
                });
              }
            }
          );
  }

  deleteRol(){
    if ( confirm('¿Estás seguro de borrar el rol?') ){
      this.rolesService
          .delete(this.current_role)
          .subscribe(
            (res) => {
              if ( res.message ){
                this.snackBar.open(res.message, 'Cerrar', {
                  duration: 3000
                });
              } else {
                this.roles.splice(this.roles.map( el => el._id ).indexOf(this.current_role._id), 1);
                this.setFirstRol();
                this.snackBar.open('El rol ha sido eliminado', 'Cerrar', {
                  duration: 3000
                });
              }
            }
          );
    }
  }

  setActiveRole(role) {
    this.current_role = _.cloneDeep(role);
  }



}
