export class User {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  extradata: any[];
  role: string;
  avatar: string;
  created_at: Date;

  constructor(model: any = null) {
    if(model._id)
    
    this._id = model._id;
    this.firstname = model.firstname;
    this.lastname = model.lastname;
    this.email = model.email;
    this.role = model.role;
    this.created_at = model.created_at;
    if(model.extradata)
      this.extradata = model.extradata;
    if(model.avatar)
      this.avatar = model.avatar;
  }
}