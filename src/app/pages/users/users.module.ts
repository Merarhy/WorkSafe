import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
import { UsersComponent } from './users.component';
import { IdToRolePipeModule } from '../../pipes/idtorole.module';
import { FuseContactsContactFormDialogComponent } from './contact-form/contact-form.component';


@NgModule({
    declarations: [
         UsersComponent,
         FuseContactsContactFormDialogComponent
    ],
    imports     : [
        IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
         UsersComponent
    ],
    entryComponents: [FuseContactsContactFormDialogComponent]
})

export class UsersModule
{

}
