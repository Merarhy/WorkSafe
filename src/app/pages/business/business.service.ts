import { Injectable } from '@angular/core';
import { Business } from './business.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class BusinessService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    getAll(): Observable<Business[]> {
        return this.http.get(`${environment.apiUrl}business`, this.requestOptions)
            .map( mapBusiness );
    }
    
    getBusiness( _id: String ): Observable<any> {
        return this.http.get(`${environment.apiUrl}business/${_id}`, this.requestOptions)
            .map( extractData );
    }
    
    saveOrUpdate(business: Business) : Observable<any> {
        if(business._id)
            return this.http.patch(`${environment.apiUrl}business/${business._id}`, business, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}business`, business, this.requestOptions).map( extractData );
    }

    
    delete(business: Business) : Observable<any> {
        if(business._id)
            return this.http.delete(`${environment.apiUrl}business/${business._id}`, this.requestOptions).map( extractData );
    }
    
    
}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toBusiness);
    } catch (error) {}
    return body || { };
}


function mapBusiness(response:Response): Business[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toBusiness);
}


function toBusiness(r:any): Business{
    let business = <Business>({
        _id: r._id,
        name: r.name,
        acronimo: r.acronimo,
        calle: r.calle,
        colonia: r.colonia,
        municipio: r.municipio,
        estado: r.estado,
        pais: r.pais,
        cp: r.cp,
        tel: r.tel,
        logo: r.logo,
        admin: r.admin,
        active: r.active,
        extradata: r.extradata,
        created_at: r.created_at
    });
    return business;
}