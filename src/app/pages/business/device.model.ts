export class Device {
  _id: string;
  _devicetype: any;
  marca: string;
  modelo: string;
  serie: string;
  cant_rack: number;
  cant_consumo: number;
  redundante: boolean;
  soporte_vigente: boolean;
  salesforce_proyecto_id: string;
  _business: any;
  status: string;
  historical: any;
  created_at: Date;

  constructor(model: any = null) {
    for (let key in model) {
        if ( model[key] ) {
            this[key] = model[key];
        }
    }
  }
}
