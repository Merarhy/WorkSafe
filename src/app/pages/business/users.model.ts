export class User {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  active: boolean;
  business: any;
  avatar: string;
  role: string;
  externo: Boolean;
  devices: any[];
  new_password: string;
  extradata: any;
  current_password: string;

  constructor(model: any = null) {
    if ( model._id) {
      this._id = model._id;
    }
    this.firstname = model.firstname;
    this.lastname = model.lastname;
    this.email = model.email;
    this.phone = model.phone;
    this.active = model.active;
    this.business = model.business;
    this.avatar = model.avatar;
    this.role = model.role;
    this.externo = model.externo;
    this.devices = model.devices;
    this.new_password =  model.new_password;
    this.extradata = model.extradata;
    this.current_password = model.current_password;
  }
}
