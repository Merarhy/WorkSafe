import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessRolesUsersComponent } from './business-roles-users.component';

describe('BusinessRolesUsersComponent', () => {
  let component: BusinessRolesUsersComponent;
  let fixture: ComponentFixture<BusinessRolesUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessRolesUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessRolesUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
