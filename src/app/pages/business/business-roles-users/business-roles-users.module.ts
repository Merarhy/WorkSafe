import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { IdToRolePipeModule } from '../../../pipes/idtorole.module';
import { BusinessRolesUsersComponent } from './business-roles-users.component';


@NgModule({
    declarations: [
         BusinessRolesUsersComponent,
    ],
    imports     : [
        IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
         BusinessRolesUsersComponent
    ]
})

export class BusinessRolesUsersModule
{

}
