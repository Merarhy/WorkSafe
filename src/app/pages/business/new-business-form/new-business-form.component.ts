import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Business } from '../business.model';
import { LoginService } from '../../../pages/login/login.service';
import { environment } from '../../../../environments/environment';

@Component({
    selector     : 'fuse-new-business-form-dialog',
    templateUrl  : './new-business-form.component.html',
    styleUrls    : ['./new-business-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class NewBusinessFormDialogComponent implements OnInit
{

    avatarPath: String = environment.apiUrl + 'avatars/';

    event: CalendarEvent;
    dialogTitle: string;
    contactForm: FormGroup;
    action: string;
    contact: Business = new Business({});

    constructor(
        public dialogRef: MatDialogRef<NewBusinessFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
    )
    {
        this.contactForm = this.createContactForm();
    }

    ngOnInit()
    {
    }

    createContactForm()
    {
        
        return this.formBuilder.group({
            name    : [this.contact.name, Validators.required ],
            acronimo: [this.contact.acronimo, Validators.required],
            calle  : [this.contact.calle, Validators.required],
            colonia   : [this.contact.colonia, Validators.required],
            municipio   : [this.contact.municipio, Validators.required],
            estado   : [this.contact.estado, Validators.required],
            pais   : [this.contact.pais, Validators.required],
            cp   : [this.contact.cp, [Validators.required, Validators.maxLength(5), Validators.pattern("/^\d{5}$/ ")]],
            tel   : [this.contact.tel, [Validators.required,Validators.minLength(7), Validators.maxLength(10)]],
            extradata   : [this.contact.extradata]
        });
    }

    addExtradata(){
        if ( !this.contact.extradata ) {
            this.contact.extradata = [];
        }
        this.contact.extradata.push({});
    
    }

    removeExtradata(index){
        this.contact.extradata.splice(index, 1);
    }

}
