import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';

import { BusinessComponent } from './business.component';

import { IdToRolePipeModule } from '../../pipes/idtorole.module';
import { NewBusinessFormDialogComponent } from './new-business-form/new-business-form.component';

@NgModule({
    declarations: [
         BusinessComponent,
         NewBusinessFormDialogComponent
    ],
    imports     : [
       IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
         BusinessComponent
    ],
    entryComponents: [NewBusinessFormDialogComponent]
})

export class BusinessModule
{

}
