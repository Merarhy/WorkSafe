import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileManagerBusinessComponent } from './file-manager-business.component';

describe('FileManagerBusinessComponent', () => {
  let component: FileManagerBusinessComponent;
  let fixture: ComponentFixture<FileManagerBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileManagerBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
