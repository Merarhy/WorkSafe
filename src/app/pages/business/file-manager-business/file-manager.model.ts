export class MyFile {
  _id: string;
  name: string;
  file: string;
  type: string;
  owner:string;
  size: string;
  _directory: string;
  _business: string;
  created:string;
  
  
  constructor(model: any = null) {
    if(model._id)
      this._id = model._id;
    this.name = model.name;
    this.file = model.file;
    this.type = model.type;
    this.owner = model.type;
    this.size = model.size;
    this.created = model.created;
    if(model._directory)
    this._directory = model.directory;
    this._business = model._business;
  if(model._business)
    this._business = model._business;
    }
}

