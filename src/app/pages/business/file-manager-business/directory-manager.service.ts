import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { LoginService } from '../../../pages/login/login.service';
import { environment } from '../../../../environments/environment';
import { MyDirectory } from './directory-manager.model';


@Injectable()

export class DirectoryBusinessManagerService
{
    onFilesChanged: BehaviorSubject<any> = new BehaviorSubject({});
    onFileSelected: BehaviorSubject<any> = new BehaviorSubject({});

    requestOptions : RequestOptions;

    constructor(private http : Http, public loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }
    /**
     * The File Manager App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */

    getAll(business_id: string) : Observable<MyDirectory[]> {
        return this.http.get(`${environment.apiUrl}business/${business_id}/directories`, this.requestOptions).map( mapDirectory );
    }

    addDirectory(business_id: string, directory: MyDirectory) : Observable<any>{
        return this.http.post(`${environment.apiUrl}business/${business_id}/directories`, directory ,this.requestOptions).map( extractData );
    }

    removeDirectory(business_id: string, directory_id: string) : Observable<any>{
        return this.http.delete(`${environment.apiUrl}business/${business_id}/directories/${directory_id}`, this.requestOptions).map( extractData );
    }
    
    sdrFavOrUnfavDirectory(business_id: string, directory_id: string, fav: Boolean) : Observable<any>{
        if ( fav ) {
            return this.http.patch(`${environment.apiUrl}business/${business_id}/directories/${directory_id}/sdr_fav`, {}, this.requestOptions).map( extractData );
        } else {
            return this.http.patch(`${environment.apiUrl}business/${business_id}/directories/${directory_id}/sdr_unfav`, {}, this.requestOptions).map( extractData );
        }
    }

}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toDirectory);
    } catch (error) {}
    return body || { };
}


function mapDirectory(response:Response): MyDirectory[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toDirectory);
}


function toDirectory(r:any): MyDirectory{
    let directory = <MyDirectory>({
        _id: r._id,
        name: r.name,
        _parent: r._parent,
        type: r.type,
        owner: r.owner,
        size: r.size,
        created: r.created,
        favorite: r.favorite
      
    });
    return directory;
}


