import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { LoginService } from '../../../pages/login/login.service';
import { environment } from '../../../../environments/environment';
import { MyFile } from './file-manager.model';

@Injectable()
export class FileManagerBusinessService 
{
  

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    /**
     * The File Manager App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */

    getAll(business_id: string) : Observable<MyFile[]> {
        return this.http.get(`${environment.apiUrl}business/${business_id}/files`, this.requestOptions).map( mapFiles );
    }


    fileServeUrl(business_id: string, file:string): string{
        return `${environment.apiUrl}files/${business_id}/${file}`;
    }

    addFile(business_id: string, directory_id: string, file: any) : Observable<any> {    
        var formData = new FormData();
        formData.append("bus_file",  file);
        return this.http.post(`${environment.apiUrl}business/${business_id}/directories/${directory_id}/files/`, formData ,this.requestOptions).map( extractData );

    }

    delete(business_id: string, file_id: string) : Observable<any> {
        return this.http.delete(`${environment.apiUrl}business/${business_id}/files/${file_id}`, this.requestOptions).map( extractData );
    }
    
    
    
}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toFile);
    } catch (error) {}
    return body || { };
}


function mapFiles(response:Response): MyFile[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toFile);
}


function toFile(r:any): MyFile{
    let file = <MyFile>({
        _id: r._id,
        name: r.name,
        file: r.file,
        type: r.type,
        owner: r.owner,
        size: r.size,
        _directory: r._directory,
        _business: r._business,
        created : r.created
    });
    return file;
}


