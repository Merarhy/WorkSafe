import { Component, OnInit, ViewEncapsulation,  ViewChild, Pipe, PipeTransform } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MatTableDataSource , MatPaginator, MatSort } from '@angular/material';
import { NavigationEnd, NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { Business } from '../business.model';
import { BusinessService } from '../business.service';
import { User } from '../users.model';
import { UsersService } from '../users.service';
import { Role } from '../roles.model';
import { RolesService } from '../roles.service';
import { fuseAnimations } from '../../../core/animations';
import { FormControl, FormGroup } from '@angular/forms';
import {LoginService} from '../../../pages/login/login.service';

import { environment } from '../../../../environments/environment';

import * as _ from 'lodash';
import * as moment from 'moment';
import { routeAnimation } from '../../../route.animation';

// Custom libraries
import { TreeComponent } from 'angular-tree-component';
import { resolve } from 'q';


@Component({
  selector: 'fuse-business-details-content',
  templateUrl: './business-details.component.html',
  styleUrls: ['./business-details.component.scss'],
  animations: fuseAnimations,
  providers: [BusinessService, UsersService, RolesService],
  encapsulation: ViewEncapsulation.None,
 
})
export class BusinessDetailsComponent implements OnInit {

  _id: String;
  section: String = 'detalle';

  current_business: Business;


  current_users: User[];
  business_users: { [_id: string]: User[] };
  
  errorMessage: String = '';

  url_fileserve: String = '';

  contentLoading: Boolean = true;

  @ViewChild(TreeComponent)
  private tree: TreeComponent;

  constructor(
    private businessService: BusinessService,
    private usersService: UsersService,
    private rolesService: RolesService,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute
    ) {
        this.route.params.subscribe(
            params => {
                this._id = params['id'];
                this._loadBusiness();
            });
        
    }

  ngOnInit() {
    
  }

  is_disabled() {
    return !this.loginService.validatePermissionTree('business', 'business', 'update');
  } 

  _loadBusiness() {
    this.businessService.getBusiness(this._id).subscribe(
        bus => {
            this.current_business = bus;
        },
        e => {this.errorMessage = e; }
    );
  }

  /**
   * 
   *  Business functions
   * 
   */
  
  addExtradata(){
    if ( !this.current_business.extradata ) {
      this.current_business.extradata = [];
    }
    this.current_business.extradata.push({});
    
  }

  removeExtradata(index){
    this.current_business.extradata.splice(index, 1);
  }

  saveBusiness(){
    this.businessService
    .saveOrUpdate(this.current_business)
    .subscribe(
      (res) => {
        if (res.message){
          // Error, business not stored
          this.snackBar.open(res.message, 'Cerrar', {
            duration: 3000
          });
        } else {
          this.snackBar.open('La empresa ha sido actualizada', 'Cerrar', {
            duration: 3000
          });
        }
      }
    );
  }

  deleteBusiness(){
    if ( confirm('¿Estás seguro? Ésta acción es irreversible')){
      this.businessService
          .delete(this.current_business)
          .subscribe(
            (res) => {
              if (res.message){
                this.snackBar.open(res.message, 'Cerrar', {
                  duration: 3000
                });
              } else {
                this.snackBar.open('La empresa ha sido eliminada', 'Cerrar', {
                  duration: 3000
                });
                this.router.navigate(['business']);
              }
            }
          );
    }
  }

 
}
