import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';

import { BusinessDetailsComponent } from './business-details.component';

import { IdToRolePipeModule } from '../../../pipes/idtorole.module';
import { FileManagerBusinessComponent } from '../file-manager-business/file-manager-business.component';
import { BusinessUsersComponent } from './business-users/business-users.component';
import {  FileSizePipes } from '../../../pipes/filesizepipe/filesizepipe.module';
import { BusinessUserFormDialogComponent } from './business-users/business-user-form/business-user-form.component';

@NgModule({
    declarations: [
         BusinessDetailsComponent,
         FileManagerBusinessComponent,
         BusinessUsersComponent,
         BusinessUserFormDialogComponent
    ],
    imports     : [
       IdToRolePipeModule,
        SharedModule,
        FileSizePipes,
    ],
    exports     : [
         BusinessDetailsComponent,
         BusinessUserFormDialogComponent
    ],
    entryComponents: [BusinessUserFormDialogComponent]
})

export class BusinessDetailsModule
{

}
