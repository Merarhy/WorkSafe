import { Component, OnInit, AfterViewInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { BusinessUserFormDialogComponent } from './business-user-form/business-user-form.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatTableDataSource , MatPaginator, MatSort} from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../../core/components/confirm-dialog/confirm-dialog.component';
import { MatSnackBar } from '@angular/material';
import { User } from '../../users.model';
import { UsersService } from '../../users.service';
import { Role } from '../../roles.model';
import { RolesService } from '../../roles.service';
import * as _ from 'lodash';
import * as moment from 'moment';
import { fuseAnimations } from '../../../../core/animations';
import { FormControl, FormGroup } from '@angular/forms';
import { LoginService } from '../../../../pages/login/login.service';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';


@Component({
  selector: 'fuse-business-users-list',
  templateUrl: './business-users.component.html',
  styleUrls: ['./business-users.component.scss'],
  animations: fuseAnimations,
  providers: [UsersService, RolesService]
})
export class BusinessUsersComponent implements OnInit, AfterViewInit {

  @Input() business: any;

  filterBy: String = '';
  searchInput: FormControl;

  @ViewChild('dialogContent') dialogContent: TemplateRef<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  contacts: any;
  user: any;
  displayedColumns = ['firstname', 'email', 'created'];
  dataSource = new MatTableDataSource();
  selectedContacts: any[];
  checkboxes: {};


  dialogRef: any;

  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
  
  
  roles: any[];
  users: any[];
  
  errorMessage: String = '';

  action: Boolean = false;

  constructor(
    private usersService: UsersService,
    private rolesService: RolesService,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private router: Router,
    public dialog: MatDialog,
    ) {
      this.searchInput = new FormControl('');
    }

  ngOnInit() {

    

    this.rolesService.getAll().subscribe(
      roles => { this.roles = roles; },
      error => { this.errorMessage = error; }
      );
    
    
    this.usersService.getAll(this.business._id).subscribe(
      users => {
        this.users = users;
        this.dataSource.data = users;
        this.dataSource.sort = this.sort;
      },
      e => { this.errorMessage = e; }  
    );

    this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
              this.usersFiltered( searchText );
            });

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  changeFilter(filter)
  {
      this.filterBy = filter;

      this.usersFiltered( this.searchInput.value );
  }

  editContact(contact) {
      this.dialogRef = this.dialog.open(BusinessUserFormDialogComponent, {
          panelClass: 'contact-form-dialog',
          data      : {
              contact: contact,
              roles: this.roles,
              action : 'edit'
          }
      });

      this.dialogRef.afterClosed()
          .subscribe(response => {
              if ( !response )
              {
                  return;
              }
              const actionType: string = response[0];
              const formData: any = response[1];
              switch ( actionType )
              {
                  /**
                   * Save
                   */
                  case 'save':
                      this.saveUser( formData );
                      break;
                  /**
                   * Delete
                   */
                  case 'delete':
                      
                      this.deleteUser( formData );
                      break;

                  case 'restorepass':
                      
                      this.restorePassword( formData );
                      break;
                      
              }
          });
  }

  newContact() {
      this.dialogRef = this.dialog.open(BusinessUserFormDialogComponent, {
          panelClass: 'contact-form-dialog',
          data      : {
              action: 'new',
              roles: this.roles
          }
      });

      this.dialogRef.afterClosed()
          .subscribe((response: any) => {
              if ( !response )
              {
                  return;
              }
              this.saveUser( response );
          });

  }

  


  activeAction(){
    if ( this.action )
    {
      this.action = false; 
    } else {
      this.action = true;
    }
  }
  
  /**
   * 
   *  User functions
   * 
   */

  usersFiltered( searchText: String ){

    this.dataSource.data = this.users;

    if ( this.filterBy && this.filterBy.length ) {
      this.dataSource.data = this.dataSource.data.filter( (el) => {
        return ( el['role'] === this.filterBy );
      });
    }

    if ( searchText && searchText.length ) {
      this.dataSource.data = this.dataSource.data.filter( (el) => {
        return ( (el['firstname'] + ' ' + el['lastname']).toLowerCase().indexOf(searchText.toLowerCase()) !== -1 || el['email'].indexOf(searchText.toLocaleLowerCase()) === 0 );
      });
    }
  }

  saveUser(userdata){
    
    this.usersService
          .saveOrUpdate(this.business._id, userdata)
          .subscribe(
            (res) => {
              if ( res.message ){
                this.snackBar.open(res.message, 'Cerrar', {
                  duration: 3000
                });
              } else {
                if ( !this.users.find(el => el._id === res._id ) ){
                  this.users.push(res);
                } else {
                  this.users[this.users.map( el => el._id ).indexOf(userdata._id)] = res;
                }
                this.dataSource.data = this.users;
                this.usersFiltered( this.searchInput.value );
                this.snackBar.open('El usuario ha sido guardado', 'Cerrar', {
                  duration: 3000
                });
              }
            }
          );
  }

  deleteUser( userdata ){
      this.usersService
          .delete( userdata )
          .subscribe(
            (res) => {
              if ( res.message ){
                this.snackBar.open(res.message, 'Cerrar', {
                  duration: 3000
                });
              } else {
                this.users.splice(this.users.map( el => el._id ).indexOf(userdata._id), 1);
                this.usersFiltered( this.searchInput.value );
                this.snackBar.open('El usuario ha sido eliminado', 'Cerrar', {
                  duration: 3000
                });
              }
            }
          );
  }

  restorePassword( userdata ){
    
    this.usersService
    .resetUserBusinessPassword(this.business._id, userdata._id)
    .subscribe(
      user => {
        
          this.snackBar.open('Notificación enviada', 'Cerrar', {
            duration: 3000
          });
        
      },
      e =>
      {  this.snackBar.open('Error al enviar la solicitud', 'Cerrar', {
          duration: 3000
        }); 
      }
    );

    
  }

}
