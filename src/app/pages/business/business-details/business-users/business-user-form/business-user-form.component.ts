import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../users.model';
import { Role } from '../../../roles.model';
import { LoginService } from '../../../../../pages/login/login.service';
import { environment } from '../../../../../../environments/environment';

@Component({
    selector     : 'fuse-business-user-form-dialog',
    templateUrl  : './business-user-form.component.html',
    styleUrls    : ['./business-user-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class BusinessUserFormDialogComponent implements OnInit
{

    avatarPath: String = environment.apiUrl + 'avatars/';

    event: CalendarEvent;
    dialogTitle: string;
    contactForm: FormGroup;
    action: string;
    contact: User;
    roles: Role[];

    constructor(
        public dialogRef: MatDialogRef<BusinessUserFormDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
    )
    {
        this.roles = data.roles;
        this.action = data.action;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = 'Editar Usuario de Empresa';
            this.contact = data.contact;
        }
        else
        {
            this.dialogTitle = 'Nuevo Usuario de Empresa';
            this.contact = new User({});
        }

        this.contactForm = this.createContactForm();
    }

    ngOnInit()
    {
    }

    createContactForm()
    {
        
        return this.formBuilder.group({
            _id      : [this.contact._id],
            firstname    : [this.contact.firstname, Validators.required],
            lastname: [this.contact.lastname, Validators.required],
            avatar  : [this.contact.avatar],
            email   : [this.contact.email,  [ 
                Validators.required,
                Validators.pattern("[^ @]*@[^ @]*") 
            ]],
            phone   : [this.contact.phone, [Validators.required, Validators.minLength(7), Validators.maxLength(10)]],
            role   : [this.contact.role, Validators.required],
            externo   : [this.contact.externo],
            extradata   : [this.contact.extradata]
        });
    }

    addExtradata(){
        if ( !this.contact.extradata ) {
            this.contact.extradata = [];
        }
        this.contact.extradata.push({});
    
    }

    removeExtradata(index){
        this.contact.extradata.splice(index, 1);
    }

    tryDeleteUser( userdata ) {
        if ( confirm('¿Estás seguro de borrar el usuario?') ){
            this.dialogRef.close(['delete', userdata ]);
        }
    }

    restorePassword( userdata ){
        if (confirm('¿Enviar correo al usuario para restaurar su contraseña?')){
            this.dialogRef.close(['restorepass', userdata ]);
        }
    }
}
