import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { BusinessUsersComponent } from './business-users.component';
import { IdToRolePipeModule } from '../../../../pipes/idtorole.module';
import { BusinessUserFormDialogComponent } from './business-user-form/business-user-form.component';


@NgModule({
    declarations: [
         BusinessUsersComponent,
         BusinessUserFormDialogComponent
    ],
    imports     : [
        IdToRolePipeModule,
        SharedModule,
        BusinessUserFormDialogComponent
    ],
    exports     : [
         BusinessUsersComponent
    ],
    entryComponents: [BusinessUserFormDialogComponent]
})

export class BusinessUsersModule
{

}
