import { Component, OnInit, ViewEncapsulation, TemplateRef, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { NewBusinessFormDialogComponent } from './new-business-form/new-business-form.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { MatTableDataSource , MatPaginator, MatSort } from '@angular/material';
import { NavigationEnd, NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { Business } from './business.model';
import { BusinessService } from './business.service';
import { User } from './users.model';
import { UsersService } from './users.service';
import { Role } from './roles.model';
import { RolesService } from './roles.service';
import { fuseAnimations } from './../../core/animations';
import { FormControl, FormGroup } from '@angular/forms';

import {LoginService} from '../../pages/login/login.service';

import { environment } from '../../../environments/environment';

import * as _ from 'lodash';
import * as moment from 'moment';
import { routeAnimation } from '../../route.animation';

// Custom libraries
import { TreeComponent } from 'angular-tree-component';
import { resolve } from 'q';


@Component({
  selector: 'fuse-bussiness-content',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.scss'],
  animations: fuseAnimations,
  providers: [BusinessService, UsersService, RolesService],
  encapsulation: ViewEncapsulation.None,
 
})
export class BusinessComponent implements OnInit {

  @ViewChild('newBusinessdialogContent') newBusinessdialogContent: TemplateRef<any>;
  @ViewChild( MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchInput: FormControl;
  dataSource = new MatTableDataSource();
  displayedColumns = ['acronimo', 'name', 'tel', 'created'];

  dialogRef: any;
  newBusinessDialogRef: MatDialogRef<NewBusinessFormDialogComponent>;

  
  permissions: any[];
  roles: any[];
  users: any[];
  business: any[];
  
  _id: any;

  businessQuery: String = '';
  avatarPath: String = environment.apiUrl + 'avatars/';

  current_role: Role;
  current_business: Business;
  current_users: User[];
  admin_user: User = new User({
    firstname : '',
    lastname : '',
    email : '',
    role : '',
  });
   current_node_display: any;
  current_files: File[];
  current_consoles: any = {};
  current_user: User;
  create_new_folder: Boolean;

  create_new_file: Boolean;
  new_file: any;
  new_console: any = {
    nimsoft: {},
    spectrum: {},
    servicenow: {},
    monitis: {}
  };
  edit_mode: Boolean = false;
  edit_user_mode: Boolean = false;
  userQuery: String = '';
  business_users: { [_id: string]: User[] };
  
  errorMessage: String = '';

  url_fileserve: String = '';

  contentLoading: Boolean = true;

  @ViewChild(TreeComponent)
  private tree: TreeComponent;

  constructor(
    private businessService: BusinessService,
    private usersService: UsersService,
    private rolesService: RolesService,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    ) {
      this.searchInput = new FormControl('');
     }

  ngOnInit() {

    this.edit_mode = false;

    this.current_node_display = {
      path : '',
      files: [],
      node: {},
      nodeRef: {}
    };


    this.businessService.getAll().subscribe(
      bus => {
        this.business = bus;
        this.dataSource.data = this.business;
        this.dataSource.sort = this.sort;
        this.contentLoading = false;
      },
      e => {this.errorMessage = e; }
    );
      
    
    
    this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
              this.businessFiltered( searchText );
            });

  }

  businessFiltered( searchText: string) {

    this.dataSource.data = this.business;

    if ( searchText && searchText.length ) {
      this.dataSource.data = this.dataSource.data.filter( (el) => {
        return ( (el['name'] ).toLowerCase().indexOf(searchText.toLowerCase()) !== -1 || (el['acronimo'] ).toLowerCase().indexOf(searchText.toLowerCase()) !== -1 );
      });
    }

  }

  /**
   * 
   *  Business functions
   * 
   */
  

  
  


  newBusinessd(){
    this.admin_user = new User({
            firstname : '',
            lastname : '',
            email : '',
            role : '',
          });
    
    /*      this.setActiveBusiness(new Business({
      _id: '',
      name: '',
      acronimo: '',
      calle: '',
      colonia: '',
      municipio: '',
      estado: '',
      pais: '',
      cp: '',
      tel: '',
      admin: '',
    }));
    */
    // this.editMode(true);
  }
  
  newBusiness() {
    this.newBusinessDialogRef = this.dialog.open(NewBusinessFormDialogComponent, {
        panelClass: 'contact-form-dialog',
        data      : {
            action: 'new'
        }
    });

    this.newBusinessDialogRef.afterClosed()
        .subscribe((response: any) => {
            if ( !response )
            {
                return;
            }
            this.businessService
            .saveOrUpdate(response)
            .subscribe(
              (res) => {
                if (res.message){
                  // Error, business not stored
                  this.snackBar.open(res.message, 'Cerrar', {
                    duration: 3000
                  });
                } else {
                  this.snackBar.open('La empresa ha sido creada', 'Cerrar', {
                    duration: 3000
                  });
                  this.router.navigate(['business/details/' + res._id]);
                }
              }
            );
        });

}
 
}
