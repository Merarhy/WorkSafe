import { Injectable } from '@angular/core';
import { User } from './users.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class UsersService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    getAll(businessId:string) : Observable<User[]> {
        return this.http.get(`${environment.apiUrl}businessusers/${businessId}`, this.requestOptions)
            .map( mapUsers );
    }
    
    saveOrUpdate(businessId:string, user: User) : Observable<any> {
        if(user._id)
            return this.http.patch(`${environment.apiUrl}businessusers/${businessId}/user/${user._id}`, user, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}businessusers/${businessId}`, user, this.requestOptions).map( extractData );
    }

    updateAvatar(businessId: string, userId: string, avatar: string): Observable<any> {
        return this.http.patch(`${environment.apiUrl}businessusers/${businessId}/user/${userId}/avatar`, {avatar: avatar}, this.requestOptions).map( extractData );
    }

    searchUser(email: String): Observable<User[]> {
        return this.http.get(`${environment.apiUrl}businessusers/search/${email}`, this.requestOptions)
            .map( mapUsers );
    }
    
    delete(user: User) : Observable<any> {
        if(user._id)
            return this.http.delete(`${environment.apiUrl}businessusers/${user.business}/user/${user._id}`,this.requestOptions).map( extractData );
    }
    
    resetUserBusinessPassword(businessId: string, _id: string):Observable<any>{
        return this.http.get(`${environment.apiUrl}/businessusers/${businessId}/user/${_id}/resetpassword`, this.requestOptions).map( extractData );
    }

    
}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toUser);
    } catch (error) {}
    return body || { };
}


function mapUsers(response:Response): User[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toUser);
}


function toUser(r:any): User{
    let user = <User>({
        _id: r._id,
        firstname: r.firstname,
        lastname: r.lastname,
        email: r.email,
        phone: r.phone,
        active: r.active,
        business: r.business,
        avatar: r.avatar,
        extradata: r.extradata,
        role: r.role,
        externo: r.externo
    });
    return user;
}