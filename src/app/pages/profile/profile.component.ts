import { Component, OnInit, ViewEncapsulation,  ViewChild, Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from "@angular/router";
import { routeAnimation } from "../../route.animation";
import {LoginService} from '../../pages/login/login.service';
import {MatDialog, MatSnackBar} from "@angular/material";
import {SettingsCatalogsService} from '../access/settings-catalogs.service';
import { User } from "../business/users.model";
import { MyUsersService } from "../my-business/my-users.service";
import { environment } from '../../../environments/environment';
import { fuseAnimations } from '../../core/animations';
import { Role } from '../business/roles.model';
import { RolesService } from '../business/roles.service';


@Component({
  selector: 'ms-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None,
  providers: [ MyUsersService, SettingsCatalogsService, RolesService]
})
export class ProfileComponent implements OnInit {


  identity:any;
  roles: any[];

  user: any={};
  new_password: string;
  confirm_password: string;
  current_password: string;
  current_user: any;
  brand: string;
  model: string;
  series: string;
  avatarPath: String = environment.apiUrl + 'avatars/';
  datacenters: any[] = [];
  datacenter_request: any = {
    days: [],
    from: '',
    to: '',
    datacenters: []
  };
  current_dc_requests: any[] = [];

  new_request: Boolean = false;

  available_hours: string[] = ['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30',
  '12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];

  constructor(
    private route: ActivatedRoute,
    private rolesService: RolesService,
    private loginService: LoginService,
    private settingsCatalogsService: SettingsCatalogsService,
    private snackBar: MatSnackBar,
    private  myusersservice: MyUsersService
  ) { }

  ngOnInit() {
   

    this.identity = this.loginService.getIdentity();

    if ( this.identity.business ) {
      // Get current user info
      this.myusersservice.getCurrentUser().subscribe(
        element => { this.user = element; },
        e => {console.log(e);}
      );

      // Get catalogs
      this.settingsCatalogsService.getCollection('infra', 'Datacenter').subscribe(
        datacenters => {
          this.datacenters = datacenters;
          this.datacenters = this.datacenters.sort(function (a, b) {
            if(a.name > b.name) return -1;
            if(a.name < b.name) return 1;
            return 0;
          });
        },
        err => { console.log(err); }
      );

      // Get DC requests
      this.myusersservice.getCurrentUserDatacenterRequests().subscribe(
        data => {
          if (data['data']) {
            this.current_dc_requests = data['data'].filter( el => !el.authorized_date );
            console.log(this.current_dc_requests);
          }
        },
        err => {
          console.log('error', err);
        }
      );

    }
  
    

  }



  UpdatePassword() {
    if ( this.new_password === this.confirm_password ) {

      this.current_user = new User({
        _id: this.loginService.identity._id,
        new_password: this.new_password,
        current_password: this.current_password
      });

      this.myusersservice
        .updateCurrentUserPassword(this.current_user)
        .subscribe(
          (res) => {
            if (res.message) {
              this.snackBar.open(res.message, 'Cerrar', {
                duration: 3000
              });
            } else {
              this.new_password = '';
              this.confirm_password = '';
              this.current_password = '';
              this.snackBar.open('La contraseña ha sido actualizada', 'Cerrar', {
                duration: 3000
              });
            }
          }
        );

    }else{
      this.snackBar.open('El nuevo password no coincide ', 'Cerrar', {
          duration: 3000
        });
    }
  }


  addDevices() {
    var device = {
      brand: this.brand,
      model: this.model,
      series: this.series
    };
    this.myusersservice
    .addCurrentUserDevice(device)
    .subscribe(
      (res) => {
        if (!res._id) {
          this.snackBar.open('Error al agregar el dispositivo.', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.user = res;
          this.brand = '';
          this.model = '';
          this.series = '';
          this.snackBar.open('Se han agregado el dispositivo.', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        console.log(err);
        this.snackBar.open('Error adding new device', 'Cerrar', {
          duration: 3000
        });
      }
    );
  }



  deleteDevices(id) {
    this.myusersservice.deleteCurrentUserDevice(id).subscribe(
      res => {
        if (res.message) {
          this.snackBar.open(res.message, 'Cerrar', {
            duration: 3000
          });
        } else {
          this.user.devices.splice( this.user.devices.map((el) =>  el._id ).indexOf(id), 1);
          this.snackBar.open('El equipo se ha eliminado', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        console.log(err);
        this.snackBar.open('Error deleting device', 'Cerrar', {
          duration: 3000
        });
      }
    );

  }

  addOrRemoveDay( checked, id ) {
    if (checked) {
      if ( this.datacenter_request.days.indexOf(id) < 0 ) {
        this.datacenter_request.days.push(id);
      }
    } else {
      this.datacenter_request.days.splice(this.datacenter_request.days.indexOf(id), 1);
    }
    this.datacenter_request.days.sort();
  }

  addOrRemoveDatacenter( checked, id ) {
    if (checked) {
      if ( this.datacenter_request.datacenters.indexOf(id) < 0 ) {
        this.datacenter_request.datacenters.push(id);
      }
    } else {
      this.datacenter_request.datacenters.splice(this.datacenter_request.datacenters.indexOf(id), 1);
    }
  }

  validDatacenterRequest() {
    if ( !this.datacenter_request.days.length  ){
      return false;
    }
    if ( !this.datacenter_request.from.length || !this.datacenter_request.to.length || parseFloat( this.datacenter_request.from.replace(':', '.') ) > parseFloat( this.datacenter_request.to.replace(':', '.') ) ){
      return false;
    }
    if ( !this.datacenter_request.datacenters.length  ){
      return false;
    }
    return true;
  }

  makeDatacenterRequest() {
    this.myusersservice.addCurrentUserDatacenterRequest(this.datacenter_request).subscribe(
      res => {
        if (res._id) {
          if ( res.status === 'Autorizada' ) {
            this.loginService.validate_token();
            this.datacenter_request = {
              days: [],
              from: '',
              to: '',
              datacenters: []
            };
            this.snackBar.open('La solicitud ha guardada y autorizada', 'Cerrar', {
              duration: 3000
            });
          } else {
            this.current_dc_requests.push( res );
            this.snackBar.open('La solicitud ha sido enviada', 'Cerrar', {
              duration: 3000
            });
          }
        } else {
          var message = 'Error saving request';
          if ( res.message ) {
            message = res.message;
          }
          this.snackBar.open(message, 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        console.log(err);
        this.snackBar.open('Error saving request', 'Cerrar', {
          duration: 3000
        });
      }
    );
  }

  getDayName( dayNumber ) {
    const dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    return dayNames[dayNumber];
  }

  getAccessDatacenter( ids ){
    if ( ids && this.datacenters ) {
      return this.datacenters.filter( el => ids.indexOf(el._id)!==-1 );
    } else {
      return [];
    }
    
  }
}
