import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
import { IdToRolePipeModule } from '../../pipes/idtorole.module';

import { ProfileComponent } from './profile.component';

@NgModule({
    declarations: [
      ProfileComponent
    ],
    imports     : [
        IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
      ProfileComponent
    ]
})

export class ProfileModule
{

}
