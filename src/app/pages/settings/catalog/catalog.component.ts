import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import * as _ from 'lodash';
import { LoginService } from '../../login/login.service';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'fuse-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
  providers: [SettingsService]
})
export class CatalogComponent implements OnInit {

  loadingCollection: Boolean = false;

  collections: any[] = [
    {type: 'settings', collection: 'Location', label: 'Ubicaciones'},
    {type: 'settings', collection: 'Categorie', label: 'Categories'},

  ];
  
  current_collection: string;
  current_collection_type: string;
  collection_data: any[];
  collection_data_all: any[];
  collection_config: {
    size: number,
    headers: string[]
  };
  newitem: any = {};

  constructor(
    public settingsService: SettingsService,
    public loginService: LoginService,
    private snackBar: MatSnackBar) {
      this.collections = _.sortBy(this.collections, ['label']);
      this.setCollection(this.collections[0]);
     }

  ngOnInit(){}

  setCollection(collection) {

    this.loadingCollection = true;

    this.collection_data = [];
    this.collection_config = { size: 0, headers: []};
    this.current_collection = collection.collection;
    this.current_collection_type = collection.type;
    this.settingsService.getCollection(collection.type, collection.collection).subscribe(
      res => {
        console.log(res);
        this.collection_data_all = res;
        this.collection_data = res.slice(0, 100);
        if (this.collection_data[0]) {
          this.collection_config.headers = Object.keys(this.collection_data[0]);
          this.collection_config.size = this.collection_config.headers.length;
          this._initializeNewItem();
        }
        this.loadingCollection = false;
      },
      err => {
        alert('Error loading data');
        this.loadingCollection = false;
      }
    );
  }

  type(item) {
    return typeof(item);
  }

  _initializeNewItem() {
    this.newitem = {};
    this.collection_config.headers.map( key =>  {
      if (key !== '_id') {
        if (typeof(this.collection_data[0][key]) !== 'object') {
          this.newitem[key] = '';
        } else {
          this.newitem[key] = [{value: ''}];
        }
      }
    });
  }

  removeSubitem(item, index) {
    item.splice(index, 1);
  }

  addSubitem(item) {
    item.push({value: ''});
  }

  addItem() {

    let tmp_item = _.cloneDeep(this.newitem);
    tmp_item = this._transformCollectionObject(tmp_item);

    this.settingsService.addCollectionItem(this.current_collection_type, this.current_collection, tmp_item).subscribe(
      res => {
        if (res['_id']) {
          this.snackBar.open('El item ha sido guardado', 'Cerrar', {
            duration: 3000
          });
          this.collection_data.push(res);
          this.collection_data_all.push(res);
          this._initializeNewItem();
        } else {
          this.snackBar.open('Ha ocurrido un error al guardar el item', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        this.snackBar.open('Ha ocurrido un error al guardar el item', 'Cerrar', {
          duration: 3000
        });
      },
    );

  }

  updateItem(item) {
    let tmp_item = _.cloneDeep(item);
    tmp_item = this._transformCollectionObject(tmp_item);

    this.settingsService.updateCollectionItem(this.current_collection_type, this.current_collection, tmp_item).subscribe(
      res => {
        if (res['_id']) {
          this.snackBar.open('El item ha sido actualizado', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.snackBar.open('Ha ocurrido un error al actualizar el item', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        this.snackBar.open('Ha ocurrido un error al actualizar el item', 'Cerrar', {
          duration: 3000
        });
      },
    );
  }

  deleteItem(item) {
    if (confirm('¿Deseas borrar el item?')) {
      const index = this.collection_data.findIndex( el => (el._id === item._id ) );
      const master_index = this.collection_data_all.findIndex( el => (el._id === item._id ) );

      this.settingsService.deleteCollectionItem(this.current_collection_type, this.current_collection, item).subscribe(
        res => {
          if (!res['message']) {
            this.snackBar.open('El item ha sido borrado', 'Cerrar', {
              duration: 3000
            });
            this.collection_data.splice(index, 1);
            this.collection_data_all.splice(master_index, 1);
          } else {
            this.snackBar.open('Ha ocurrido un error al borrar el item', 'Cerrar', {
              duration: 3000
            });
          }
        },
        err => {
          this.snackBar.open('Ha ocurrido un error al borrar el item', 'Cerrar', {
            duration: 3000
          });
        },
      );
    }
  }

  // convert object to array of any items [{value:123}] => [123]
  _transformCollectionObject(item) {
      const tmp_item = item;
      for (const key in tmp_item) {
        if (typeof tmp_item[key] !== 'string') {
            tmp_item[key] = tmp_item[key].map( el => el.value );
        }
      }
      return tmp_item;
  }

 

}
