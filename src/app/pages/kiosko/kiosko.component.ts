import { NavigationEnd, NavigationStart, RouterModule, Router, ActivatedRoute} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { MatSnackBar } from '@angular/material';
import * as _ from 'lodash';
import { KioskoService } from './kiosko.service';
import { fuseAnimations } from '../../core/animations';

@Component({
  selector: 'fuse-app-kiosko',
  templateUrl: './kiosko.component.html',
  styleUrls: ['./kiosko.component.scss'],
  providers: [KioskoService],
  animations : fuseAnimations
})
export class KioskoComponent implements OnInit {
  
  current_catalog: any;
  errorMessage: String = '';

  constructor(
    private kioskoService: KioskoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.kioskoService.getCatalogo().subscribe(
      data =>  { this.current_catalog = data; },
      err  =>  { this.errorMessage = err; }
    );
    
  }

}
