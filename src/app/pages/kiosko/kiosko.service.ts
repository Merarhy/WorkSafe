import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LoginService } from '../login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class KioskoService {
   
    requestOptions: RequestOptions;

    constructor(private http: Http, loginService: LoginService){
        const headers = new Headers({'Authorization': loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }
    
    getCatalogo(): Observable<any> {
        return this.http.get(`${environment.apiUrl}kiosko/catalogos`, this.requestOptions).map( extractData );
    }

}

function extractData(res: Response): Object {
    let body = res.json();
    try {
        if (body.data) {
            body = body.data.map(toAny);
        } else {
            body = toAny(body);
        }
    } catch (error) {}
    return body || { };
}

function toAny(r: any): any {
    const options = {};
    for (const key in r) {
        if (typeof(r[key]) !== 'object') {
            options[key] = r[key];
        } else {
            options[key] = r[key].map( el => { return { value: el }; });
        }
    }
    return <any>(options);
}


function mapAny(response: Response): any[] {
    // The response of the API has a results
    // property with the actual results
    return response.json();
}
