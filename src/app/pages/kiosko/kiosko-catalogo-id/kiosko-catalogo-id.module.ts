import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { KioskoCatalogoIdComponent } from './kiosko-catalogo-id.component';
import { AgmCoreModule } from '@agm/core';
import { BrowserModule } from '@angular/platform-browser';
import { CatalogoSiteIdComponent } from '../catalogo-site-details/catalogo-site-id.component';

@NgModule({
    imports: [
        BrowserModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    declarations: [
        KioskoCatalogoIdComponent,
        CatalogoSiteIdComponent
   
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]

})
export class KioskoModule
{
}

