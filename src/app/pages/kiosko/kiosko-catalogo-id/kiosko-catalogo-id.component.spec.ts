import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KioskoCatalogoIdComponent } from './kiosko-catalogo-id.component';

describe('KioskoCatalogoIdComponent', () => {
  let component: KioskoCatalogoIdComponent;
  let fixture: ComponentFixture<KioskoCatalogoIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KioskoCatalogoIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KioskoCatalogoIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
