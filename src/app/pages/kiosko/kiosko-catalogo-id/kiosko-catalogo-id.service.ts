import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../../environments/environment';
import { LoginService } from '../../../pages/login/login.service';

@Injectable()

export class KioskoSiteService {

    requestOptions: RequestOptions;

    constructor(private http: Http, loginService: LoginService){
        const headers = new Headers({'Authorization': loginService.getToken()});
        this.requestOptions = new RequestOptions({headers: headers});
    }

    getSitesfromCatalog(catalogoid: String): Observable<any> {
        return this.http.get(`${environment.apiUrl}catalogo/places/${catalogoid}`, this.requestOptions).map( extractData );
    }

    addSitesfromCatalog(catalogoid: String, file: any, site: any): Observable<any>{
        const formData = new FormData();
        formData.append('bus_file',  file),
        formData.append('bus',  JSON.stringify(site));
        return this.http.post(`${environment.apiUrl}catalogo/places/${catalogoid}`, formData, this.requestOptions).map( extractData ); 
    }
    editSitesfromCatalog(siteid: String, file: any, site: any){
        const formData = new FormData();
       
            formData.append('bus',  JSON.stringify(site));

            if (file){
                formData.append('bus_file',  file);
            }
            return this.http.patch(`${environment.apiUrl}catolog/places/${siteid}`, formData, this.requestOptions).map( extractData );    
            
    }
    
    
    removeSitesFromCatalog(siteid: String): Observable<any>{
        return this.http.delete(`${environment.apiUrl}catalogo/places/${siteid}`, this.requestOptions).map( extractData );
    }



}

function extractData(res: Response): Object {
    let body = res.json();
    try {
        if (body.data) {
            body = body.data.map(toAny);
        } else {
            body = toAny(body);
        }
    } catch (error) {}
    return body || { };
}

function toAny(r: any): any {
    const options = {};
    for (const key in r) {
        if (typeof(r[key]) !== 'object') {
            options[key] = r[key];
        } else {
            options[key] = r[key].map( el => { return { value: el }; });
        }
    }
    return <any>(options);
}


function mapAny(response: Response): any[] {
    // The response of the API has a results
    // property with the actual results
    return response.json();
}