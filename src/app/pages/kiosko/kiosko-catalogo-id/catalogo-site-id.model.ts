export class Site {
    _id: string;
    name: string;
    calle: String;
    colonia: String;
    municipio: String;
    estado: String;
    tel: String;
    lat: Number;
    lng: Number;
    description: String;
    
    constructor(model: any = null) {
      if ( model._id) {
        this._id = model._id;
      }

      this.name = model.name;
      this.calle = model.calle;
      this.colonia = model.colonia;
      this.municipio = model.municipio;
      this.estado = model.estado;
      this.tel = model.estado;
      this.lat = model.lat;
      this.lng = model.lng;
      this.description =  model.description;
      
    }
}
