import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule } from '@agm/core';
import { NavigationEnd, NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { fuseAnimations } from '../../../core/animations';
import { LoginService } from '../../login/login.service';
import { MatSnackBar } from '@angular/material';
import { KioskoSiteService } from './kiosko-catalogo-id.service';
import { Site } from './catalogo-site-id.model';
import { CatalogoSiteIdComponent } from '../catalogo-site-details/catalogo-site-id.component';
import { environment } from '../../../../environments/environment';
import { TreeComponent } from 'angular-tree-component';
import { resolve } from 'q';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'fuse-kiosko-catalogo-id',
  templateUrl: './kiosko-catalogo-id.component.html',
  styleUrls: ['./kiosko-catalogo-id.component.scss'],
  providers: [KioskoSiteService],
  animations: fuseAnimations
})
export class KioskoCatalogoIdComponent implements OnInit {

    
    create_place: Boolean = true;
    siteid: String;
    lat = 19.2582705;
    lng = -99.58121540000002;
    errorMessage: String = '';
    current_site: any;
    size1: number;
    size2: number;
    margin: string;

     
    curent_lat: any;
    current_lng: any;
  
    dialogRef: any;
    newBusinessDialogRef: MatDialogRef<CatalogoSiteIdComponent>;

    server: String;
    url: String = 'site/img/';
    location: any;

   
    
    myname: String; 
    
    new_site: any;
    selectedFiles: any; 
     

  constructor(
    private loginService:  LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private kioskoService: KioskoSiteService,
    public dialog: MatDialog
  ) { 
 
    this.route.params.subscribe(
      params => {
          this.siteid = params['id'];
      });

  }

  ngOnInit() {
    this.kioskoService.getSitesfromCatalog(this.siteid).subscribe(
       response => {  
         if (response.length >= 1)
          { 
            this.current_site = response; 
            this.size1 = 60;
            this.size2 = 40;
            this.margin = '85%';
          }else
          { 
              this.current_site = [];
              this.size1 = 0;
              this.size2 = 100;
              this.margin = '90%';
          }
        },
      error => { this.errorMessage = error; }
    );

    if (navigator.geolocation){
      navigator.geolocation.getCurrentPosition(position => {
        this.location = position.coords;
        this.curent_lat = this.location.latitude;
        this.current_lng = this.location.longitude;
        this.lat = this.curent_lat;
        this.lng = this.current_lng;

        });
     }
      
    this.server = environment.apiUrl + this.url;
  }



  // show modal add Site

  showAddSite(){
    
    this.newBusinessDialogRef = this.dialog.open(CatalogoSiteIdComponent, {
      panelClass: 'contact-form-dialog',
        data: {
          action: 'new'
        }    
    });

   
    this.newBusinessDialogRef.afterClosed()
        .subscribe((response: any) => {
        if ( !response )
        {
                return;
          }else{

          this.addSite(response);

          }
      });



  }


  addSite(data){
    this.new_site = data[0].save;
    this.selectedFiles = data[0].img.item(0);

    if (this.selectedFiles){

      const  req = (this.selectedFiles.type === 'image/jpeg' || this.selectedFiles.type === 'image/jpg' ) ? true : false;
      if(req){
          this.kioskoService.addSitesfromCatalog(this.siteid, this.selectedFiles, this.new_site).subscribe(
            (response) => {
              if (response.message){
                this.snackBar.open(response.message, 'cerrar', {
                  duration: 3000
                });
              }else{
                this.snackBar.open('Se ha creado el nuevo sitio', 'cerrar', {
                  duration: 3000
                });
                this.current_site.push(response);
              }
          });
      }else{
        this.snackBar.open('Solo se permite imagenes JPG', 'Cerrar', {
          duration: 3000
        });
      }
    }else{
      this.snackBar.open('No se ha seleccionado ningina imagen', 'Cerrar', {
        duration: 3000
      });

    }


  }

  

  removeSite(siteid){  

     this.kioskoService.removeSitesFromCatalog(siteid).subscribe(
      (res) => {
        if ( res.message ){
          this.snackBar.open(res.message, 'Cerrar', {
            duration: 3000
          });
        } else {         
          this.current_site.splice( this.current_site.map((el) =>  el._id ).indexOf(siteid), 1);
          this.snackBar.open('El sitio ha sido eliminado', 'Cerrar', {
            duration: 3000
          });
        }
      });
  
  }

  showEditSite(data){

    this.newBusinessDialogRef = this.dialog.open(CatalogoSiteIdComponent, {
      panelClass: 'contact-form-dialog',
        data: {
          action: 'edit',
          data: data
        }
    });
    this.newBusinessDialogRef.afterClosed()
        .subscribe((response: any) => {
            if ( !response )
            {
                return;
            }else{

              this.editSite(response);

            }
          });


  }

  editSite(response){
   const id_site = response[0].save._id;
   const site = response[0].save;
   const img = (response[0].img) ? response[0].img.item(0) : response[0].img;
  
  
  this.kioskoService.editSitesfromCatalog(id_site, img, site).subscribe(
   
  );


  }
  

  openModal(site){
  
    this.current_site = this.current_site.map( el => {
      if ( el._id === site._id ) {
        el.type = true;
        this.lat = el.lat;
        this.lng = el.lng;
      } else {
        el.type = false;
      }
      return el;
    });

  }

}
