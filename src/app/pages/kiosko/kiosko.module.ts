import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { KioskoService } from './kiosko.service';
import { KioskoComponent } from './kiosko.component';
import { SharedModule } from '../../core/modules/shared.module';
import { FuseWidgetModule } from '../../core/components/widget/widget.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { KioskoCatalogoIdComponent } from './kiosko-catalogo-id/kiosko-catalogo-id.component';
import { AgmCoreModule } from '@agm/core';
import { CatalogoSiteIdComponent } from './catalogo-site-details/catalogo-site-id.component';
import { CatalogoSiteFormComponent } from './catalogo-site-form/catalogo-site-form.component';

@NgModule({
    imports: [
        SharedModule,
        FuseWidgetModule,
        NgxChartsModule
    ],
    declarations: [
        KioskoComponent,
        CatalogoSiteIdComponent,
        CatalogoSiteFormComponent,
   
    ],
    providers: [
        KioskoService,
        
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class KioskoModule
{
}

