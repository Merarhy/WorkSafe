import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog , MatSnackBar } from '@angular/material';
import { fuseAnimations } from '../../../core/animations';
import { CatalogoSiteService } from './catalogo-site-id.service';
import { NavigationEnd, NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { Site } from '../kiosko-catalogo-id/catalogo-site-id.model';

@Component({
  selector: 'fuse-catalogo-site-id',
  templateUrl: './catalogo-site-id.component.html',
  styleUrls: ['./catalogo-site-id.component.scss'],
  providers: [CatalogoSiteService],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class CatalogoSiteIdComponent implements OnInit {
  
 
  current_site: any;
  title: String;
  info: any;
  contactForm: FormGroup;
  
  server: String;
  url: String = 'site/img/';
  selectedFiles: any;
 
 

  constructor(
    private catalogoSiteService: CatalogoSiteService,
    private route: ActivatedRoute,
    public dialogRef: MatDialogRef<CatalogoSiteIdComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
  ) { 



    if(data.action === 'new'){
      this.title = 'Crear Sitio';
      this.info = new Site({});
      
    }else{
      this.title = 'Editar Sitio';
      this.info = data.data;
     
    }

    this.contactForm = this.createContactForm();
    this.server = environment.apiUrl + this.url;
    
  }

  ngOnInit() {

  


  }

  detectAudio(event) {
    this.selectedFiles = event.target.files;
  }

  editSite(data){
   
   this.dialogRef.close([{
    'save': data,
    'img':  this.selectedFiles }]);

  }
  crearSite(data){
   if(this.selectedFiles){
    this.dialogRef.close([{
     'save': data,
     'img':  this.selectedFiles }]);
    }else{
      this.snackBar.open('No se ha seleccionado ninguna imagen', 'Cerrar', {
        duration: 3000
      });
    }
   } 


  createContactForm()
  {  
      return this.formBuilder.group({
          name: [this.info.name, Validators.required],
          tel: [this.info.tel],
          calle: [this.info.calle, Validators.required],
          colonia: [this.info.colonia, Validators.required],
          municipio: [this.info.municipio, Validators.required],
          estado: [this.info.estado, Validators.required],
          codigo: [this.info.codigo],
          lat: [this.info.lat, Validators.required],
          lng: [this.info.lng, Validators.required],
          description: [this.info.description],
  
        }); 
  
  }

}
