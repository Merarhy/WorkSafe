import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoSiteIdComponent } from './catalogo-site-id.component';

describe('CatalogoSiteIdComponent', () => {
  let component: CatalogoSiteIdComponent;
  let fixture: ComponentFixture<CatalogoSiteIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoSiteIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoSiteIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
