import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { fuseAnimations } from '../../../core/animations';
import { NavigationEnd, NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-catalogo-site-form',
  templateUrl: './catalogo-site-form.component.html',
  styleUrls: ['./catalogo-site-form.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class CatalogoSiteFormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
