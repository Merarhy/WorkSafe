import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoSiteFormComponent } from './catalogo-site-form.component';

describe('CatalogoSiteFormComponent', () => {
  let component: CatalogoSiteFormComponent;
  let fixture: ComponentFixture<CatalogoSiteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogoSiteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoSiteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
