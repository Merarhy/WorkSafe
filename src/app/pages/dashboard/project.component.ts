import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectsDashboardService } from './projects.service';
import * as shape from 'd3-shape';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { fuseAnimations } from '../../core/animations';
import { MyUsersService } from '../my-business/my-users.service';
import { LoginService } from '../login/login.service';
import { SettingsCatalogsService } from '../access/settings-catalogs.service';
import { AccessService } from '../access/access.service';
import { MatTableDataSource , MatPaginator, MatSort} from '@angular/material';
import { Evento } from '../access/access.model';
import { DirectoryManagerService } from '../my-business/file-manager/directory-manager.service';
import { FileManagerService  } from '../my-business/file-manager/file-manager.service';
import {NgxChartsModule} from '@swimlane/ngx-charts';


moment.locale('es-mx');

@Component({
    selector     : 'fuse-project',
    templateUrl  : './project.component.html',
    styleUrls    : ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
    providers: [AccessService, SettingsCatalogsService, MyUsersService, DirectoryManagerService, FileManagerService]
})
export class FuseProjectComponent implements OnInit, OnDestroy
{  
      
  single: any[] =  
   [
      {
        name: 'Germany',
        value: 100
      },
    ];
  multi: any[] = [
    {
      name: '2018',
      series: [
        {
          name: 'enero',
          value: 24
        },
        {
          name: 'febrero',
          value: 18
        }
      ]
    },
  
  ];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#00bcd4']
  };

  // line, area
  autoScale = true;

  

    


    
    users: any;

    projects: any[];
    selectedProject: any;

    widgets: any;
    widget5: any = {};
    widget6: any = {};
    widget7: any = {};
    widget8: any = {};
    widget9: any = {};
    widget11: any = {};

    // Not demo
    dateNow = moment().format('D MMM YYYY');
    eventos: any;
    today_events: any;
    nexts_events: any;
    location_events: any;

    // Files
    dataSource: any;
    displayedColumns = ['name'];
  
    current_directory: any;
    current_file: any;
    select_files: any;
    showDetails: Boolean = false;
    errorMessage: String = '';
    url_fileserve: any;
    
    identity: any;

    constructor(
        private router: Router,
        private loginService: LoginService,
        private calendarService: AccessService,
        private projectsDashboardService: ProjectsDashboardService,
        private directoryManagerService: DirectoryManagerService,
        private fileManagerService: FileManagerService,
        private usersService: MyUsersService,
    )
    {
        this.projectsDashboardService.read().then( complete => {
            this.projects = this.projectsDashboardService.projects;

            this.selectedProject = this.projects[0];

            this.widgets = this.projectsDashboardService.widgets;
            

            /**
             * Widget 5
             */
            this.widget5 = {
                currentRange  : 'TW',
                xAxis         : true,
                yAxis         : true,
                gradient      : false,
                legend        : false,
                showXAxisLabel: false,
                xAxisLabel    : 'Days',
                showYAxisLabel: false,
                yAxisLabel    : 'Isues',
                scheme        : {
                    domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                },
                onSelect      : (ev) => {
                },
                supporting    : {
                    currentRange  : '',
                    xAxis         : false,
                    yAxis         : false,
                    gradient      : false,
                    legend        : false,
                    showXAxisLabel: false,
                    xAxisLabel    : 'Days',
                    showYAxisLabel: false,
                    yAxisLabel    : 'Isues',
                    scheme        : {
                        domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                    },
                    curve         : shape.curveBasis
                }
            };

            /**
             * Widget 6
             */
            this.widget6 = {
                currentRange : 'TW',
                legend       : false,
                explodeSlices: false,
                labels       : true,
                doughnut     : true,
                gradient     : false,
                scheme       : {
                    domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63']
                },
                onSelect     : (ev) => {

                }
            };

            /**
             * Widget 7
             */
            this.widget7 = {
                currentRange: 'T'
            };

            /**
             * Widget 8
             */
            this.widget8 = {
                legend       : false,
                explodeSlices: false,
                labels       : true,
                doughnut     : false,
                gradient     : false,
                scheme       : {
                    domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63', '#ffc107']
                },
                onSelect     : (ev) => {
                
                }
            };

            /**
             * Widget 9
             */
            this.widget9 = {
                currentRange  : 'TW',
                xAxis         : false,
                yAxis         : false,
                gradient      : false,
                legend        : false,
                showXAxisLabel: false,
                xAxisLabel    : 'Days',
                showYAxisLabel: false,
                yAxisLabel    : 'Isues',
                scheme        : {
                    domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                },
                curve         : shape.curveBasis
            };

            /**
             * Widget 11
             */
            this.widget11.onContactsChanged = new BehaviorSubject({});
            this.widget11.onContactsChanged.next(this.widgets.widget11.table.rows);
            this.widget11.dataSource = new FilesDataSource(this.widget11);

        });

    }

    ngOnInit() {

        this.identity =  this.loginService.getIdentity();


        

        if ( !this.loginService.permissions || !this.loginService.permissions.length ) {
            setTimeout( () => {
                this._initServices();
            }, 2000);
        } else {
            this._initServices();
        }
      
        if (this.identity.business){
        
            this.directoryManagerService.getsdrFavDirectories().subscribe(
                dirs => { this.current_directory = dirs; },
                err => { this.errorMessage = err; },
            );
            this.fileManagerService.getAll().subscribe(
                files => { this.current_file = files;  },
                err => { this.errorMessage = err; },
            );
        }

    }

    _initServices() {
        
        if ( this.loginService.hasPermission('access-profile-reception') ) {
          /*this.calendarService.getCommingLocationEvents().subscribe(
            res => {
              this.location_events = res;
      
              this.location_events = this.location_events.map( el => {
                el.start_date = moment.utc(el.start_date);
                el.end_date = moment.utc(el.end_date);
                return el;
              });
            
            },
            e => { console.log(e); }
          );*/
        }
        
        if ( this.loginService.hasPermission('access-event-read') ) {
        
            
        } else {
          console.log('no permissions');
        }

       

      }
      
      getDayName( dayNumber ) {
        const dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
        return dayNames[dayNumber];
      }

    ngOnDestroy() {
    }

    showFiles(dir){

        this.showDetails = true;
   
        this.select_files = this.current_file.filter( (el) => { return  el._directory  == dir._id });

    }

    viewImg(file){
        this.url_fileserve = this.fileManagerService.fileServeUrl(file._business,file.file);
        window.open(this.url_fileserve, '_blank');
    }

}

export class FilesDataSource extends DataSource<any>
{
    constructor(private widget11)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        return this.widget11.onContactsChanged;
    }

    disconnect()
    {
    }
}

