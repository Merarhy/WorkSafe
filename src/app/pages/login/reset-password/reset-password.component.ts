import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from "@angular/material";
import {fadeInAnimation} from "../../../route.animation";
import {Router, ActivatedRoute} from "@angular/router";
import { FuseConfigService } from '../../../core/services/config.service';
import {ResetPasswordService} from './reset-password.service';
import {LoginService} from '../login.service';
import { fuseAnimations } from '../../../core/animations';

@Component({
  selector: 'ms-register',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers: [ResetPasswordService],
  animations : fuseAnimations
})
export class ResetPasswordComponent implements OnInit {

  password: string;
  passwordConfirm: string;
  identity: any;
  
  private sub: any;
  private token: string;

  constructor(
    private router: Router,
    private fuseConfig: FuseConfigService,
    private route: ActivatedRoute,
    private resetPasswordService: ResetPasswordService,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
  ) { 
    this.fuseConfig.setSettings({
      layout: {
          navigation: 'none',
          toolbar   : 'none',
          footer    : 'none'
      }
  });

  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.token = params['token'];
      this.resetPasswordService.getIdentity(params['token']).subscribe(
        user => {
          if (!user['email']){
          } else {
            this.identity = user;
          }
        },
        err => {
          console.log(err);
          
        }
      );
    });
  }

  passwordRestore() {
    
    this.resetPasswordService.resetPassword(this.token,this.password).subscribe(
      data => {
        if (data._id){
          this.snackBar.open('El password ha sido actualizado.', 'Cerrar', {
            duration: 3000
          });
          this.loginService.doLogout();
        } else {
          this.snackBar.open(data.message, 'Cerrar', {
            duration: 3000
          });
        }
      },
      e => {
        this.snackBar.open('Ha ocurrido un error inesperado.', 'Cerrar', {
          duration: 3000
        });
      }
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
