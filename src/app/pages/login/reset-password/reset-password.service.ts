import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import { User } from '../../../pages/users/users.model';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ResetPasswordService {

    identity: any;
    token: string;

    constructor(
        private http: Http,
        private router: Router,
    ){}

    getIdentity(token: string) {
        return this.http.get(`${environment.apiUrl}auth/reset-password-request/${token}`).map( extractData );
    }

    resetPassword(token: string, password: string): Observable<any> {
        return this.http.post(`${environment.apiUrl}auth/reset-password/`, {token: token, password: password}).map( extractData );
    }
    
}


function extractData(res: Response): Object {
    const body = res.json();
    return body;
}
