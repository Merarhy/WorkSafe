import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
//import { RouterModule } from '@angular/router';

import { ResetPasswordComponent } from './reset-password.component';



@NgModule({
    declarations: [
        ResetPasswordComponent
    ],
    imports     : [
        SharedModule,
       // RouterModule.forChild(routes)
    ],
    exports     : [
        ResetPasswordComponent
    ]
})

export class  ResetPasswordModule
{

}
