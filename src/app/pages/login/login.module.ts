import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
//import { RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RouterModule, Routes} from '@angular/router';


@NgModule({
    declarations: [
        LoginComponent,
        ForgotPasswordComponent
    ],
    imports     : [
        SharedModule,
        RouterModule,
       // RouterModule.forChild(routes)
    ],
    exports     : [
        LoginComponent
    ]
})

export class LoginModule
{

}
