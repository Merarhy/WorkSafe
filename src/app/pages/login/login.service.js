"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var environment_1 = require("../../../environments/environment");
var LoginService = /** @class */ (function () {
    function LoginService(http, router) {
        this.http = http;
        this.router = router;
        this.build = false;
        this.permissions = [];
        this.permissions_slugs = [];
        this.appData = {};
        if (!this.build)
            this.__construct_data();
    }
    // Generate base data
    LoginService.prototype.__construct_data = function () {
        this.build = true;
        // Get token from local storage
        this._initializeToken();
        // Get identity from local strorage
        this._initializeIdentity();
    };
    // Load token from local storage and set up in a local variable
    LoginService.prototype._initializeToken = function () {
        var token = localStorage.getItem('token');
        if (token != undefined) {
            this.token = token;
            this.validate_token();
        }
        else {
            this.token = null;
            this._autoLogout();
        }
    };
    // Load identity from local storage an set up in a local variable
    LoginService.prototype._initializeIdentity = function () {
        var identity = JSON.parse(localStorage.getItem('identity'));
        if (identity != undefined) {
            this.identity = identity;
        }
        else {
            this.identity = null;
            this._autoLogout();
        }
    };
    // Build a permissions tree from current identity
    LoginService.prototype._makePermissionsTree = function () {
        var _this = this;
        // If permissions doesn't exists
        if (!this.permissions_slugs)
            return;
        // Sort permissions as a tree in a local variable
        this.permissions_slugs.forEach(function (element) {
            if (element) {
                var perm = element.split('-');
                if (!_this.permissions[perm[0]])
                    _this.permissions[perm[0]] = [];
                if (!_this.permissions[perm[0]][perm[1]])
                    _this.permissions[perm[0]][perm[1]] = [];
                if (_this.permissions[perm[0]][perm[1]].indexOf(perm[2]) == -1)
                    _this.permissions[perm[0]][perm[1]].push(perm[2]);
            }
        });
    };
    // Make a login request
    LoginService.prototype.doLogin = function (userdata) {
        return this.http.post(environment_1.environment.apiUrl + "auth/login", userdata).map(extractData);
    };
    // Set up identity on a local storage and set up in local variable
    LoginService.prototype.setIdentity = function (user) {
        localStorage.setItem('identity', JSON.stringify(user));
        this._initializeIdentity();
    };
    // Set up token on a local storage and setup in a local variable
    LoginService.prototype.setToken = function (token) {
        localStorage.setItem('token', token);
        this._initializeToken();
    };
    // Return identity from local variable
    LoginService.prototype.getIdentity = function () {
        return this.identity;
    };
    // Return token from local variable
    LoginService.prototype.getToken = function () {
        return this.token;
    };
    // Validate current token if isn't expired
    LoginService.prototype.validate_token = function () {
        var _this = this;
        var headers = new http_1.Headers({ 'Authorization': this.getToken() });
        var requestOptions = new http_1.RequestOptions({ headers: headers });
        var response = this.http.get(environment_1.environment.apiUrl + "auth/validate-token", requestOptions).map(extractData).subscribe(function (data) {
            if (data['valid']) {
                _this.setIdentity(data['user']);
                _this.permissions_slugs = data['permissions'];
                _this._makePermissionsTree();
                // this.get_extra_appdata();
                return true;
            }
            else {
                _this.doLogout();
                return false;
            }
        }, function (err) { return false; });
    };
    LoginService.prototype._autoLogout = function () {
        if (this.router.url !== '/') {
            this.router.navigate(['/']);
        }
    };
    // Validate if user is logged in
    LoginService.prototype.validateLogged = function () {
        if (!this.identity || !this.identity._id || !this.token || this.token.length == 0) {
            this.doLogout();
        }
    };
    // Redirect if user is logged in
    LoginService.prototype.redirectLogin = function () {
        if (this.identity && this.identity._id && this.token && this.token.length > 0) {
            this.router.navigate(['/admin']);
        }
    };
    // Validate if a permission or array of permissions are in current permissions variable
    LoginService.prototype.hasPermission = function (perms) {
        var _this = this;
        // initialize context variable
        var to_validate = [];
        // If permission is string, convert to array
        if (typeof (perms) == 'string') {
            to_validate.push(perms);
        }
        // If permission is array, copy to local variable
        if (typeof (perms) == 'object') {
            to_validate = perms;
        }
        // Filter permissions slugs that are in stored in local
        var authorized = to_validate.filter(function (el) {
            return (_this.permissions_slugs.indexOf(el) > -1);
        });
        // return true if user has one or more permissions
        return authorized.length > 0;
    };
    // Validate if user has permissions to a module, a component or activity based on the permissions tree
    LoginService.prototype.validatePermissionTree = function (module, component, activity) {
        // Validate if module permission exists
        if (this.permissions[module]) {
            // Validate if component validation is defined
            if (component) {
                //Validate if component permission exists
                if (this.permissions[module][component]) {
                    // Validate if activity validation is defined
                    if (activity) {
                        // Validate if activity permission exists
                        if (this.permissions[module][component].indexOf(activity) > -1) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    };
    LoginService.prototype.unavailableDays = function () {
        var days = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];
        // Return empty if is not business user
        if (!this.identity.business) {
            return [];
        }
        // Return all days if not assigned days
        if (!this.identity.dc_access || !this.identity.dc_access.days) {
            return days;
        }
        var unavailable = [];
        for (var i = 0; i < 7; ++i) {
            if (this.identity.dc_access.days.indexOf(i) === -1) {
                unavailable.push(days[i]);
            }
        }
        return unavailable;
    };
    LoginService.prototype.available_user_hours = function (type) {
        var available_hours = ['00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30', '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30',
            '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00', '22:30', '23:00', '23:30'];
        var tmp_available = available_hours;
        if (this.identity.business && type === 'datacenter') {
            if (this.identity.dc_access) {
                var tmp_from = parseFloat(this.identity.dc_access.from.replace(':', '.'));
                var tmp_to = parseFloat(this.identity.dc_access.to.replace(':', '.'));
                tmp_available = available_hours.filter(function (el) {
                    return (parseFloat(el.replace(':', '.')) >= tmp_from && parseFloat(el.replace(':', '.')) <= tmp_to);
                });
            }
            else {
                tmp_available = [tmp_available[0]];
            }
        }
        return tmp_available;
    };
    // Get extra app data
    LoginService.prototype.get_extra_appdata = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new http_1.Headers({ 'Authorization': _this.getToken() });
            var requestOptions = new http_1.RequestOptions({ headers: headers });
            _this.http.get(environment_1.environment.apiUrl + "tools/exchange-rate", requestOptions)
                .toPromise()
                .then(function (response) {
                var responseData = response.json();
                if (responseData && responseData['data'] && responseData['data'].rates) {
                    _this.appData.exchange = {
                        'USD': (responseData['data'].rates['MXN']).toFixed(2),
                        'EUR': (responseData['data'].rates['MXN'] / responseData['data'].rates['EUR']).toFixed(2)
                    };
                }
                resolve(responseData);
            })["catch"](function (err) {
                reject({});
            });
        });
    };
    // Clear data and redirect
    LoginService.prototype.doLogout = function () {
        localStorage.removeItem('identity');
        localStorage.removeItem('token');
        this.token = null;
        this.identity = null;
        this.permissions = [];
        this.permissions_slugs = [];
        this.build = false;
        this.router.navigate(['/']);
    };
    LoginService.prototype.goBack = function () {
        window.history.back();
    };
    LoginService = __decorate([
        core_1.Injectable()
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
function extractData(res) {
    var body = res.json();
    return body;
}
/*
function mapUsers(response:Response): User[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toUser);
}


function toUser(r:any): User{
    let user = <User>({
        _id: r._id,
        firstname: r.firstname,
        lastname: r.lastname,
        email: r.email,
        role: r.role
    });
    return user;
}
*/ 
