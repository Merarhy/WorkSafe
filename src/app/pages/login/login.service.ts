import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../pages/users/users.model';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';


@Injectable()
export class LoginService {

    identity: any;
    token: string;
    build: Boolean = false;
    permissions: any = [];
    permissions_slugs: any = [];
    appData: any = {};

    constructor(
        private http: Http,
        private router: Router
    ){
        if (!this.build)
            this.__construct_data();
    }

    // Generate base data
    __construct_data(){
        this.build = true;
        // Get token from local storage
        this._initializeToken();

        // Get identity from local strorage
        this._initializeIdentity();
    }

    // Load token from local storage and set up in a local variable
    _initializeToken(){
        let token = localStorage.getItem('token');
        if(token!=undefined){
            this.token = token
            this.validate_token();
        } else{
            this.token = null;
            this._autoLogout();
        }
    
    }
    // Load identity from local storage an set up in a local variable
    _initializeIdentity(){
        let identity = JSON.parse(localStorage.getItem('identity'));
        if(identity!=undefined){
            this.identity = identity
        }else{
            this.identity = null;
            this._autoLogout();
        }
    }

    // Build a permissions tree from current identity
    _makePermissionsTree(){

        // If permissions doesn't exists
        if(!this.permissions_slugs)
            return;
        

        // Sort permissions as a tree in a local variable
        this.permissions_slugs.forEach(element => {
            if(element){
                var perm = element.split('-');
                if(!this.permissions[perm[0]])
                    this.permissions[perm[0]] = [];
                if(!this.permissions[perm[0]][perm[1]])
                    this.permissions[perm[0]][perm[1]] = [];
                if(this.permissions[perm[0]][perm[1]].indexOf(perm[2]) == -1)
                    this.permissions[perm[0]][perm[1]].push(perm[2]);
            }
        });

    }


    // Make a login request
    doLogin(userdata: any) {
        return this.http.post(`${environment.apiUrl}auth/login`, userdata).map( extractData );
    }

    // Set up identity on a local storage and set up in local variable
    setIdentity(user: any) {
        localStorage.setItem('identity', JSON.stringify(user));
        this._initializeIdentity();
    }

    // Set up token on a local storage and setup in a local variable
    setToken(token: string) {
        localStorage.setItem('token', token);
        this._initializeToken();
    }

    // Return identity from local variable
    getIdentity(): any{
        return this.identity;
    }

    // Return token from local variable
    getToken(): string{
        return this.token;
    }

    // Validate current token if isn't expired
    validate_token() {

        let headers = new Headers({'Authorization':this.getToken()});
        let requestOptions = new RequestOptions({headers : headers});

        var response = this.http.get(`${environment.apiUrl}auth/validate-token`, requestOptions).map( extractData).subscribe(
            (data) => { 
                if(data['valid']){
                    this.setIdentity(data['user']);
                    this.permissions_slugs = data['permissions'];
                    this._makePermissionsTree();
                   // this.get_extra_appdata();
                    return true;
                } else {
                    this.doLogout();
                    return false;
                }
            },
            (err) => { return false; }
        );
    }

    _autoLogout() {
        if ( this.router.url !== '/' ) {
            this.router.navigate(['/']);
        }
    }

    // Validate if user is logged in
    validateLogged() {
        if(!this.identity || !this.identity._id || !this.token || this.token.length==0){
            this.doLogout();
        }
    }

    // Redirect if user is logged in
    redirectLogin() {
        if (this.identity && this.identity._id && this.token && this.token.length>0){
            setTimeout( () => {
                this.router.navigate(['/admin']);
            }, 300);
        }
    }

    // Validate if a permission or array of permissions are in current permissions variable
    hasPermission(perms:any): boolean{
        // initialize context variable
        var to_validate : string[] = [];
        // If permission is string, convert to array
        if(typeof(perms)=='string'){
            to_validate.push(perms);
        }
        // If permission is array, copy to local variable
        if(typeof(perms)=='object'){
            to_validate = perms;
        }

        // Filter permissions slugs that are in stored in local
        let authorized = to_validate.filter( (el) => {
            return (this.permissions_slugs.indexOf(el)>-1);
        });
        
        // return true if user has one or more permissions
        return authorized.length>0;
    }

    // Validate if user has permissions to a module, a component or activity based on the permissions tree
    validatePermissionTree(module:string,component?:string,activity?:string):boolean{
        // Validate if module permission exists
        if(this.permissions[module]){
            // Validate if component validation is defined
            if(component){
                //Validate if component permission exists
                if(this.permissions[module][component]){
                    // Validate if activity validation is defined
                    if(activity){
                        // Validate if activity permission exists
                        if(this.permissions[module][component].indexOf(activity)>-1){
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    unavailableDays() {
        var days = ['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'];
        // Return empty if is not business user
        if ( !this.identity.business ) {
            return [];
        }
        // Return all days if not assigned days
        if ( !this.identity.dc_access || !this.identity.dc_access.days ) {
            return days;
        }

        var unavailable = [];
        for ( let i = 0; i < 7; ++i ) {
            if ( this.identity.dc_access.days.indexOf(i) === -1 ) {
                unavailable.push( days[i] );
            }
        }
        return unavailable;
    }

    available_user_hours(type: String) {
        const available_hours: string[] = ['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30',
        '12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];
        
        var tmp_available = available_hours;
        
        if ( this.identity.business && type === 'datacenter' ) {
            if ( this.identity.dc_access ) {
                var tmp_from = parseFloat( this.identity.dc_access.from.replace(':', '.') );
                var tmp_to = parseFloat( this.identity.dc_access.to.replace(':', '.') );
                tmp_available = available_hours.filter( el => {
                    return ( parseFloat( el.replace(':', '.') ) >= tmp_from && parseFloat( el.replace(':', '.') ) <= tmp_to );
                });
            } else {
                tmp_available = [ tmp_available[0] ];
            }
        }

        return tmp_available;
    }

    // Get extra app data
    get_extra_appdata() {
        return new Promise<any>((resolve, reject) => {
            let headers = new Headers({'Authorization':this.getToken()});
            let requestOptions = new RequestOptions({headers : headers});

            this.http.get(`${environment.apiUrl}tools/exchange-rate`, requestOptions )
            .toPromise()
            .then( (response) => {
            var responseData = response.json();
            if( responseData && responseData['data'] && responseData['data'].rates) {
                this.appData.exchange = {
                'USD': (responseData['data'].rates['MXN']).toFixed(2),
                'EUR': (responseData['data'].rates['MXN'] / responseData['data'].rates['EUR']).toFixed(2)
                };
            }
            resolve( responseData );
            } )
            .catch( err => {
            reject({});
            });
        });
        
    }


    // Clear data and redirect
    doLogout(){
        localStorage.removeItem('identity');
        localStorage.removeItem('token');
        this.token = null;
        this.identity = null;
        this.permissions = [];
        this.permissions_slugs = [];
        this.build = false;
        this.router.navigate(['/']);
        
    }

    goBack() {
        window.history.back();
    }
    
}


function extractData(res: Response): Object {
    let body = res.json();
    return body;
}

/*
function mapUsers(response:Response): User[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toUser);
}


function toUser(r:any): User{
    let user = <User>({
        _id: r._id,
        firstname: r.firstname,
        lastname: r.lastname,
        email: r.email,
        role: r.role
    });
    return user;
}
*/