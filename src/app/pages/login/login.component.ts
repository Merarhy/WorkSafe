import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../core/services/config.service';
import { fuseAnimations } from '../../core/animations';
import { Router, ActivatedRoute } from "@angular/router";
import {LoginService} from './login.service';

@Component({
    selector   : 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls  : ['./login.component.scss'],
    animations : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;

    // Declare default email and pass value
    email: string;
    password: string;

    errorMessage: string = '';
    loading: boolean= false;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
        private router: Router,
        private route: ActivatedRoute,
    )
    {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none'
            }
        });


    }

    ngOnInit()
    {
        this.loginService.redirectLogin();
        this.loginForm = this.formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

       
    }


    loginUser(){
        this.loading = true;
        this.errorMessage = '';
        this.loginService.doLogin({email:this.email,password:this.password}).subscribe(
          data => {
            if(data['token']){
                // Store token
                this.loginService.setIdentity(data['user']);
                this.loginService.setToken(data['token']);
                this.loginService.redirectLogin();
                this.loading = false;
              } else {
                this.password = '';
                this.errorMessage = 'Email o password incorrectos.';
                this.loading = false;
              }
          },
          e => {
            this.errorMessage = e;
            this.loading = false;
          }
        );

    }
}
