import { Injectable } from '@angular/core';
import {Router } from '@angular/router';
import { User } from '../../../pages/users/users.model';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../../environments/environment';

@Injectable()
export class ForgotPasswordService {

    identity: any;
    token: string;
    
    constructor(
        private http: Http,
        private router: Router,
    ){}

    forgontPassword(email: string): Observable<any> {
        return this.http.post(`${environment.apiUrl}auth/forgot-password`, {email: email}).map( extractData );
    }
    
}


function extractData(res: Response): Object {
    const body = res.json();
    return body;
}
