import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
//import { RouterModule } from '@angular/router';

import { ForgotPasswordComponent } from './forgot-password.component';
import { RouterModule, Routes} from '@angular/router';


@NgModule({
    declarations: [
        ForgotPasswordComponent
    ],
    imports     : [
        SharedModule,
        RouterModule,
       // RouterModule.forChild(routes)
    ],
    exports     : [
        ForgotPasswordComponent
    ]
})

export class forgotpasswordModule
{

}
