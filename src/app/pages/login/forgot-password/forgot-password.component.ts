import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatSnackBar} from "@angular/material";
import {fadeInAnimation} from "../../../route.animation";
import {Router, ActivatedRoute} from "@angular/router";
import { FuseConfigService } from '../../../core/services/config.service';
import { ForgotPasswordService} from './forgot-password.service';
import {LoginService} from '../login.service';
import { fuseAnimations } from '../../../core/animations';
import { UsersService as UsersServiceSdr } from '../../users/users.service';
import { MyUsersService as UsersServiceBusiness } from '../../my-business/my-users.service';
import {ResetPasswordService} from '../reset-password/reset-password.service';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  providers: [UsersServiceSdr, UsersServiceBusiness, ForgotPasswordService, ResetPasswordService],
  
  animations : fuseAnimations
})
export class ForgotPasswordComponent implements OnInit {

  forgotPasswordForm: FormGroup;
  forgotPasswordFormErrors: any;
  users: any;
  userSdr: any;
  userBusiness: any;
  errorMessagge: String = '';

  password: string;
  passwordConfirm: string;
  identity: any;

  email: string;
  
  private sub: any;
  private token: string;


  constructor(
      private router: Router,
      private fuseConfig: FuseConfigService,
      private formBuilder: FormBuilder,
      private userServiceSdr: UsersServiceSdr,
      private userServiceBusiness: UsersServiceBusiness,
      private forgotPasswordService: ForgotPasswordService,
      private route: ActivatedRoute,
      private snackBar: MatSnackBar,
      private resetPasswordService: ResetPasswordService
  )
  {
      this.fuseConfig.setSettings({
          layout: {
              navigation: 'none',
              toolbar   : 'none',
              footer    : 'none'
          }
      });

      this.forgotPasswordFormErrors = {
          email: {}
      };
  }

  ngOnInit()
  { 

      this.forgotPasswordForm = this.formBuilder.group({
          email: ['', [Validators.required, Validators.email]]
      });

      this.forgotPasswordForm.valueChanges.subscribe(() => {
          this.onForgotPasswordFormValuesChanged();
      });
  }



  forgotPassword(){
    
    this.forgotPasswordService.forgontPassword(this.email).subscribe(
    response => {
    
        this.snackBar.open(response.message, 'Cerrar', {
            duration: 3000
          });
    },
    err => {    
        this.snackBar.open(err.message, 'Cerrar', {
            duration: 3000
          });
    }
    );
  }

 


  onForgotPasswordFormValuesChanged()
  {
      for ( const field in this.forgotPasswordFormErrors )
      {
          if ( !this.forgotPasswordFormErrors.hasOwnProperty(field) )
          {
              continue;
          }

          // Clear previous errors
          this.forgotPasswordFormErrors[field] = {};

          // Get the control
          const control = this.forgotPasswordForm.get(field);

          if ( control && control.dirty && !control.valid )
          {
              this.forgotPasswordFormErrors[field] = control.errors;
          }
      }
  }
}
