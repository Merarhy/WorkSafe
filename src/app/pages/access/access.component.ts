"use strict";
import { Component, OnInit, ViewChild, Pipe, PipeTransform, ViewEncapsulation } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import * as moment from 'moment';
import {MatDialog, MatSnackBar} from "@angular/material";
import * as _ from 'lodash';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
//import { CompleterService, CompleterData } from 'ng2-completer';
import {LoginService} from '../../pages/login/login.service';
import {AccessService} from "./access.service";
import { Evento } from "./access.model";

moment.locale('es-mx');
const eventDateFormat: string = 'YYYY-MM-DD, HH:mm AM';

@Component({
  selector: 'ms-main-content',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss'],
   providers: [AccessService]
})

export class CalendarComponent implements OnInit {

  event: CalendarEvent;
  locale: string = 'es';
  view: string = 'month';
  edit_mode : Boolean;
  eventos: any[];
  current_event: Evento;
  activeDayIsOpen: boolean = true;
  viewDate: Date = new Date();
  events: any[];

  constructor(
    private calendarService: AccessService,
    private dialogRef: MatDialog,
    private snackBar: MatSnackBar,
    //private completerService: CompleterService,
    private router: Router,
    private loginService: LoginService
  ) {}

 ngOnInit() {

    const ev = new Evento();

    this.calendarService.getAll().subscribe(
          res => {
          this.events = res.map( (el) => {
            return ev.transformFromDB(el);
          });
        },
        e => { console.log(e); }
    );
  }

  dayClicked({date, events}: {date: Date, events: CalendarEvent[]}): void {
      this.view = 'day';
      this.viewDate = date;
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.router.navigate(['/access/solicitud/' + event['_id'] ]);
  }

}

