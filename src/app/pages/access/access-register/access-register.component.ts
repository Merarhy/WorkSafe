import { Component, ViewEncapsulation,OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import {AccessService} from "../access.service";
import {MatDialog, MatSnackBar} from "@angular/material";
import * as moment from 'moment';
import {UsersService as BusinessUserService} from '../../business/users.service';
import { environment } from '../../../../environments/environment';
import { fuseAnimations } from '../../../core/animations';

@Component({
  selector: 'ms-main-content',
  templateUrl: './access-register.component.html',
  styleUrls: ['./access-register.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations,providers: [AccessService, BusinessUserService],
})
export class AccessRegisterComponent implements AfterViewInit {

  @ViewChild( 'myInput' ) private _inputElement: ElementRef;

  private sub: any;

  title: String = '';
  redirect: String = '';

  qr_code: String = '';
  loadingContent: Boolean = false;


  searchChangeObserver;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private accessService: AccessService,
  ) {}

  ngAfterViewInit(): void {
    this.setInputFocus();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      
      if (params['type']) {
          switch (params['type']) {
            case 'id':
              this.title = 'Imprimir ID';
              this.redirect = '/access/id/';
            break;
            case 'user':
              this.title = 'Registro de Usuario';
              this.redirect = '/access/user/';
            break;
            case 'laptop':
              this.title = 'Registro de Laptop';
              this.redirect = '/access/laptop/';
            break;
            case 'device':
              this.title = 'Registro de Dispositivo DataCenter';
              this.redirect = '/access/device/';
            break;
            default:
              this.snackBar.open('Tipo de evento no identificado.', 'Cerrar', {
                duration: 3000
              });
              this.router.navigate(['/admin/']);
          }
      } else {
        this.snackBar.open('Tipo de evento no identificado.', 'Cerrar', {
          duration: 3000
        });
        this.router.navigate(['/admin/']);
      }

    });
  }

  onSearchChange(searchValue: string) {
    if (!this.searchChangeObserver) {
      Observable.create(observer => {
          this.searchChangeObserver = observer;
      })
      .debounceTime(500) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe( () => {
        this.searchEvent();
      });
    }
    this.searchChangeObserver.next(searchValue);
  }

  setInputFocus() {
    setTimeout( () => {
      this._inputElement.nativeElement.focus();
    });
  }

  searchEvent() {
    if ( this.qr_code.length ) {
      this.accessService.readQr( this.qr_code ).subscribe(
        data => {
          if (data && data.event) {
            this.router.navigate([this.redirect + '' + this.qr_code]);
          } else {
            this.qr_code = '';
            this.snackBar.open('El código ingresado es incorrecto.', 'Cerrar', {
              duration: 3000
            });
          }
          //this._setupEvent(data);
        },
        err => {
          this.qr_code = '';
          this.snackBar.open('El código ingresado es incorrecto.', 'Cerrar', {
            duration: 3000
          });
        }
      );
    }
  }


  onCamSuccess(){}

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
