import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { AccessRegisterComponent } from './access-register.component';

@NgModule({
    declarations: [
        AccessRegisterComponent,

        
    ],
    imports     : [
     //  IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
        AccessRegisterComponent
    ]
})

export class AccessRegisterModule
{

}
