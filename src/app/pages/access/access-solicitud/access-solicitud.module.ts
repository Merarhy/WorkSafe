import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { AccessSolicitudComponent } from './access-solicitud.component';

@NgModule({
    declarations: [
      AccessSolicitudComponent
    ],
    imports     : [
        RouterModule,
        SharedModule,
    ],
    exports     : [
      AccessSolicitudComponent
    ]
})

export class AccessSolicitudModule
{

}
