import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import {IMyDpOptions} from 'mydatepicker';
import { Evento } from '../access.model';
import {SettingsCatalogsService} from '../settings-catalogs.service';
import {AccessService} from '../access.service';
import {UsersService} from '../../users/users.service';
import {UsersService as BusinessUserService} from '../../business/users.service';
import {MyDevicesService} from '../../my-business/my-devices/my-devices.service';
import {LoginService} from '../../../pages/login/login.service';
import * as moment from 'moment';
import {MatDialog, MatSnackBar} from '@angular/material';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';
import { environment } from '../../../../environments/environment';
import { fuseAnimations } from './../../../core/animations';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

export const MY_FORMATS = {
  parse  : {
      dateInput: 'LL'
  },
  display: {
      dateInput         : 'YYYY-MM-DD',
      monthYearLabel    : 'YYYY MM',
      dateA11yLabel     : 'YYYY-MM-DD',
      monthYearA11yLabel: 'YYYY MM'
  }
};


@Component({
  selector: 'fuse-main-content',
  templateUrl: './access-solicitud.component.html',
  styleUrls: ['./access-solicitud.component.scss'],
  animations   : fuseAnimations,
  providers: [AccessService, SettingsCatalogsService, UsersService, BusinessUserService, MyDevicesService,
  
  {   provide    : DateAdapter,
      useClass: MomentDateAdapter,
      deps    : [MAT_DATE_LOCALE]
  },

  {provide    : MAT_DATE_FORMATS,
      useValue: MY_FORMATS
  }
  
  ]
})
export class AccessSolicitudComponent implements OnInit {

  @ViewChild( 'myInputExternalEmail' ) private _inputElement: ElementRef;
  @ViewChild( 'myInputBusinessEmail' ) private _inputBusinessElement: ElementRef;
  @ViewChild( 'myInputUserEmail' ) private _inputUserElement: ElementRef;


  // Form
    form: FormGroup;
    formErrors: any;
    isDisabled: boolean
    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
    horizontalStepperStep3Errors: any;


 // date = new FormControl(moment());
  avatarPath: String = environment.apiUrl + 'avatars/';

  offices: any[];
  datacenters: any[];

  new_evento: Evento = new Evento();

  tmp_dates: any = {};

  tmp_fields: any = {
    user_query: '',
    user_suggest: [],
    business_user_query: '',
    business_user_suggest: [],
    external_user: {},
    external_user_query: '',
    external_user_suggest: [],
    new_external: false
  };

  business_devices: any[];
  invites: any = {
    users: [],
    business: [],
    external: []
  };

  
  today: any = moment.utc();
  yesterday: any = moment.utc().subtract(1, 'd');

  calendarInicioOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    todayBtnTxt: 'Hoy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    disableWeekdays: [],
    disableUntil: {year: parseInt( this.yesterday.format('YYYY') ), month: parseInt( this.yesterday.format('M') ), day: parseInt( this.yesterday.format('D') )}
  };

  calendarFinOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    todayBtnTxt: 'Hoy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    disableWeekdays: [],
    disableUntil: {year: parseInt( this.yesterday.format('YYYY') ), month: parseInt( this.yesterday.format('M') ), day: parseInt( this.yesterday.format('D') )}
  };

  searchExternalChangeObserver;
  searchBusinessChangeObserver;
  searchUserChangeObserver;

  constructor(
    private accessService: AccessService,
    private snackBar: MatSnackBar,
    private router: Router,
    private settingsCatalogsService: SettingsCatalogsService,
    private userService: UsersService,
    private businessUserService: BusinessUserService,
    private myDevicesService: MyDevicesService,
    private formBuilder: FormBuilder,
    private loginService: LoginService
  ) {
    

    // Get catalogs
    this.settingsCatalogsService.getCollection('settings', 'Location').subscribe(
      offices => {
        this.offices = offices;
        if ( this.offices && this.offices[0] ) {
          this.new_evento.sede = this.offices[0].name;
        }
      },
      err => { console.log(err); }
    );
    /*
    this.settingsCatalogsService.getCollection('infra', 'Datacenter').subscribe(
      datacenters => {
        this.datacenters = datacenters;
      },
      err => { console.log(err); }
    );
    */
    
    // Validate is business user
    if ( this.loginService.identity.business ) {
      // Get catalogs
      this.settingsCatalogsService.getCollection('infra', 'Devicetype').subscribe(
        deviceTypes => {
          this.myDevicesService.getDevices().subscribe(
            devices => {
              if (devices && devices['data']) {
                this.business_devices = devices['data'].map( el => {
                  el._devicetype = deviceTypes.find( dv => dv._id === el._devicetype );
                  return el;
                });
              }
            },
            err => { console.log(err); }
          );
        },
        err => { console.log(err); }
      );
    }

    
  }

  ngOnInit() {

    setTimeout( () => {
      this._initTimes();
    }, 1000);
  }
  submmited = false;
  onSubmit(){
    this.submmited=true;
  }

  _initTimes() {
    this.new_evento = new Evento();
    if ( this.offices && this.offices[0] ) {
      this.new_evento.sede = this.offices[0].name;
    }
    
    const d = moment().add(1, 'days');
    this.tmp_dates = {
      start_date: d,
      end_date: d,
      hora_inicio: '09:00',
      hora_fin: '10:00'
    };

    this.tmp_fields = {
      user_query: '',
      user_suggest: [],
      business_user_query: '',
      business_user_suggest: [],
      external_user: {},
      external_user_query: '',
      external_user_suggest: [],
      new_external: false
    };

    this.invites = {
      users: [],
      business: [],
      external: []
    };

    this.setupDate('start_date');
    this.setupDate('end_date');

    if (this.loginService.identity.business) {
      this.tmp_fields.business_user_query = this.loginService.identity.email;
      this.searchBusinessUser(true);
    } else {
      this.tmp_fields.user_query = this.loginService.identity.email;
      this.searchUser(true);
    }
  }

  userCanAuthorize() {
    if ( this.new_evento.type === 'datacenter' ) {
      return 'Solicitar Visita';
    } else {
      if ( this.loginService.identity.business ) {
        return 'Solicitar Visita';
      } else {
        return 'Guardar y Autorizar';
      }
    }
  }

  onSearchExternalChange(searchValue: string) {
    if (!this.searchExternalChangeObserver) {
      Observable.create(observer => {
          this.searchExternalChangeObserver = observer;
      })
      .debounceTime(500) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe( () => {
        this.searchExternalUser();
      });
    }
    this.searchExternalChangeObserver.next(searchValue);
  }

  onSearchBusinessChange(searchValue: string) {
    if (!this.searchBusinessChangeObserver) {
      Observable.create(observer => {
          this.searchBusinessChangeObserver = observer;
      })
      .debounceTime(500) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe( () => {
        this.searchBusinessUser();
      });
    }
    this.searchBusinessChangeObserver.next(searchValue);
  }

  onSearchUserChange(searchValue: string) {
    if (!this.searchUserChangeObserver) {
      Observable.create(observer => {
          this.searchUserChangeObserver = observer;
      })
      .debounceTime(500) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe( () => {
        this.searchUser();
      });
    }
    this.searchUserChangeObserver.next(searchValue);
  }

  getSalas(office){
    if ( !office ){
      return [];      
    }
    const site = this.offices.find( (el) => el.name === office );
    if ( site && site.rooms ) {
      return site.rooms;
    } else {
      return [];
    }
  }

  filteredDatacenters() {
    if ( !this.loginService.identity.business ) {
      return this.datacenters;
    }
    if ( !this.loginService.identity.dc_access || !this.loginService.identity.dc_access.datacenters ) {
      return [];
    }
    return this.datacenters.filter( el => {
      return ( this.loginService.identity.dc_access.datacenters.indexOf( el._id ) !== -1 );
    });
  }

  getFases(dc){
    if ( !dc ) {
      return [];
    }
    const site = this.datacenters.find( (el) => el.name === dc );
    if ( site && site.fases ) {
      return site.fases;
    } else {
      return [];
    }
  }

  setupDate(type) {

    switch (type) {
      case 'start_date':
        if (this.tmp_dates.start_date){
          this.new_evento.start_date = moment.utc(this.tmp_dates.start_date.format('YYYY-MM-DD') + ' ' + this.tmp_dates.hora_inicio + ':00').toDate();
        } else {
          this.new_evento.start_date = null;
        }
        // Predefine end date
        if ( !this.new_evento.end_date ) {
          this.tmp_dates.end_date = this.tmp_dates.start_date;
        }

        const d = moment.utc( this.new_evento.start_date ).subtract(1, 'd');

        

        // If start day is bigger than end day
        if ( moment.utc(this.new_evento.start_date).unix() >= moment.utc(this.new_evento.end_date).unix() ) {
          this.tmp_dates.end_date = this.tmp_dates.start_date;
          const hours = this.loginService.available_user_hours(this.new_evento.type);
          this.tmp_dates.hora_fin = (hours && hours[hours.findIndex( el => el === this.tmp_dates.hora_inicio)+1]) ? hours[hours.findIndex( el => el==this.tmp_dates.hora_inicio)+1] : '';
        }

      break;
      case 'end_date':
        if (this.tmp_dates.end_date){
          this.new_evento.end_date = moment.utc(this.tmp_dates.end_date.format('YYYY-MM-DD') + ' ' + this.tmp_dates.hora_fin + ':00' ).toDate();
        } else {
          this.new_evento.end_date = null;
        }
        // If start day is bigger than end day
        if ( moment.utc(this.new_evento.start_date).unix() >= moment.utc(this.new_evento.end_date).unix() ) {
          this.tmp_dates.start_date = this.getCopyOfOptions(this.tmp_dates.end_date);
          const hours = this.loginService.available_user_hours(this.new_evento.type);
          this.tmp_dates.hora_inicio = (hours && hours[hours.findIndex( el => el === this.tmp_dates.hora_inicio)-1]) ? hours[hours.findIndex( el => el==this.tmp_dates.hora_inicio)-1] : '';
        }
      break;
    }
  }

  searchUser(prefill?: Boolean) {
    
    if ( this.tmp_fields.user_query && this.tmp_fields.user_query.length >= 1 ) {
      this.userService.searchUser(this.tmp_fields.user_query).subscribe(
        users => {
          this.tmp_fields.user_suggest = users;
          if ( prefill && this.tmp_fields.user_suggest && this.tmp_fields.user_suggest.length > 0) {
            this.selectUser( this.tmp_fields.user_suggest[0] );
          }
        },
        err => { console.log(err); }
      );
    } else {
      this.tmp_fields.user_suggest = [];
    }
    
  }

  selectUser(user) {
    this.tmp_fields.user_query = '';
    this.tmp_fields.user_suggest = [];
    if (!this.new_evento.users_invitations) {
      this.new_evento.users_invitations = [];
    }
    this.tmp_fields.user = {};
    this.removeUser( user._id );
    this.invites.users.push(user);
    this.new_evento.users_invitations.push({_user: user._id});
  }

  searchInfraUser() {
    if (this.tmp_fields.infra_user_query && this.tmp_fields.infra_user_query.length) {
      this.userService.searchUser(this.tmp_fields.infra_user_query).subscribe(
        user => {
          if (user && user[0] && user[0]._id) {
            this.new_evento._infra_user = user[0]._id;
            this.tmp_fields.infra_user = user[0];
            this.tmp_fields.infra_user_query = '';
          } else {
            this.snackBar.open('No se ha encontrado el usuario.', 'Cerrar', {
              duration: 3000
            });
          }
        },
        err => { console.log(err); }
      );
    }
  }

  searchBusinessUser(prefill?: Boolean) {
    if ( (this.loginService.validatePermissionTree('tools', 'businessusers', 'search') || this.loginService.validatePermissionTree('business', 'users', 'read')) ) {
      if ( this.tmp_fields.business_user_query && this.tmp_fields.business_user_query.length >= 1) {
        this.businessUserService.searchUser(this.tmp_fields.business_user_query).subscribe(
          users => {
            this.tmp_fields.business_user_suggest = users;
            if ( prefill && this.tmp_fields.business_user_suggest && this.tmp_fields.business_user_suggest.length > 0) {
              this.selectBusinessUser( this.tmp_fields.business_user_suggest[0] );
            }
          },
          err => { console.log(err); }
        );
      } else {
        this.tmp_fields.business_user_suggest = [];
      }
    } else {
      this.snackBar.open('No tienes permisos para buscar usuarios.', 'Cerrar', {
        duration: 3000
      });
    }

  }

  selectBusinessUser(user) {
    this.tmp_fields.business_user_query = '';
    this.tmp_fields.business_user_suggest = [];
    if (!this.new_evento.business_users_invitations) {
      this.new_evento.business_users_invitations = [];
    }
    this.tmp_fields.business_user = {};
    this.removeBusinessUser( user._id );
    this.invites.business.push(user);
    this.new_evento.business_users_invitations.push({_business_user: user._id});
  }

  searchExternalUser() {
    if (this.loginService.validatePermissionTree('access', 'externaluser', 'read') && this.tmp_fields.external_user_query && this.tmp_fields.external_user_query.length >= 1) {
      this.accessService.searchExternalUser(this.tmp_fields.external_user_query).subscribe(
        users => {
          this.tmp_fields.external_user_suggest = users;
        },
        err => { console.log(err); }
      );
    } else {
      this.tmp_fields.external_user_suggest = [];
    }
  }

  removeUser( user_id ) {
    this.new_evento.users_invitations = this.new_evento.users_invitations.filter( el => el._user !== user_id );
    this.invites.users = this.invites.users.filter( el => el._id !== user_id);
  }

  removeInfraUser() {
    delete this.tmp_fields.infra_user;
    delete this.new_evento._infra_user;
  }

  removeBusinessUser( user_id ) {
    this.new_evento.business_users_invitations = this.new_evento.business_users_invitations.filter( el => el._business_user !== user_id );
    this.invites.business = this.invites.business.filter( el => el._id !== user_id);
  }

  addExternalUser() {

    if (!this.tmp_fields.external_user.name || !this.tmp_fields.external_user.email) {
      alert('El nombre y el email son requeridos');
      return;
    }

    this.accessService.addExternalUser(this.tmp_fields.external_user).subscribe(
      (user) => {
        if (user._id) {
          if (!this.new_evento.external_invitations) {
            this.new_evento.external_invitations = [];
          }
          this.tmp_fields.external_user = {};
          this.removeExternalUser( user._id );
          this.invites.external.push(user);
          this.new_evento.external_invitations.push({_external_user: user._id});
          this.cancelAddExternalUser();
        } else {
          let message = 'Ha ocurrido un error al crear el usuario';
          if ( user.message ) {
            message = user.message;
          }
          this.snackBar.open(message, 'Cerrar', {
            duration: 3000
          });
        }
      },
      (err) => {
        let message = 'Ha ocurrido un error al crear el usuario';
        if ( err.message ) {
          message = err.message;
        }
        this.snackBar.open(message, 'Cerrar', {
          duration: 3000
        });
      }
    );

  }

  cancelAddExternalUser() {
    this.tmp_fields.external_user_query = '';
    this.tmp_fields.external_user = {};
    this.tmp_fields.new_external = false;
  }

  selectExternalUser(user) {
    this.tmp_fields.external_user_query = '';
    this.tmp_fields.external_user_suggest = [];
    if (!this.new_evento.external_invitations) {
      this.new_evento.external_invitations = [];
    }
    this.tmp_fields.external_user = {};
    this.removeExternalUser( user._id );
    this.invites.external.push(user);
    this.new_evento.external_invitations.push({_external_user: user._id});
  }

  createExternalUser() {
    this.tmp_fields.new_external = true;
    this.tmp_fields.external_user.email = this.tmp_fields.external_user_query;
  }

  removeExternalUser(user_id) {
    this.new_evento.external_invitations = this.new_evento.external_invitations.filter( el => el._external_user !== user_id );
    this.invites.external = this.invites.external.filter( el => el._id !== user_id);
  }

  resetDeviceRequests(event) {
    if (event) {
      this.new_evento.device_requests = {
        ingreso: [],
        salida: [],
        no_registrado: false
      };
    } else {
      delete this.new_evento.device_requests;
    }
  }

  resetDeviceRequestsByType() {
    const copyInicio = this.getCopyOfOptions( this.calendarInicioOptions );
    const copyFin = this.getCopyOfOptions( this.calendarFinOptions );
    if ( this.new_evento.type !== 'datacenter' ) {
      delete this.new_evento.device_manage;
      delete this.new_evento.device_requests;
      copyInicio.disableWeekdays = [];
      copyFin.disableWeekdays = [];
      this.calendarInicioOptions = copyInicio;
      this.calendarFinOptions = copyFin;
    } else {
      // Protect unavailable access days
      copyInicio.disableWeekdays = this.loginService.unavailableDays();
      copyFin.disableWeekdays = this.loginService.unavailableDays();
      this.calendarInicioOptions = copyInicio;
      this.calendarFinOptions = copyFin;
      this.resetDays();
    }
    // delete this.new_evento.sede;
    // delete this.new_evento.sala;
  }

  getCopyOfOptions(item): any {
    return JSON.parse(JSON.stringify(item));
  }

  resetDays(): any {
    if ( !this.loginService.identity.business || !this.loginService.identity.dc_access || !this.loginService.identity.dc_access.days ) {
      return;
    }
    let valid_day = this.loginService.identity.dc_access.days.indexOf( this.new_evento.start_date.getDay() );
    if ( valid_day === -1 ) {
      this.tmp_dates.start_date = null;
      // find valid day starting from today
      let tries = 1;
      while ( valid_day === -1 && tries < 7 ) {
        valid_day = this.loginService.identity.dc_access.days.indexOf( moment().add( tries, 'days').toDate().getDay() );
        
        if ( valid_day !== -1 ) {
          const d = moment().add( tries, 'days');
          this.tmp_dates.start_date = { date: { year: d.format('YYYY'), month: d.format('M'), day: d.format('D') } };
        }
        tries++;
      }
      this.setupDate('start_date');

      this.tmp_dates.end_date = this.tmp_dates.start_date;
      this.setupDate('end_date');
    }

    const hours = this.loginService.available_user_hours(this.new_evento.type);
    this.tmp_dates.hora_inicio = (hours && hours.length) ? hours[0] : '';
    this.tmp_dates.hora_fin = (hours && hours.length) ? hours[1] : '';
    this.setupDate('start_date');
    this.setupDate('end_date');
  }


  filterDevices(status) {
    if ( !this.business_devices ) {
      return [];
    }
    return this.business_devices.filter( el => el.status === status );
  }

  addOrRemoveDevice(checked, device_id, type) {
    if ( checked ) {
      if ( this.new_evento.device_requests[type].indexOf(device_id) < 0 ) {
        this.new_evento.device_requests[type].push(device_id);
      }
    } else {
      if ( this.new_evento.device_requests[type].indexOf(device_id) !== -1 ) {
        this.new_evento.device_requests[type].splice(this.new_evento.device_requests[type].indexOf(device_id), 1);
      }
    }
  }

  saveEvent() {
    /*
    if (this.new_evento.type !== 'datacenter' && (!this.new_evento.users_invitations || !this.new_evento.users_invitations.length) ) {
      alert('Debes incluir al evento al menos a una persona de SDR');
      return;
    }
    */
   // console.log(this.new_evento)

    this.accessService.saveOrUpdate(this.new_evento).subscribe(
      (res) => {
        if (res._id) {
          this._initTimes();
          
          this.snackBar.open('El evento ha sido guardado', 'Cerrar', {
            duration: 3000
          });
          this.loginService.goBack();
        } else {
          this.snackBar.open('Ha ocurrido un error al guardar el evento', 'Cerrar', {
            duration: 3000
          });
        }
      },
      (err) => {
        this.snackBar.open('Ha ocurrido un error al guardar el evento', 'Cerrar', {
          duration: 3000
        });
      }
    );



  }

  onFormValuesChanged()
  {
      for ( const field in this.formErrors )
      {
          if ( !this.formErrors.hasOwnProperty(field) )
          {
              continue;
          }

          // Clear previous errors
          this.formErrors[field] = {};

          // Get the control
          const control = this.form.get(field);

          if ( control && control.dirty && !control.valid )
          {
              this.formErrors[field] = control.errors;
          }
      }
  }

}
