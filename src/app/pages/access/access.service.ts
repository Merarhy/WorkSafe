import { Injectable } from '@angular/core';
import { Evento } from './access.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class AccessService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    getCommingLocationEvents(): Observable<Evento[]> {
        return this.http.get(`${environment.apiUrl}access/location/comming`, this.requestOptions)
            .map( mapEventos );
    }

    getLasts(): Observable<Evento[]> {
        return this.http.get(`${environment.apiUrl}access/lasts`, this.requestOptions)
            .map( mapEventos );
    }
    getPending(): Observable<Evento[]> {
        return this.http.get(`${environment.apiUrl}access/pending`, this.requestOptions)
            .map( mapEventos );
    }
    getAll(): Observable<Evento[]> {
        return this.http.get(`${environment.apiUrl}access`, this.requestOptions)
            .map( mapEventos );
    }


    getEventoById(evento_id: string) : Observable<Evento>{
        return this.http.get(`${environment.apiUrl}access/${evento_id}`, this.requestOptions)
            .map( extractData );
    }


    saveOrUpdate(evento: Evento) : Observable<any> {
        if (evento._id) {
            return this.http.patch(`${environment.apiUrl}access/${evento._id}`, evento, this.requestOptions).map( extractData );
        }else{
            return this.http.post(`${environment.apiUrl}access`, evento, this.requestOptions).map( extractData );
        }
    }

    updateAssignation(_event_id: String, _user_id: String): Observable<any> {
        return this.http.patch(`${environment.apiUrl}access/${_event_id}/assign-engineer`, {_user: _user_id}, this.requestOptions)
            .map( extractData );
    }

    addAuthorization(evento: Evento, type: string): Observable<any> {
        return this.http.patch(`${environment.apiUrl}access/${evento._id}/dc-authorize/${type}`, {}, this.requestOptions).map( extractData );
    }

    searchExternalUser( email: string ): Observable<any> {
        return this.http.get(`${environment.apiUrl}access/external-user/${email}`, this.requestOptions)
            .map( mapAny );
    }

    addExternalUser(user: any): Observable<any> {
        return this.http.post(`${environment.apiUrl}access/external-user`, user, this.requestOptions).map( extractData );
    }

    updateExternalUserAvatar(userId: string, avatar: string): Observable<any> {
        return this.http.patch(`${environment.apiUrl}access/external-user/${userId}/avatar`, {avatar: avatar}, this.requestOptions).map( extractData );
    }

    readQr(qr_code: String): Observable<any> {
        return this.http.get(`${environment.apiUrl}access/qr/${qr_code}`, this.requestOptions)
            .map( extractData );
    }

    /*
    delete(evento: Evento) : Observable<any> {
        if(evento._id) {
            return this.http.delete(`${environment.apiUrl}access/${evento._id}`, this.requestOptions).map( extractData );
        }
    }
    */

    registerUserEvent(ingreso: Boolean, qr_code: String): Observable<any> {
        if ( ingreso ) {
            return this.http.get(`${environment.apiUrl}access/registration/entry/${qr_code}`, this.requestOptions)
            .map( extractData );
        } else {
            return this.http.get(`${environment.apiUrl}access/registration/exit/${qr_code}`, this.requestOptions)
            .map( extractData );
        }
    }

    registerDeviceEvent(ingreso: Boolean, qr_code: String, device: any): Observable<any> {
        if ( ingreso ) {
            return this.http.post(`${environment.apiUrl}access/device-registration/entry/${qr_code}`, device, this.requestOptions)
            .map( extractData );
        } else {
            return this.http.post(`${environment.apiUrl}access/device-registration/exit/${qr_code}`, device, this.requestOptions)
            .map( extractData );
        }
    }

    /* GET DatCenter Devices to manage on current Event */
    getDatacenterDevices(qr_code: String): Observable<any> {
        return this.http.get(`${environment.apiUrl}/access/dc-devices/${qr_code}`, this.requestOptions)
            .map( extractData );
    }

    /* Change Device Datacenter Status */
    registerDeviceDatacenterEvent(qr_code: String, device: any): Observable<any> {
        return this.http.patch(`${environment.apiUrl}access/dc-device-registration/${qr_code}`, device, this.requestOptions)
        .map( extractData );
    }

}


function mapAny(response:Response): any[] {
    // The response of the API has a results
    // property with the actual results
    return response.json().data;
}

function extractData(res: Response): any {
    let body = res.json();
    try {
        body = body.map(toEvento);
    } catch (error) {}
    return body || { };
}

function mapEventos(response:Response): Evento[] {
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toEvento);
}




function toEvento(r:any): Evento{
    var options = {};
    for(var key in r){
      options[key] = r[key];
    }
    let evento = <Evento>(r);
    return evento;
}
