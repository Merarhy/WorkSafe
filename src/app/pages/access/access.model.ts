import * as moment from 'moment';
 
let eventDateFormat2: string = 'YYYY-MM-DD';

export class Evento {
  _id: any;
  type: any = {
    acceso_edificio: Boolean,
    datacenter: Boolean,  
    comunicaiones: Boolean, 
    registros_vitales: Boolean, 
    posiciones_alternas: Boolean, 
    areas_comunes: Boolean, 
  };
  name_event: String;
  _user: any;
  _business_user: any;
  name_business: String;
  descripcion: String;
  sede: string;
  sala: String[];
  device_manage: Boolean;
  device_requests: {
      ingreso: String[],
      salida:  String[],
      no_registrado:  Boolean
      };
  users_invitations: any[];
  business_users_invitations: any[];
  external_invitations: any[];
  start_date: Date;
  end_date: Date;
  _servicedesk_user: String;
  _security_user: String;
  _infra_user: String;
  _user_authorize: String; // User whom authorize the event
  _authorized_date: Date;
  status: String;

  constructor(model: any = null) {
    for(var key in model){
      this[key] = model[key];
    }
    if ( !model || !model.type ) {
      this.type = {
        acceso_edificio: false,
        datacenter: false,  
        comunicaiones: false, 
        registros_vitales: false, 
        posiciones_alternas: false, 
        areas_comunes: false
      }
    }
  }

  transformFromDB(data: any) {
    var offset = moment().utcOffset();
    var localText = moment(data.start_date).local().utcOffset(offset);
    data.start_date = new Date(data.start_date);
    var tmp_date = new Date ( data.start_date.getTime() + (data.start_date.getTimezoneOffset() * 60000) );

    let tmp = {
      _id: data._id,
      start: moment(tmp_date),
      ignoreTimezone: true,
      title: data.name_event,
      color: 'red'
    };
    return tmp;
  }
}