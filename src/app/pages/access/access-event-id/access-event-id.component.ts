import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MatDialogRef } from "@angular/material";
import { ActivatedRoute, Router } from "@angular/router";
import {IMyDpOptions} from 'mydatepicker';
import { Evento } from "../access.model";
import {SettingsCatalogsService} from '../settings-catalogs.service';
import { AccessService } from "../access.service";
import {UsersService} from '../../users/users.service';
import {UsersService as BusinessUserService} from '../../business/users.service';
import {LoginService} from '../../../pages/login/login.service';
import {MyDevicesService} from '../../my-business/my-devices/my-devices.service';
import { MatSnackBar } from "@angular/material";
import { environment } from '../../../../environments/environment';
import * as moment from 'moment';
import * as _ from 'lodash';
import { fuseAnimations } from './../../../core/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';


@Component({
  selector: 'ms-main-content',
  templateUrl: './access-event-id.component.html',
  styleUrls: ['./access-event-id.component.scss'],
  animations: fuseAnimations,
  providers: [AccessService, SettingsCatalogsService, UsersService, BusinessUserService, MyDevicesService,
    {
      provide : DateAdapter,
      useClass: MomentDateAdapter,
      deps    : [MAT_DATE_LOCALE]
    },
    {
      provide : MAT_DATE_FORMATS,
      useValue: MAT_MOMENT_DATE_FORMATS
    }
  ]
})
export class AccessEventIdComponent implements OnInit {

  @ViewChild( 'myInputExternalEmail' ) private _inputElement: ElementRef;
  @ViewChild( 'myInputBusinessEmail' ) private _inputBusinessElement: ElementRef;
  @ViewChild( 'myInputUserEmail' ) private _inputUserElement: ElementRef;

    form: FormGroup;
    formErrors: any;
    date = new FormControl(moment([2017, 0, 1]));

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep1Errors: any;
    horizontalStepperStep2Errors: any;
    horizontalStepperStep3Errors: any;

  avatarPath: String = environment.apiUrl + 'avatars/';
  
  offices: any[];
  datacenters: any[];
  available_hours: string[] = ['00:00','00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30',
                              '12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'];

                              
  sub: any;

  evento: Evento = new Evento();

  full_evento: any;

  tmp_dates: any = {};

  tmp_fields: any = {
    user_query: '',
    user_suggest: [],
    business_user_query: '',
    business_user_suggest: [],
    external_user: {},
    external_user_query: '',
    external_user_suggest: [],
    new_external: false
  };

  my_invite: string;

  business_devices: any[];
  invites: any = {
    users: [],
    business: [],
    external: []
  };
  qr_url: any;

  edit_authorized: Boolean = false;

  loadingContent: boolean = true;

  
  today: any = moment.utc();
  yesterday: any = moment.utc().subtract(1, 'd');

  calendarInicioOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    todayBtnTxt: 'Hoy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    disableWeekdays: [],
    disableUntil: {year: parseInt( this.yesterday.format('YYYY') ), month: parseInt( this.yesterday.format('M') ), day: parseInt( this.yesterday.format('D') )}
  };

  calendarFinOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    todayBtnTxt: 'Hoy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    disableWeekdays: [],
    disableUntil: {year: parseInt( this.yesterday.format('YYYY') ), month: parseInt( this.yesterday.format('M') ), day: parseInt( this.yesterday.format('D') )}
  };


  searchExternalChangeObserver;
  searchBusinessChangeObserver;
  searchUserChangeObserver;
 

  constructor(
  private route: ActivatedRoute,
  private router: Router,
  private accessService: AccessService, 
  private snackBar: MatSnackBar,
  private settingsCatalogsService: SettingsCatalogsService,
  private userService: UsersService,
  private businessUserService: BusinessUserService,
  private myDevicesService: MyDevicesService,
  private loginService: LoginService
  ) {
    
    // Get catalogs
    this.settingsCatalogsService.getCollection('settings', 'Location').subscribe(
      offices => {
        this.offices = offices;
      },
      err => { console.log(err); }
    );
    /*
    this.settingsCatalogsService.getCollection('infra', 'Datacenter').subscribe(
      datacenters => {
        this.datacenters = datacenters;
      },
      err => { console.log(err); }
    );
    */
    // Validate is business user
    if ( this.loginService.identity.business ) {
      // Get catalogs
      this.settingsCatalogsService.getCollection('infra', 'Devicetype').subscribe(
        deviceTypes => {
          this.myDevicesService.getDevices().subscribe(
            devices => {
              if (devices && devices['data']) {
                this.business_devices = devices['data'].map( el => {
                  el._devicetype = deviceTypes.find( dv => dv._id === el._devicetype );
                  return el;
                });
              }
            },
            err => { console.log(err); }
          );
        },
        err => { console.log(err); }
      );
    }
  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {

      if (params['id']) {
          this.accessService.getEventoById(params['id']).subscribe(
            event => {
              if (event && event['event']._id) {
                this.evento = event['event'];
                // Create full object
                this.full_evento = event['details'];
                // Initialize item
                this._convertEvent();
                console.log(event)
              } else {
                this.snackBar.open('El evento que buscas no existe.', 'Cerrar', {
                  duration: 3000
                });
                this.router.navigate(['/access']);
              }
              this.loadingContent = false;
            },
            error => {
              this.loadingContent = false;
              this.snackBar.open('Ha ocurrido un error al cargar el evento.', 'Cerrar', {
                duration: 3000
              });
              this.router.navigate(['/access']);
            }
          );
      } else {
        this.router.navigate(['/access']);
      }

    });

  }

  // Convert to a simple unpopulated object
  _convertEvent() {
    // Get my_invite code
    if (this.loginService.identity.business) {
      // Validate if belongs to this appointment as business user
      var invite = this.evento.business_users_invitations.find( el => el._business_user === this.loginService.identity._id );
      if (invite) {
        this.my_invite = invite._id;
        this.qr_url = environment.apiUrl + 'files/access/' + this.evento._id + '/invite/' + invite._id + '/type/business';
      }
    } else {
      // Validate if belongs to this appointment as sdr user
      var invite = this.evento.users_invitations.find( el => el._user === this.loginService.identity._id );
      if (invite) {
        this.my_invite = invite._id;
      }
    }

    // Setup organizador
    var organizer = '';
    if (this.full_evento._user) {
      organizer = '_user';
    }
    if (this.full_evento._business_user) {
      organizer = '_business_user';
    }
    if ( organizer ) {
      this.tmp_fields.organizador = this.full_evento[organizer];
    }

    // Setup dates
    var init_date = moment.utc(this.full_evento.start_date);
    var end_date = moment.utc(this.full_evento.end_date);

    this.tmp_fields.start_date = init_date;
    this.tmp_fields.end_date = end_date;

    this.tmp_dates = {
      start_date: init_date,
      end_date: end_date,
      hora_inicio: init_date.format('HH:mm'),
      hora_fin: end_date.format('HH:mm')
    };
  
    if ( this.full_evento.business_users_invitations ) {
      this.loadingContent = false;
      this.full_evento.business_users_invitations.forEach( el => {
        if (el.qr && el._business_user) {
          el._business_user.qr = el.qr;
        }
        if ( el._business_user ) {
          this.invites.business.push(el._business_user);
        }
      });
    }
    if (this.full_evento.users_invitations ) {
      this.full_evento.users_invitations.forEach( el => {
        this.loadingContent = false;
        if (el.qr) {
          el._user.qr = el.qr;
        }
        if ( el._user ) {
          this.invites.users.push(el._user);
        }

      });
    }
    if (this.full_evento.external_invitations) {
      this.full_evento.external_invitations.forEach( el => {
        this.loadingContent = false;
        if (el.qr) {
          el._external_user.qr = el.qr;
        }
        if ( el._external_user ) {
          this.invites.external.push(el._external_user);
        }
        
      });
    }

    this.tmp_fields.infra_user = this.full_evento._infra_user;


    this.setupDate('start_date');
    this.setupDate('end_date');

  }

  userEdithAuthorized() {


    //console.log(this.my_invite)
    // First validate user belongs to invitation
    if ( this.my_invite ) {
      // Validate if is SDR user, then has permissions
      if ( !this.loginService.identity.business ) {
        return true;


     
      } else {
        // if business user
        // validate only organizer can edit when is not autorized
        if ( this.tmp_fields && this.tmp_fields.organizador  ) {
          return true;
        } else {
          return false;
        }
      }
    }  
    /*else {
     
      // If event is datacenter, also infra coord in location can edit
      if ( this.evento.type === 'datacenter' ) {
        // Get user location
      
        var location_keywe_name = [];
        if ( this.offices && this.offices.length && this.loginService.identity.keywe ) {
          location_keywe_name = this.offices.filter( el => el.keywe_name === this.loginService.identity.keywe.ubicacion ).map( el => el.name );
        }
        if( this.loginService.hasPermission('access-profile-coordinfra') && ( location_keywe_name.indexOf( this.full_evento.sede ) !== -1 ) ) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
      
    }*/

  }

  disabledModifyRequestType() {
    if (
      !this.my_invite ||
      (this.loginService.identity.business && !this.loginService.identity.dc_access)
    ){
      return true;
    } else {
      return false;
    }
  }

  onSearchUserChange(searchValue: string) {
    if (!this.searchUserChangeObserver) {
      Observable.create(observer => {
          this.searchUserChangeObserver = observer;
      })
      .debounceTime(500) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe( () => {
        this.searchUser();
      });
    }
    this.searchUserChangeObserver.next(searchValue);
  }

  onSearchBusinessChange(searchValue: string) {
    if (!this.searchBusinessChangeObserver) {
      Observable.create(observer => {
          this.searchBusinessChangeObserver = observer;
      })
      .debounceTime(500) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe( () => {
        this.searchBusinessUser();
      });
    }
    this.searchBusinessChangeObserver.next(searchValue);
  }

  onSearchExternalChange(searchValue: string) {
    if (!this.searchExternalChangeObserver) {
      Observable.create(observer => {
          this.searchExternalChangeObserver = observer;
      })
      .debounceTime(500) // wait 300ms after the last event before emitting last event
      .distinctUntilChanged() // only emit if value is different from previous value
      .subscribe( () => {
        this.searchExternalUser();
      });
    }
    this.searchExternalChangeObserver.next(searchValue);
  }





  setupDate(type) {
    switch (type) {
      case 'start_date':
        if (this.tmp_dates.start_date && this.tmp_dates.start_date.date && this.tmp_dates.start_date.date.year){
          this.evento.start_date = moment.utc(this.tmp_dates.start_date.date.year+'-'+('0' + (this.tmp_dates.start_date.date.month)).slice(-2)+'-'+('0' + (this.tmp_dates.start_date.date.day)).slice(-2)+' '+this.tmp_dates.hora_inicio+':00').toDate();
        } else {
          this.evento.start_date = null;
        }
        // Predefine end date
        if ( !this.evento.end_date ) {
          this.tmp_dates.end_date = this.getCopyOfOptions(this.tmp_dates.start_date);
        }

        var d = moment.utc( this.evento.start_date ).subtract(1, 'd');

        this.calendarFinOptions.disableUntil = {year: parseInt( d.format('YYYY') ), month: parseInt( d.format('M') ), day: parseInt( d.format('D') )}

        // If start day is bigger than end day
        if ( moment.utc(this.evento.start_date).unix() >= moment.utc(this.evento.end_date).unix() ) {
          this.tmp_dates.end_date = this.getCopyOfOptions(this.tmp_dates.start_date);
          const hours = this.loginService.available_user_hours(this.evento.type);
          this.tmp_dates.hora_fin = (hours && hours[hours.findIndex( el => el === this.tmp_dates.hora_inicio)+1]) ? hours[hours.findIndex( el => el==this.tmp_dates.hora_inicio)+1] : '';
        }

      break;
      case 'end_date':
        if (this.tmp_dates.end_date && this.tmp_dates.end_date.date && this.tmp_dates.end_date.date.year){
          this.evento.end_date = moment.utc(this.tmp_dates.end_date.date.year+'-'+('0' + (this.tmp_dates.end_date.date.month)).slice(-2)+'-'+('0' + (this.tmp_dates.end_date.date.day)).slice(-2)+' '+this.tmp_dates.hora_fin+':00').toDate();
        } else {
          this.evento.end_date = null;
        }
        // If start day is bigger than end day
        if ( moment.utc(this.evento.start_date).unix() >= moment.utc(this.evento.end_date).unix() ) {
          this.tmp_dates.start_date = this.getCopyOfOptions(this.tmp_dates.end_date);
          const hours = this.loginService.available_user_hours(this.evento.type);
          this.tmp_dates.hora_inicio = (hours && hours[hours.findIndex( el => el === this.tmp_dates.hora_inicio)-1]) ? hours[hours.findIndex( el => el==this.tmp_dates.hora_inicio)-1] : '';
        }
      break;
    }
  }

  searchUser() {
    
    if (this.tmp_fields.user_query && this.tmp_fields.user_query.length) {
      this.userService.searchUser(this.tmp_fields.user_query).subscribe(
        users => {
          this.tmp_fields.user_suggest = users;
        },
        err => { console.log(err); }
      );
    } else {
      this.tmp_fields.ser_suggest = [];
    }
    

  }

  searchInfraUser() {
    if (this.tmp_fields.infra_user_query && this.tmp_fields.infra_user_query.length) {
      this.userService.searchUser(this.tmp_fields.infra_user_query).subscribe(
        user => {
          if (user && user[0] && user[0]._id) {
            this.accessService.updateAssignation( this.evento._id, user[0]._id ).subscribe(
              assign => {
                if ( assign._id ) {
                  this.evento._infra_user = user[0]._id;
                  this.tmp_fields.infra_user = user[0];
                  this.tmp_fields.infra_user_query = '';
                  this.snackBar.open('El usuario ha sido asignado', 'Cerrar', {
                    duration: 3000
                  });
                } else {
                  this.snackBar.open('Ha ocurrido un error al asignar el usuario.', 'Cerrar', {
                    duration: 3000
                  });
                }
              },
              err => {
                this.snackBar.open('Ha ocurrido un error al asignar el usuario.', 'Cerrar', {
                  duration: 3000
                });
              }
            );
          } else {
            this.snackBar.open('No se ha encontrado el usuario.', 'Cerrar', {
              duration: 3000
            });
          }
        },
        err => { console.log(err); }
      );
    }
  }

  searchBusinessUser() {
    
    if ( (this.loginService.validatePermissionTree('tools','businessusers','search') || this.loginService.validatePermissionTree('business','users','read')) ) {
      if ( this.tmp_fields.business_user_query && this.tmp_fields.business_user_query.length>=1) {
        this.businessUserService.searchUser(this.tmp_fields.business_user_query).subscribe(
          users => {
            this.tmp_fields.business_user_suggest = users;
          },
          err => { console.log(err); }
        );
      } else {
        this.tmp_fields.business_user_suggest = [];
      }
    } else {
      this.snackBar.open('No tienes permisos para buscar usuarios.', 'Cerrar', {
        duration: 3000
      });
    }
  
  }

  selectUser(user) {
    this.tmp_fields.user_query = '';
    this.tmp_fields.user_suggest = [];
    if (!this.evento.users_invitations) {
      this.evento.users_invitations = [];
    }
    this.tmp_fields.user = {};
    this.removeUser( user._id );
    this.invites.users.push(user);
    this.evento.users_invitations.push({_user: user._id});
  }

  selectBusinessUser(user) {
    this.tmp_fields.business_user_query = '';
    this.tmp_fields.business_user_suggest = [];
    if (!this.evento.business_users_invitations) {
      this.evento.business_users_invitations = [];
    }
    this.tmp_fields.business_user = {};
    this.removeBusinessUser( user._id );
    this.invites.business.push(user);
    this.evento.business_users_invitations.push({_business_user: user._id});
  }

  searchExternalUser() {
    if (this.loginService.validatePermissionTree('access','externaluser','read') && this.tmp_fields.external_user_query && this.tmp_fields.external_user_query.length>=1) {
      this.accessService.searchExternalUser(this.tmp_fields.external_user_query).subscribe(
        users => {
          this.tmp_fields.external_user_suggest = users;
        },
        err => { console.log(err); }
      );
    } else {
      this.tmp_fields.external_user_suggest = [];
    }
  }


  removeUser( user_id ) {
    this.evento.users_invitations = this.evento.users_invitations.filter( el => el._user !== user_id );
    this.invites.users = this.invites.users.filter( el => el._id !== user_id);
  }

  removeInfraUser() {
    
    this.accessService.updateAssignation( this.evento._id, '' ).subscribe(
      assign => {
        if ( assign._id ) {
          delete this.tmp_fields.infra_user;
          delete this.evento._infra_user;
          this.snackBar.open('El usuario ha sido borrado', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.snackBar.open('Ha ocurrido un error al borrar el usuario.', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        this.snackBar.open('Ha ocurrido un error al borrar el usuario.', 'Cerrar', {
          duration: 3000
        });
      }
    );
  }

  removeBusinessUser( user_id ) {
    this.evento.business_users_invitations = this.evento.business_users_invitations.filter( el => el._business_user !== user_id );
    this.invites.business = this.invites.business.filter( el => el._id !== user_id);
  }

  addExternalUser() {
    
    if (!this.tmp_fields.external_user.name || !this.tmp_fields.external_user.email) {
      alert('El nombre y el email son requeridos');
      return;
    }

    this.accessService.addExternalUser(this.tmp_fields.external_user).subscribe(
      (user) => {
        if (user._id) {
          if (!this.evento.external_invitations) {
            this.evento.external_invitations = [];
          }
          this.tmp_fields.external_user = {};
          this.removeExternalUser( user._id );
          this.invites.external.push(user);
          this.evento.external_invitations.push({_external_user: user._id});
          this.cancelAddExternalUser();
        } else {
          var message = 'Ha ocurrido un error al crear el usuario';
          if ( user.message ) {
            message = user.message;
          }
          this.snackBar.open(message, 'Cerrar', {
            duration: 3000
          });
        }
      },
      (err) => {
        var message = 'Ha ocurrido un error al crear el usuario';
        if ( err.message ) {
          message = err.message;
        }
        this.snackBar.open(message, 'Cerrar', {
          duration: 3000
        });
      }
    );

  }

  cancelAddExternalUser() {
    this.tmp_fields.external_user_query = '';
    this.tmp_fields.external_user = {};
    this.tmp_fields.new_external = false;
  }

  selectExternalUser(user) {
    this.tmp_fields.external_user_query = '';
    this.tmp_fields.external_user_suggest = [];
    if (!this.evento.external_invitations) {
      this.evento.external_invitations = [];
    }
    this.tmp_fields.external_user = {};
    this.removeExternalUser( user._id );
    this.invites.external.push(user);
    this.evento.external_invitations.push({_external_user: user._id});
  }

  createExternalUser() {
    this.tmp_fields.new_external = true;
    this.tmp_fields.external_user.email = this.tmp_fields.external_user_query;
  }

  removeExternalUser(user_id) {
    this.evento.external_invitations = this.evento.external_invitations.filter( el => el._external_user !== user_id );
    this.invites.external = this.invites.external.filter( el => el._id !== user_id);
  }

  resetDeviceRequests(event) {
    if (event) {
      this.evento.device_requests = {
        ingreso: [],
        salida: [],
        no_registrado: false
      };
    } else {
      delete this.evento.device_requests;
    }
  }

  resetDeviceRequestsByType() {
    let copyInicio = this.getCopyOfOptions( this.calendarInicioOptions );
    let copyFin = this.getCopyOfOptions( this.calendarFinOptions );
    if ( this.evento.type !== 'datacenter' ) {
      delete this.evento.device_manage;
      delete this.evento.device_requests;
      copyInicio.disableWeekdays = [];
      copyFin.disableWeekdays = [];
      this.calendarInicioOptions = copyInicio;
      this.calendarFinOptions = copyFin;
    } else {
      // Protect unavailable access days
      copyInicio.disableWeekdays = this.loginService.unavailableDays();
      copyFin.disableWeekdays = this.loginService.unavailableDays();
      this.calendarInicioOptions = copyInicio;
      this.calendarFinOptions = copyFin;
      this.resetDays();
    }
    delete this.evento.sede;
    delete this.evento.sala;
  }

  getCopyOfOptions(item): any {
    return JSON.parse(JSON.stringify(item));
  }

  resetDays(): any {
    if ( !this.loginService.identity.business || !this.loginService.identity.dc_access || !this.loginService.identity.dc_access.days ) {
      return;
    }
    var valid_day = this.loginService.identity.dc_access.days.indexOf( this.evento.start_date.getDay() );
    if ( valid_day === -1 ) {
      this.tmp_dates.start_date = null;
      // find valid day starting from today
      var tries = 1;
      while( valid_day === -1 && tries < 7 ) {
        valid_day = this.loginService.identity.dc_access.days.indexOf( moment().add( tries, 'days').toDate().getDay() );
        
        if ( valid_day !== -1 ) {
          var d = moment().add( tries, 'days');
          this.tmp_dates.start_date = { date: { year: d.format('YYYY'), month: d.format('M'), day: d.format('D') } };
        }
        tries++;
      }
      this.setupDate('start_date');

      this.tmp_dates.end_date = this.tmp_dates.start_date;
      this.setupDate('end_date');
    }

    const hours = this.loginService.available_user_hours(this.evento.type);
    this.tmp_dates.hora_inicio = (hours && hours.length) ? hours[0] : '';
    this.tmp_dates.hora_fin = (hours && hours.length) ? hours[1] : '';
    this.setupDate('start_date');
    this.setupDate('end_date');
  }

  filterDevices(status) {
    if ( !this.business_devices ) {
      return [];
    }
    return this.business_devices.filter( el => el.status === status );
  }

  addOrRemoveDevice(checked, device_id, type) {
    if ( checked ) {
      if ( this.evento.device_requests[type].indexOf(device_id) < 0 ) {
        this.evento.device_requests[type].push(device_id);
      }
    } else {
      if ( this.evento.device_requests[type].indexOf(device_id) !== -1 ) {
        this.evento.device_requests[type].splice(this.evento.device_requests[type].indexOf(device_id), 1);
      }
    }
  }

  saveEvent() {
    var require_confirm = true;
   
    
    this.evento.start_date = this.tmp_dates.start_date;
    this.evento.end_date = this.tmp_dates.end_date;
    
    
    if ( !require_confirm || confirm("Las autorizaciones existentes serán descartadas.\n¿Deseas continuar?") ) {
      
      this.accessService.saveOrUpdate(this.evento).subscribe(
        (res) => {
          if (res._id) {
            this.evento = res;
            this.snackBar.open('El evento ha sido actualizado', 'Cerrar', {
              duration: 3000
            });
          } else {
            this.snackBar.open('Ha ocurrido un error al guardar el evento', 'Cerrar', {
              duration: 3000
            });
          }
          this.loadingContent = false;
        },
        (err) => {
          this.loadingContent = false;
          this.snackBar.open('Ha ocurrido un error al guardar el evento', 'Cerrar', {
            duration: 3000
          });
        }
      );
    }
    
  }

  setAuthorization( type ) {
    this.loadingContent = true;
    this.accessService.addAuthorization(this.evento, type).subscribe(
      (res) => {
        if (res._id) {
          this.evento = res;
          this.snackBar.open('El evento ha sido actualizado', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.snackBar.open('Ha ocurrido un error al guardar el evento', 'Cerrar', {
            duration: 3000
          });
        }
        this.loadingContent = false;
      },
      (err) => {
        this.loadingContent = false;
        this.snackBar.open('Ha ocurrido un error al guardar el evento', 'Cerrar', {
          duration: 3000
        });
      }
    );
  }



}
