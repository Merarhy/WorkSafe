import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { AccessEventIdComponent } from './access-event-id.component';

@NgModule({
    declarations: [
       AccessEventIdComponent
    ],
    imports     : [
        SharedModule,
        RouterModule,
        SharedModule,
    ],
    exports     : [
       AccessEventIdComponent
    ]
})

export class AccessEventIDModule
{

}
