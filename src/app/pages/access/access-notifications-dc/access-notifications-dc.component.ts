import { Component, OnInit ,Input, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import * as moment from 'moment';
import { MatDialog, MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Subject } from "rxjs";
import { fadeInAnimation } from "../../../route.animation";
import { AccessService } from "../access.service";
import { MyUsersService } from '../../my-business/my-users.service';
import { Evento } from "../access.model";
import { routeAnimation } from "../../../route.animation";
import { LoginService } from '../../../pages/login/login.service';
import { RouterModule, Router } from '@angular/router';
import * as _ from 'lodash';
import { environment } from '../../../../environments/environment';

declare let ResizeSensor;


@Component({
  selector: 'ms-access-notifications-dc-widget',
  templateUrl: './access-notifications-dc.html',
  styleUrls: ['./access-notifications-dc.component.scss'],
   host: {
    "[@fadeInAnimation]": 'true'
  },
  animations: [ fadeInAnimation, routeAnimation],
  providers: [AccessService, MyUsersService]
})
export class AccessNotificationsDcComponent implements OnInit {
  

  current_date: any;
 
  eventos: any;
  pending_events: any;
  nexts_events: any;
  previous_eventos: any;
  previous_authorization_events: any;
  location_events: any;
  dc_access_requests: any;
  avatarPath: String = environment.apiUrl + 'avatars/';

  loadingAuthorization: Boolean = false;

  isOpen: boolean;

  constructor(
    private dialog: MatDialog,
    private calendarService: AccessService,
    private loginService: LoginService,
    private usersService: MyUsersService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
  }



  ngOnInit() {

    this.current_date = moment();

    

    if ( this.loginService.identity.business && this.loginService.hasPermission('mybusiness-dcaccess-authorize') ) {
      this.loadingAuthorization = true;
      this.usersService.getUsersDatacenterRequests().subscribe(
        requests => {
          if (requests.data) {
            this.dc_access_requests = requests.data;
          }
          this.loadingAuthorization = false;
        },
        error => {
          this.loadingAuthorization = false;
          console.log( error );
        }
      );
    }

  }

  authorizeAccess( req_id: string ) {

    this.loadingAuthorization = true;
    this.usersService.authorizeUserDatacenterRequest( req_id ).subscribe(
      res => {
        this.loadingAuthorization = false;
        if ( res._id ) {
          var index = this.dc_access_requests.findIndex( el => el._id === req_id );
          this.dc_access_requests[ index ] = res;
          this.dc_access_requests = this.dc_access_requests.filter(el => {
            return el.status === 'Iniciada';
          });
          this.snackBar.open('La solicitud ha sido actualizada.', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.snackBar.open('Ha ocurrido un error al actualizar la solicitud.', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        this.loadingAuthorization = false;
        console.log('error', err);
        this.snackBar.open('Ha ocurrido un error al actualizar la solicitud.', 'Cerrar', {
          duration: 3000
        });
      }
    );

  }

  rejectAccess( req_id: string ) {
    this.loadingAuthorization = true;
    this.usersService.rejectUserDatacenterRequest( req_id ).subscribe(
      res => {
        this.loadingAuthorization = false;
        if ( res._id ) {
          var index = this.dc_access_requests.findIndex( el => el._id === req_id );
          this.dc_access_requests[ index ] = res;
          this.dc_access_requests = this.dc_access_requests.filter(el => {
            return el.status === 'Iniciada';
          });
          this.snackBar.open('La solicitud ha sido actualizada.', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.snackBar.open('Ha ocurrido un error al actualizar la solicitud.', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        this.loadingAuthorization = false;
        console.log('error', err);
        this.snackBar.open('Ha ocurrido un error al actualizar la solicitud.', 'Cerrar', {
          duration: 3000
        });
      }
    );

  }

  getDayName( dayNumber ) {
    const dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    return dayNames[dayNumber];
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  onClickOutside() {
    this.isOpen = false;
  }

}
