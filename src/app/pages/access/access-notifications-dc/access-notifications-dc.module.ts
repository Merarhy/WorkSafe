import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';

import { AccessNotificationsDcComponent } from './access-notifications-dc.component';

@NgModule({
    declarations: [
        AccessNotificationsDcComponent
    ],
    imports     : [
     //  IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
        AccessNotificationsDcComponent
    ]
})

export class AccessNotificationsDcModule
{

}
