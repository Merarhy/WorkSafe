import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { AccessEventRegisterLaptopComponent } from './access-event-register-laptop.component';

@NgModule({
    declarations: [
        AccessEventRegisterLaptopComponent
    ],
    imports     : [
        SharedModule,
        RouterModule,
    ],
    exports     : [
        AccessEventRegisterLaptopComponent
    ]
})

export class AccessEventRegisterLaptopModule
{

}
