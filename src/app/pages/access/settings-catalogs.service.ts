import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class SettingsCatalogsService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }


    getCollection(type: string, collection: string): Observable<any> {
        return this.http.get(`${environment.apiUrl}tools/catalog/${type}/${collection}`, this.requestOptions).map( extractData );
    }

    addCollectionItem(type: string, collection: string, data: any): Observable<any> {
        return this.http.post(`${environment.apiUrl}tools/catalog/${type}/${collection}`, data, this.requestOptions).map( extractData );
    }

    updateCollectionItem(type: string, collection: string, data: any): Observable<any> {
        return this.http.patch(`${environment.apiUrl}tools/catalog/${type}/${collection}/${data._id}`, data, this.requestOptions)
        .map( extractData );
    }

    deleteCollectionItem(type: string, collection: string, data: any): Observable<any> {
        return this.http.delete(`${environment.apiUrl}tools/catalog/${type}/${collection}/${data._id}`, this.requestOptions)
        .map( extractData );
    }

}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        if (body.data) {
            body = body.data.map(toAny);
        } else {
            body = toAny(body);
        }
    } catch (error) {}
    return body || { };
}

function toAny(r: any): any {
    let options = {};
    for (let key in r) {
        if (typeof(r[key]) !== 'object') {
            options[key] = r[key];
        } else {
            options[key] = r[key].map( el => { return { value: el }; });
        }
    }
    return <any>(options);
}

/*
function mapAny(response: Response): any[] {
    // The response of the API has a results
    // property with the actual results
    return response.json();
}
*/