import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { AccessEventRegisterDeviceComponent } from './access-event-register-device.component';
import { RouterModule, Routes} from '@angular/router';

@NgModule({
    declarations: [
       AccessEventRegisterDeviceComponent
    ],
    imports     : [
        SharedModule,
        RouterModule,
        SharedModule,
    ],
    exports     : [
       AccessEventRegisterDeviceComponent
    ]
})

export class AccessEventRegisterDeviceModule
{

}
