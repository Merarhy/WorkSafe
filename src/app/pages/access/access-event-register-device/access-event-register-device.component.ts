import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import {AccessService} from "../access.service";
import {MatDialog, MatSnackBar} from "@angular/material";
import * as moment from 'moment';
import {LoginService} from '../../../pages/login/login.service';
import {UsersService as BusinessUserService} from '../../business/users.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ms-access-event-register-device',
  templateUrl: './access-event-register-device.component.html',
  styleUrls: ['./access-event-register-device.component.scss'],
  providers: [AccessService, BusinessUserService]
})
export class AccessEventRegisterDeviceComponent implements OnInit {

  @ViewChild( 'serialDeviceInput' ) private _serialDevice: ElementRef;
  
    private sub: any;
    
    qr_code: String = '';
    serialDevice: String = '';
    new_device: Boolean = false;
    new_device_data: any = {};
    device_requests: any = {};
    loadingContent: Boolean = true;
  
    evento: any ;
    user: any;
    avatarPath: String = environment.apiUrl + 'avatars/';
  
    tmp_fields: any = {};
  
    searchChangeObserver;
  
    constructor(
      private location: Location,
      private router: Router,
      private route: ActivatedRoute,
      private snackBar: MatSnackBar,
      private loginService: LoginService,
      private accessService: AccessService,
      private businessUserService: BusinessUserService
    ) {}
  
    ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
        
        if (params['qr_code']) {
            this.qr_code = params['qr_code'];
            this.searchEvent();
        } else {
          this.snackBar.open('No se ha identificado un código QR.', 'Cerrar', {
            duration: 3000
          });
          this.router.navigate(['/admin/']);
        }
  
      });
    }
  
    onSearchChange(searchValue: string) {
      if (!this.searchChangeObserver) {
        Observable.create(observer => {
            this.searchChangeObserver = observer;
        })
        .debounceTime(2000) // wait 2000ms after the last event before emitting last event
        .distinctUntilChanged() // only emit if value is different from previous value
        .subscribe( () => {
          this.searchSerial();
        });
      }
      this.searchChangeObserver.next(searchValue);
    }
  
    setInputFocus() {
      setTimeout( () => {
        this._serialDevice.nativeElement.focus();
      });
    }
  
    searchSerial() {
      var new_status = '';
      var device = this.device_requests.ingreso.find( el => (el.serie === this.serialDevice && el.status === 'Standby' ) );
      if( device ) {
        new_status = 'Ingresado';
      } else {
        new_status = 'Standby';
        device = this.device_requests.salida.find( el => (el.serie === this.serialDevice && el.status === 'Ingresado') );
      }

      if ( device ) {
        this.loadingContent = true;
        this.registerDeviceDatacenterEvent(device._id, device.status, new_status);
      } else {
        this.snackBar.open('No se ha encontrado el dispositivo o ya fue registrado.', 'Cerrar', {
          duration: 3000
        });
      }
      
    }
    
    /*
    addDevice() {
      this.registerDeviceEvent(true, this.new_device_data);
    }
    */
  
    searchEvent() {
      if ( this.qr_code.length ) {
        this.accessService.readQr( this.qr_code ).subscribe(
          data => {
            if ( data.type === 'business' ) {
              this._setupEvent(data);
              this.loadingContent = false;
              this.setInputFocus();
            } else {
              this.snackBar.open('El usuario no es cliente de SDR.', 'Cerrar', {
                duration: 3000
              });
              this.router.navigate(['/admin/']);
            }
          },
          err => {
            this.qr_code = '';
            this.snackBar.open('El código ingresado es incorrecto.', 'Cerrar', {
              duration: 3000
            });
            this.router.navigate(['/admin/']);
          }
        );
        /// GET DC Devices
        this.accessService.getDatacenterDevices( this.qr_code ).subscribe(
          data => {
            if ( data && data.device_requests ) {
              this.device_requests = data.device_requests;
            } else {
              this.qr_code = '';
              this.snackBar.open('No se han encontrado dispositivos a gestionar.', 'Cerrar', {
                duration: 3000
              });
            }
          },
          err => {
            this.qr_code = '';
            this.snackBar.open('El código ingresado es incorrecto.', 'Cerrar', {
              duration: 3000
            });
            this.router.navigate(['/admin/']);
          }
        );
      }
    }
  
    _setupEvent(data) {
      if (data && data.event) {
        this.evento = data.event;
        this.user = data.user;
        this.user._business_user.devices = this.user._business_user.devices.map( el => {
          let reg_dev = this.user.devices.find( dv => dv.series === el.series );
          if ( reg_dev ) {
            if ( reg_dev.entry_date ) {
              el.entry_date = reg_dev.entry_date;
            }
            if ( reg_dev.exit_date ) {
              el.exit_date = reg_dev.exit_date;
            }
          }
          return el;
        });
        var init_date = moment.utc(this.evento.start_date);
        var end_date = moment.utc(this.evento.end_date);
        this.evento.business_users_invitations = this.evento.business_users_invitations.map( el => {
          if ( el.entry_date ) {
            el.entry_date = moment(el.entry_date);
          }
          if ( el.exit_date ) {
            el.exit_date = moment(el.exit_date);
          }
          return el;
        });
  
        this.tmp_fields.start_date = init_date.format('DD MMM YYYY HH:mm');
        this.tmp_fields.end_date = end_date.format('DD MMM YYYY HH:mm');
      } else {
        this.qr_code = '';
        this.snackBar.open('El código ingresado es incorrecto.', 'Cerrar', {
          duration: 3000
        });
        this.router.navigate(['/admin/']);
      }
    }
  
    registerDeviceDatacenterEvent( device_id: String, prev_status: String, new_status: String ) {

      var device = {
        id: device_id,
        status: new_status,
        prev_status: prev_status
      }
  
      this.accessService.registerDeviceDatacenterEvent( this.qr_code, device  ).subscribe(
        data => {
          if ( data && data.device_requests ) {
            this.serialDevice = '';
            this.device_requests = data.device_requests;
          } else {
            this.serialDevice = '';
            var message = 'Ha ocurrido un error al registrar el dispositivo.';
            if ( data.message ) {
              message = data.message;
            }
            this.snackBar.open(message, 'Cerrar', {
              duration: 3000
            });
          }
          this.loadingContent = false;
        },
        err => {
          this.serialDevice = '';
          this.loadingContent = false;
          var message = 'Ha ocurrido un error al registrar el usuario.';
          
          if ( err && err._body && JSON.parse(err._body) && JSON.parse(err._body).message ) {
            message = JSON.parse(err._body).message;
          }
          this.snackBar.open( message , 'Cerrar', {
            duration: 3000
          });
        }
      );
  
    }
  
    ngOnDestroy() {
      this.sub.unsubscribe();
    }
  
    toMoment( date ) {
      return moment( date ).format('HH:mm');
    }

    humanDate( currentDate ) {
      return moment( currentDate ).format( 'D MMM YYYY');
    }

}
