import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Evento } from "../access.model";
import { AccessService } from "../access.service";
import {LoginService} from '../../../pages/login/login.service';
import { MatSnackBar } from "@angular/material";
import { environment } from '../../../../environments/environment';
import * as moment from 'moment';

@Component({
  selector: 'ms-access-invitecode',
  templateUrl: './access-invitecode.component.html',
  styleUrls: ['./access-invitecode.component.scss'],
  providers: [AccessService]
})
export class AccessInvitecodeComponent implements OnInit {

  sub: any;

  evento: Evento = new Evento();
  full_evento: any;

  tmp_dates: any = {};

  tmp_fields: any = {
    external_user: {}
  };

  my_invite: string;

  invites: any = {
    users: [],
    business: []
  };
  qr_url: any;
  avatarPath: String = environment.apiUrl + 'avatars/';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private accessService: AccessService,
    private snackBar: MatSnackBar,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

      if (params['event_id']) {
          this.accessService.getEventoById(params['event_id']).subscribe(
            event => {
              if (event && event['event']._id && event['event']['status'] === 'Autorizado') {
                this.evento = event['event'];
                // Create full object
                this.full_evento = event['details'];
                // Initialize items
                this._convertEvent();
              } else {
                this.snackBar.open('El evento que buscas no existe.', 'Cerrar', {
                  duration: 3000
                });
                this.router.navigate(['/admin']);
              }
            },
            error => {
              this.snackBar.open('Ha ocurrido un error al cargar el evento.', 'Cerrar', {
                duration: 3000
              });
              this.router.navigate(['/admin']);
            }
          );
      } else {
        this.router.navigate(['/admin']);
      }

    });
  }

  // Convert to a simple unpopulated object
  _convertEvent() {
    // Get my_invite code
    if (this.loginService.identity.business) {
      // Validate if is owner
      var invite = this.evento.business_users_invitations.find( el => el._business_user === this.loginService.identity._id );
      if (invite) {
        this.my_invite = invite._id;
        this.qr_url = environment.apiUrl + 'files/access/' + this.evento._id + '/invite/' + invite._id + '/type/business';
      }
    }

    // Setup organizador
    var organizer = '';
    if (this.full_evento._user) {
      organizer = '_user';
    }
    if (this.full_evento._business_user) {
      organizer = '_business_user';
    }
    if ( organizer ) {
      this.tmp_fields.organizador = this.full_evento[organizer];
    }

    // Setup dates
    var init_date = moment.utc(this.full_evento.start_date);
    var end_date = moment.utc(this.full_evento.end_date);

    this.tmp_fields.start_date = init_date.format('DD MMM YYYY HH:mm');
    this.tmp_fields.end_date = end_date.format('DD MMM YYYY HH:mm');

    this.tmp_dates = {
      start_date: { date: { year: init_date.format('YYYY'), month: init_date.format('M'), day: init_date.format('D') } },
      end_date: { date: { year: end_date.format('YYYY'), month: end_date.format('M'), day: end_date.format('D') } },
      hora_inicio: init_date.format('HH:mm'),
      hora_fin: end_date.format('HH:mm')
    };

    if (this.full_evento.business_users_invitations) {
      this.full_evento.business_users_invitations.forEach( el => {
        this.invites.business.push(el._business_user);
      });
    }
    if (this.full_evento.users_invitations) {
      this.full_evento.users_invitations.forEach( el => {
        this.invites.users.push(el._user);
      });
    }


    this.setupDate('start_date');
    this.setupDate('end_date');

  }

  setupDate(type) {
    switch (type) {
      case 'start_date':
        if (this.tmp_dates.start_date && this.tmp_dates.start_date.date && this.tmp_dates.start_date.date.year){
          this.evento.start_date = moment.utc(this.tmp_dates.start_date.date.year+'-'+('0' + (this.tmp_dates.start_date.date.month)).slice(-2)+'-'+('0' + (this.tmp_dates.start_date.date.day)).slice(-2)+' '+this.tmp_dates.hora_inicio+':00').toDate();
        } else {
          this.evento.start_date = null;
        }
      break;
      case 'end_date':
        if (this.tmp_dates.end_date && this.tmp_dates.end_date.date && this.tmp_dates.end_date.date.year){
          this.evento.end_date = moment.utc(this.tmp_dates.end_date.date.year+'-'+('0' + (this.tmp_dates.end_date.date.month)).slice(-2)+'-'+('0' + (this.tmp_dates.end_date.date.day)).slice(-2)+' '+this.tmp_dates.hora_fin+':00').toDate();
        } else {
          this.evento.end_date = null;
        }
      break;
    }
  }

}
