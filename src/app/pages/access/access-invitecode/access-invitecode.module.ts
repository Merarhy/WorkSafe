import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { AccessInvitecodeComponent } from './access-invitecode.component';

@NgModule({
    declarations: [
        AccessInvitecodeComponent
    ],
    imports     : [
        SharedModule,
        RouterModule,
        SharedModule,
    ],
    exports     : [
        AccessInvitecodeComponent
    ]
})

export class AccessInvitationscodeModule
{

}
