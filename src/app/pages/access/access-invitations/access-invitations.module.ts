import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';

import { AccessInvitationsComponent } from './access-invitations.component';

@NgModule({
    declarations: [
        AccessInvitationsComponent
    ],
    imports     : [
     //  IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
        AccessInvitationsComponent
    ]
})

export class AccessInvitationsModule
{

}
