import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material";
import * as moment from 'moment';
import {  AccessDashboarComponent} from "../access-dashboard/access-dashboard.component";
import { ActivatedRoute } from "@angular/router";
import { AccessService} from "../access.service";
import {CalendarComponent} from "../access.component";
import {MatSnackBar} from "@angular/material";
import * as _ from 'lodash';
import {FormControl} from "@angular/forms";
//import { CompleterService, CompleterData } from 'ng2-completer';
import { Http } from "@angular/http";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
//import { NguiDatetimePickerModule, NguiDatetime} from '@ngui/datetime-picker';
import { Evento } from "../access.model";
import { RouterModule, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'ms-access-invitations',
  templateUrl: './access-invitations.component.html',
  styleUrls: ['./access-invitations.component.scss'],
  providers: [AccessService]
})
export class AccessInvitationsComponent implements OnInit {

  sub: any;


  /*
  user_email = [];
  current_email: any;
  business_email = [];
  current_email2: any;
  errorMessage: string = '';
  
  id_invitation: any;
  evento: any={};
  edit_mode : Boolean;
  current_event: Eventos;
  invitado_SDR: any;
  tmp:any;
  current_invitations: any = {}; 
  newSDR: Eventos;
  newBusiness: Eventos;
  newExternal: Eventos;
  types:any[];
  */
  evento: Evento;

  constructor(   
    private snackBar: MatSnackBar,
   // private completerService: CompleterService,
    private routes: ActivatedRoute,
    private route: Router,
    private calendarService: AccessService, 
    private http: Http
    ) {}

  ngOnInit() {
    /*
    this.sub = this.routes.params.subscribe(params => {
      if(params['id']){
       this.calendarService.getEventoById(params['id']).subscribe(
          evento => {
            if(evento && evento._id){
              this.evento = evento;
            } else {
              this.snackBar.open('El evento no existe', 'Cerrar', {
                duration: 3000
              });
              console.log('se debe hacer redireccion')
            }
          },
          error => { console.log(error); }
        );
      } 
    });  
    */



    
    /*
    this.usersService.getAll().subscribe(
        res => {this.user_email = res; this.current_email =  this.user_email.map((el) =>{return(el.email)});},          
        e => {this.errorMessage = e;}       
    ); 

    this.usersServices.getAll().subscribe(
      res => { this.business_email = res; this.current_email2 = this.business_email.map((el) => {return(el.email)});},
      e => {this.errorMessage = e;}
    );
    */
    
  }



/*
  setActive(evento){

      if(evento.length>0) {
        
          this.current_event = _.cloneDeep(this.evento[0]);
          
            this.newInvitationSDR();
            this.newInvitationBusiness();
            this.newInvitationExternal();
        }else{
        error =>  {console.log(error);}
        };
  }

 saveEvent(){
      if(moment(this.current_event.start).format('YYYY-MM-DD') === moment(this.current_event.end).format('YYYY-MM-DD')){
        this.calendarService
        .saveOrUpdate(this.current_event)
        .subscribe(
          (res) => {
            if(res.message){
              this.snackBar.open(res.message, 'Cerrar', {
                duration: 3000
              });
            } else {    
              this.snackBar.open('El evento ha sido guardado', 'Cerrar', {
                duration: 3000
              });
            }
          }
        );
      }else{
        this.snackBar.open('El Evento debe finalizar el mismo Dia', 'Cerrar', {
                duration: 3000
              });
      }        
  }


  addInvitation(data){
  
  console.log(data);
    
    //////if(moment(this.current_event.start).format('YYYY-MM-DD') === moment(this.current_event.end).format('YYYY-MM-DD')){
      var tmp: any;
      if(this.newSDR.users_invitations[0]._user){
         tmp = this.newInvitationSDR();
         console.log("1");
      }if(this.newBusiness.business_users_invitations[0]._business_user){
        tmp = this.newInvitationBusiness();
        console.log("2");
      }else{
        tmp = this.newInvitationExternal();
        console.log("3");
      }

      this.calendarService
      .saveOrUpdate(tmp)
      .subscribe(
          (res) => {
            if(res.message){
              this.snackBar.open(res.message, 'Cerrar', {
                duration: 3000
              });
            } else {    
              this.snackBar.open('El evento ha sido guardado', 'Cerrar', {
                duration: 3000
              });
            }
          }
        );
      }else{
        this.snackBar.open('El Evento debe finalizar el mismo Dia', 'Cerrar', {
                duration: 3000
              });
    }
            //////
  }

  newInvitationSDR(){
    this.newSDR = new Eventos({

    _id: this.current_event._id,
    
    users_invitations:{
      _user: '',
    }

    }); 
  return this.newSDR;
  }

  newInvitationBusiness(){

    this.newBusiness = new Eventos({

      _id: this.current_event._id,

      business_users_invitations:{
        _business_user: '',
      }

    });
    return this.newBusiness;
  }

  newInvitationExternal(){
    
    this.newExternal = new Eventos ({

      _id: this.current_event._id,
      
      external_invitations:{
        name:'',
        cel:'',
        company:'',
        email: ''
      }
    });
    return this.newExternal;
  }

  */

}
