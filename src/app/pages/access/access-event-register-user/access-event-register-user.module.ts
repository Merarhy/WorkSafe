import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { AccessEventRegisterUserComponent } from './access-event-register-user.component';
import { WebCamModule } from 'ack-angular-webcam';

@NgModule({
    declarations: [
        AccessEventRegisterUserComponent
    ],
    imports     : [
     //  IdToRolePipeModule,
        SharedModule,
        RouterModule,
        WebCamModule
    ],
    exports     : [
        AccessEventRegisterUserComponent
    ]
})

export class AccessEventRegisterUserModule
{

}
