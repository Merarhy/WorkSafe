import { Component, ViewEncapsulation, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import {AccessService} from "../access.service";
import {MatDialog, MatSnackBar} from "@angular/material";
import * as moment from 'moment';
import {LoginService} from '../../../pages/login/login.service';
import {UsersService as BusinessUserService} from '../../business/users.service';
import { environment } from '../../../../environments/environment';
import { fuseAnimations } from '../../../core/animations';

@Component({
  selector: 'ms-access-event-register-id',
  templateUrl: './access-event-register-id.component.html',
  styleUrls: ['./access-event-register-id.component.scss'],
  providers: [AccessService, BusinessUserService],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None,
})
export class AccessEventRegisterIdComponent implements OnInit {

  private sub: any;

  qr_code: String = '';
  loadingContent: Boolean = false;

  evento: any;
  user: any;
  user_type: string;
  avatarPath: String = environment.apiUrl + 'avatars/';
  domainUrl: String = environment.domainUrl;

  tmp_fields: any = {};
  print_hidde: Boolean = false;
  searchChangeObserver;

  public webcam; // will be populated by ack-webcam [(ref)]
  public base64;
  public captured: any = false;
  takePhoto: any = false;
  options: any;

  qr_url: String = '';

  constructor(
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private accessService: AccessService,
    private businessUserService: BusinessUserService
  ) {
    this.options = {
      audio: false,
      video: true,
      //fallback: true,//force flash
      width: 300,
      height: 225, 
      fallbackMode: 'callback',
      fallbackSrc: 'jscam_canvas_only.swf',
      fallbackQuality: 50
    };
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      
      if (params['qr_code']) {
          this.qr_code = params['qr_code'];
          this.searchEvent();
      } else {
        this.snackBar.open('No se ha identificado un código QR.', 'Cerrar', {
          duration: 3000
        });
        this.router.navigate(['/admin/']);
      }

    });
    
  }

  searchEvent() {
    if ( this.qr_code.length ) {
      this.accessService.readQr( this.qr_code ).subscribe(
        data => {
          this._setupEvent(data);
        },
        err => {
          this.qr_code = '';
          this.snackBar.open('El código ingresado es incorrecto.', 'Cerrar', {
            duration: 3000
          });
          this.router.navigate(['/admin/']);
        }
      );
    }
  }

  _setupEvent(data) {
    if (data && data.event) {
      this.user_type = data.type;
      this.evento = data.event;
      this.user = data.user;
      var init_date = moment.utc(this.evento.start_date);
      var end_date = moment.utc(this.evento.end_date);
      this.evento.business_users_invitations = this.evento.business_users_invitations.map( el => {
        if ( el.entry_date ) {
          el.entry_date = moment(el.entry_date);
        }
        if ( el.exit_date ) {
          el.exit_date = moment(el.exit_date);
        }
        return el;
      });

      this.evento.external_invitations = this.evento.external_invitations.map( el => {
        if ( el.entry_date ) {
          el.entry_date = moment(el.entry_date);
        }
        if ( el.exit_date ) {
          el.exit_date = moment(el.exit_date);
        }
        return el;
      });

      this.tmp_fields.start_date = init_date.format('DD MMM YYYY HH:mm');
      this.tmp_fields.end_date = end_date.format('DD MMM YYYY HH:mm');

      this.qr_url = environment.apiUrl + 'files/access/' + this.evento._id + '/invite/' + this.user._id + '/type/'+this.user_type;
    } else {
      this.qr_code = '';
      this.snackBar.open('El código ingresado es incorrecto.', 'Cerrar', {
        duration: 3000
      });
    }
  }

  registerUserEvent( ingreso: Boolean ) {

    this.accessService.registerUserEvent( ingreso, this.qr_code ).subscribe(
      data => {
        if ( data && data.event ) {
          this._setupEvent(data);
          this.snackBar.open('El usuario ha sido registrado', 'Cerrar', {
            duration: 3000
          });
        } else {
          var message = 'Ha ocurrido un error al registrar el usuario.';
          if ( data.message ) {
            message = data.message;
          }
          this.snackBar.open( message , 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        var message = 'Ha ocurrido un error al registrar el usuario.';
        
        if ( err && err._body && JSON.parse(err._body) && JSON.parse(err._body).message ) {
          message = JSON.parse(err._body).message;
        }
        this.snackBar.open( message , 'Cerrar', {
          duration: 3000
        });
      }
    );

    /*
    this.evento._id
    this.user._id
    if (this.user._business_user) {
      var type = 'business'
    }
    console.log();
    console.log(this.user);
    */
    /*
    this.loadingContent = true;
    setTimeout( () => {
      this.loadingContent = false;
    },3000);
    */
  }

  resetEvent() {
    if( !this.qr_code ) {
      this.router.navigate(['/admin/']);
    } else {
      this.qr_code = '';
      this.loadingContent = false;
    
      delete this.evento;
      delete this.user;
      this.tmp_fields = {};
    
      delete this.base64;
      this.captured = false;
      this.takePhoto = false;
    }
  }

  captureBase64(){
    return this.webcam.getBase64()
    .then( base => {
      this.captured = new Date();
      this.base64 = base;
      setTimeout( () => this.webcam.resizeVideo(), 0);
    })
    .catch( e=>console.error(e) )
//    setTimeout(()=>this.webcam.onResize(), 0)
  }

  clearPhoto() {
    this.captured = null;
    this.base64 = null;
  }

  genBase64(){
    this.webcam.getBase64()
    .then( base=>this.base64=base)
    .catch( e=>console.error(e) )
  }

  savePhoto() {
    // Validate is business user
    if (this.user._business_user) {
      this.loadingContent = true;
      this.businessUserService.updateAvatar(this.user._business_user.business._id, this.user._business_user._id, this.base64).subscribe(
        data => {
          if ( data && data._id && data.avatar ) {
            this.user._business_user.avatar = data.avatar;
            this.loadingContent = false;
            this.snackBar.open('El avatar ha sido actualizado.', 'Cerrar', {
              duration: 3000
            });
            this.takePhoto = false;
            this.captured = null;
            this.base64 = null;
          } else {
            this.loadingContent = false;
            this.snackBar.open('Ha ocurrido un error al cargar el avatar.', 'Cerrar', {
              duration: 3000
            });
          }
        },
        error => {
          this.loadingContent = false;
          console.log('error', error);
          this.snackBar.open('Ha ocurrido un error al cargar el avatar.', 'Cerrar', {
            duration: 3000
          });
        }
      );
    }
    // Validate if is external user
    if (this.user._external_user) {
      this.loadingContent = true;
      this.accessService.updateExternalUserAvatar(this.user._external_user._id, this.base64).subscribe(
        data => {
          if ( data && data._id && data.avatar ) {
            this.user._external_user.avatar = data.avatar;
            this.loadingContent = false;
            this.snackBar.open('El avatar ha sido actualizado.', 'Cerrar', {
              duration: 3000
            });
            this.takePhoto = false;
            this.captured = null;
            this.base64 = null;
          } else {
            this.loadingContent = false;
            this.snackBar.open('Ha ocurrido un error al cargar el avatar.', 'Cerrar', {
              duration: 3000
            });
          }
        },
        error => {
          this.loadingContent = false;
          console.log('error', error);
          this.snackBar.open('Ha ocurrido un error al cargar el avatar.', 'Cerrar', {
            duration: 3000
          });
        }
      );
    }
  }

 
  onCamError(err){
    this.takePhoto = false;
    console.log(err);
    this.snackBar.open('Por favor conecta una cámara al equipo. (' + err + ')', 'Cerrar', {
      duration: 3000
    });
  }
 
  onCamSuccess(){}

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>SDR México</title>
          <style>
          body {
            font-family: Arial, sans-serif;
            margin: 0;
          }
          table {
            width: 100%;
            max-width: 600px;
          }
          .header {
            text-align: center;
            font-size: 1.3em;
            font-weight: 600;
            background-color: #eaeaea;
            padding: 5px 0;
          }
          .avatar-container {
            text-align: center;
            padding-top: 20px;
          }
          .avatar-container img {
            border-radius: 50%;
          }
          h3 {
            font-size: 1em;
          }
          .details-content {
            padding: 0 10px;
          }
          .details-table {
            font-size: .8em;
          }
          .event-date {
            text-align: center;
            color: #616161;
          }
          .event-label {
            font-weight: 600;
          }
          img {
            max-width: 100%;
          }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
