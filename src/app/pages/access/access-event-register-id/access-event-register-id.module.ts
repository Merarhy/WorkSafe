import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';

import { AccessEventRegisterIdComponent } from './access-event-register-id.component';
import { WebCamModule } from 'ack-angular-webcam';
@NgModule({
    declarations: [
       AccessEventRegisterIdComponent
    ],
    imports     : [
       WebCamModule,
        SharedModule,
    ],
    exports     : [
       AccessEventRegisterIdComponent
    ]
})

export class AccessEventREgisterIdModule
{

}
