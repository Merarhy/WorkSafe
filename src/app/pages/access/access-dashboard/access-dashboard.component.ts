import { Component, OnInit ,Input, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import * as moment from 'moment';
import { MatDialog, MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Subject } from "rxjs";
import { fadeInAnimation } from "../../../route.animation";
import { AccessService } from "../access.service";
import { MyUsersService } from '../../my-business/my-users.service';
import {SettingsCatalogsService} from '../settings-catalogs.service';
import { Evento } from "../access.model";
import { routeAnimation } from "../../../route.animation";
import { LoginService } from '../../../pages/login/login.service';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { environment } from '../../../../environments/environment';


declare let ResizeSensor;


@Component({
  selector: 'ms-widget-access',
  templateUrl: './access-dashboard.html',
  styleUrls: ['./access-dashboard.component.scss'],
   host: {
    "[@fadeInAnimation]": 'true'
  },
  animations: [ fadeInAnimation, routeAnimation],
  providers: [AccessService, MyUsersService, SettingsCatalogsService]
})
export class AccessDashboarComponent implements OnInit {
  
  current_date: any;
 
  items: Array<any> = [];
  marquee: Array<any> = [];
  eventos: any;
  pending_events: any;
  nexts_events: any;
  previous_eventos: any;
  previous_authorization_events: any;
  location_events: any;
  dc_access_requests: any;
  avatarPath: String = environment.apiUrl + 'avatars/';

  loadingAuthorization: Boolean = false;
 
  UserID: string;
  private sub: any;



  constructor(
    private dialog: MatDialog,
    private calendarService: AccessService,
    private loginService: LoginService,
    private usersService: MyUsersService,
    private settingsCatalogsService: SettingsCatalogsService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {

  
  }

  users: any;

  ngOnInit() {
    if ( !this.loginService.permissions || !this.loginService.permissions.length ) {
      setTimeout( () => {
        this._initServices();
      }, 2000);
    } else {
      this._initServices();
    }
  }

  _initServices() {
    this.current_date = moment();

    if ( this.loginService.hasPermission('access-profile-reception') ) {
      this.calendarService.getCommingLocationEvents().subscribe(
        res => {
          this.location_events = res;
  
          this.location_events = this.location_events.map( el => {
            el.start_date = moment.utc(el.start_date);
            el.end_date = moment.utc(el.end_date);
            return el;
          });
        
        },
        e => { console.log(e); }
      );
    }
    
    if ( this.loginService.hasPermission('access-event-read') ) {
      this.calendarService.getLasts().subscribe(
        res => {
          this.eventos = res;
  
          this.eventos = this.eventos.map( el => {
            el.start_date = moment.utc(el.start_date);
            el.end_date = moment.utc(el.end_date);
            return el;
          });
  
          var now = moment().subtract(1,'hour').format('YYYY-MM-DD HH:mm');
          var current_time = moment.utc(now).unix();
  
          this.pending_events = this.eventos.filter( el => el.status !== 'Autorizado' );
          this.nexts_events = this.eventos.filter((el) =>  (el.status === 'Autorizado' ) );
  
          this.nexts_events.sort(function(a,b){
            return new Date(a.start_date).getTime() - new Date(b.start_date).getTime()
          });
   
        
        },
        e => { console.log(e); }
      );
    } else {
      console.log('no permissions');
    }

     // Get current users
     if (  this.loginService.identity.business && this.loginService.hasPermission('mybusiness-users-read') ) {
      this.usersService.getAll().subscribe(
        users => {this.users = users; },
        e => { alert('Error loading business data'); }
      );
    }
  }
  
  getDayName( dayNumber ) {
    const dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    return dayNames[dayNumber];
  }

  

}
