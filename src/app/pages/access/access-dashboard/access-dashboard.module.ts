import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';

// import { BusinessComponent } from './business.component';

// import { IdToRolePipeModule } from '../../../pipes/idtorole.module';
import {AccessDashboarComponent } from './access-dashboard.component';
// import { UsersBusinessComponent } from './users-business/users-business.component'

@NgModule({
    declarations: [
      AccessDashboarComponent
    ],
    imports     : [
     //  IdToRolePipeModule,
        SharedModule,
    ],
    exports     : [
      AccessDashboarComponent
    ]
})

export class AccessDashboardModule
{

}
