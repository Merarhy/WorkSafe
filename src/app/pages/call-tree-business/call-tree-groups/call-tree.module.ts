import { NotificacionesComponent } from './notificaciones/notificaciones.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import {CallTreeComponent } from './call-tree.component';
import { NewGroupFormDialogComponent } from './new-group-form/new-group-form.component';
import { RouterModule, Routes} from '@angular/router';
import { MainCallTreeComponent } from '../main-call-tree.component';


@NgModule({
    declarations: [
      MainCallTreeComponent,
      CallTreeComponent,
      NotificacionesComponent,
      NewGroupFormDialogComponent,
    ],
    imports     : [
     
        SharedModule,
        RouterModule
    ],
    exports     : [
      CallTreeComponent
    ],

    entryComponents: [NewGroupFormDialogComponent],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class CallTreeModule
{

}