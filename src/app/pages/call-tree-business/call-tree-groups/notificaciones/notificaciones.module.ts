import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../core/modules/shared.module';
import { NotificacionesComponent } from './notificaciones.component';

@NgModule({
    declarations: [
        NotificacionesComponent
    ],
    imports     : [
     
        SharedModule
    ],
    exports     : [
        NotificacionesComponent
    ]
})

export class NotificacionesModule
{

}
