import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { NewGroupFormDialogComponent } from './new-group-form.component';
import { SharedModule } from '../../../../core/modules/shared.module';


@NgModule({
    declarations: [
        NewGroupFormDialogComponent,  

    ],
    imports     : [
     
        SharedModule,
        RouterModule
    ],
    exports     : [
        NewGroupFormDialogComponent,
    ],

})

export class CallTreeModule
{

}