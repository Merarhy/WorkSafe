import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { NewGroupFormDialogComponent } from './new-group-form/new-group-form.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatTableDataSource , MatPaginator, MatSort} from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { MatSnackBar } from '@angular/material';
import * as _ from 'lodash';
import * as moment from 'moment';
import { fuseAnimations } from '../../../core/animations';
import { FormControl, FormGroup } from '@angular/forms';
import { Group } from '../group.model';
import { LoginService } from '../../../pages/login/login.service';
import { CallTreeService } from '../call-tree.service';
import { NavigationEnd, NavigationStart, RouterModule, Router, ActivatedRoute} from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'fuse-call-tree',
  templateUrl: './call-tree.component.html',
  styleUrls: ['./call-tree.component.scss'],
  animations: fuseAnimations,
  providers: [  CallTreeService ]
})

export class CallTreeComponent implements OnInit {



  @ViewChild('dialogContent') dialogContent: TemplateRef<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  searchInput: FormControl;
  avatarPath: String = environment.apiUrl + 'avatars/';

  contact: any;
  displayedColumns = ['name', 'description', 'sms', 'call', 'recording', 'email', 'buttons'];
  dataSource = new MatTableDataSource();
  notifications: {};
  dialogRef: any;

  newGroupDialogRef: MatDialogRef<NewGroupFormDialogComponent>;

  groups: any[];

  current_group: Group;
  action_mennu: Boolean = false;
  users: any[];
  
  errorMessage: String = '';
  constructor(
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private callTreeService: CallTreeService,
    
    private router: Router,
    public dialog: MatDialog,
  ) { 
     this.searchInput = new FormControl('');
  }

  ngOnInit() {
    if (this.loginService.hasPermission('mybusiness-groups-read')){
      this.getGroups();
    }

    this.searchInput.valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(searchText => {
              this.groupsFiltered( searchText );
            });
 
  }

  getGroups(){
    this.callTreeService.getAll().subscribe(
      groups => {
        this.groups = groups;  
        this.dataSource.data = this.groups;
        this.dataSource.sort = this.sort;
        this.setFirstGroup();
      },
      e => { this.errorMessage = e; }  
    );
  }

  setFirstGroup() {
    if ( this.groups && this.groups.length ) {
      this.current_group = this.groups[0];
    } else {
      this.current_group = undefined;
    }
  }



  newGroup() {
    this.dialogRef = this.dialog.open(NewGroupFormDialogComponent, {
        panelClass: 'contact-form-dialog',
        data      : {
            action: 'new',
            // roles: this.roles
        }
    });

    this.dialogRef.afterClosed()
        .subscribe((response: any) => {
            if ( !response )
            {
                return;
            }
            this.saveGroup( response );
        });

  }

  


  

  saveGroup(groupdata){

    const business_id  =  this.loginService.getIdentity().business._id;
      
      this.callTreeService
      .saveOrUpdate(business_id , groupdata)
      .subscribe(
        (res) => {
          if (res.message){
            this.snackBar.open(res.message, 'Cerrar', {
              duration: 3000
            });
          }else{
            if (groupdata._id){
              this.getGroups();
              this.snackBar.open('El grupo se ha actualizado', 'Cerrar', {
                duration: 3000
              });
            } else{
              this.getGroups();
              this.snackBar.open('El grupo se ha creado', 'Cerrar', {
                duration: 3000
              });
            } 
          }

        }
      );

  }

  deleteGroup(groupdata){
  
    this.callTreeService
        .delete(groupdata)
        .subscribe(
          (res) => {
            if ( res.message ){
              this.snackBar.open(res.message, 'Cerrar', {
                duration: 3000
              });
            } else {
              this.getGroups();
              this.snackBar.open('El grupo ha sido eliminado', 'Cerrar', {
                duration: 3000
              });
            }
          }
        );
  
  }

  editGroup(contact) {
  
    this.dialogRef = this.dialog.open(NewGroupFormDialogComponent, {
        panelClass: 'contact-form-dialog',
        data      : {
            group: this.current_group,
            contact: contact,
            action : 'edit'
        }
    });

    this.dialogRef.afterClosed()
    .subscribe(response => {
        if ( !response )
        {
            return;
        }
        const actionType: string = response[0];
        const formData: any = response[1];
        switch ( actionType )
        {
            /**
             * Save
             */
            case 'save':
                this.saveGroup( formData );
                break;
            /**
             * Delete
             */
            case 'delete':
                
                this.deleteGroup( formData );
                break;
                
        }
    });
  }
 

  
  
  groupsFiltered( searchText: string) {

    this.dataSource.data = this.groups;

    if ( searchText && searchText.length ) {
      this.dataSource.data = this.dataSource.data.filter( (el) => {
        return ( (el['name'] ).toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
      });
    }

  }
      
}





