import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Group } from './group.model'; 
import { LoginService } from '../login/login.service';
import { MyFile } from '../business/file-manager-business/file-manager.model';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../../environments/environment';



@Injectable()

export class NotificationsService {
    
    requestOptions: RequestOptions;
    
    constructor(private http: Http, loginService: LoginService){
        const headers = new Headers({'Authorization': loginService.getToken()});
        this.requestOptions = new RequestOptions({headers: headers});
    }

    /**
     * The File Manager App Main Resolver
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */


    addAuido(GroupId: any, file: any): Observable<any> {    
        const formData = new FormData();
        formData.append('bus_file',  file);
        return this.http.post(`${environment.apiUrl}calltree/audio/${GroupId}`, formData, this.requestOptions).map( extractData );

    }
    


    fileServeUrl(group_id: any): string{
        return `${environment.apiUrl}audios/${group_id}?v=` + ( Math.random() * 1000 ); 
    }
    

  
    addEmail(GroupId: String, data: any): Observable<any> {
      
        return this.http.post(`${environment.apiUrl}calltree/email/${GroupId}`, data, this.requestOptions).map( extractData );
        
    }


    addSMS(GroupId: String, data: any): Observable<any> {    
        return this.http.post(`${environment.apiUrl}calltree/sms/${GroupId}`, data, this.requestOptions).map( extractData );
        
    }

    testNotifier(GroupId: String) {
        return this.http.get(`${environment.apiUrl}calltree/groups/${GroupId}/notifier/test`, this.requestOptions).map( extractData );
    }
  
}

function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toGroup);
    } catch (error) {}
    return body || { };
}


function mapGroups(response: Response): Group[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toGroup);
}


function toGroup(r: any): Group{
const   group = <Group>({
        _id: r._id,
        name: r.name,
        description: r.description,
        _business: r._business,
        notifications: r.notifications,
        configemail: r.configemail,
        configsms: r.configsms,
        audio: r.audio,
        users: r.users,
    });
    return group;
}
