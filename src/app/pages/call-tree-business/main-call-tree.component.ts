import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { CallTreeComponent } from './call-tree-groups/call-tree.component';

@Component({
  selector: 'fuse-main-call-tree',
  templateUrl: './main-call-tree.component.html',
  styleUrls: ['./main-call-tree.component.scss']
})
export class MainCallTreeComponent implements OnInit {

  constructor(
    private loginService: LoginService,
  ) { }

  ngOnInit() {
  }

}
