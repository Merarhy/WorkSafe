export class Group {
    _id: string;
    name: string;
    description: string;
    notifications: any = [{
      sms: Boolean,
      call: Boolean,  
      recording: Boolean,
      email: Boolean
    }];

    configemail: any = {
      head: String,
      message: String,
      response: {},
    };
    configsms: any = {
        message_sms: String,
        response_sms: {},
    }; 
    audio: any;
    _business: String;
    users: [String];
    
    constructor(model: any = null) {
      if ( model._id) {
        this._id = model._id;
      }
      if ( !model || !model.notifications ) {
        this.notifications = {
          sms: false,
          call: false,  
          recording: false, 
          email: false, 
        };
      }

      this.name = model.name;
      this.description = model.description;
      this._business = model._business;
      this.users = model.users;
      this.configemail = model.configemail;
      this.configsms = model.configsms; 
      this.audio = model.audio;
    }
}
