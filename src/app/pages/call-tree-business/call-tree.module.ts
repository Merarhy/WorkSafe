import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';
import { RouterModule, Routes} from '@angular/router';
import { MainCallTreeComponent } from './main-call-tree.component';


@NgModule({
    declarations: [
      MainCallTreeComponent,  

    ],
    imports     : [
     
        SharedModule,
        RouterModule
    ],
    exports     : [
      MainCallTreeComponent,
    ],

})

export class CallTreeModule
{

}