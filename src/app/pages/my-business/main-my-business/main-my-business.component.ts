import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../pages/login/login.service';

@Component({
  selector: 'ms-main-content',
  templateUrl: './main-my-business.component.html',
  styleUrls: ['./main-my-business.component.scss']
})
export class MainMyBusinessComponent implements OnInit {

  constructor(public loginService: LoginService) { }

  ngOnInit() {
  }

}
