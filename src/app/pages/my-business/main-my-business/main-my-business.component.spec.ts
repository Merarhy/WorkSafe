import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainMyBusinessComponent } from './main-my-business.component';

describe('MainMyBusinessComponent', () => {
  let component: MainMyBusinessComponent;
  let fixture: ComponentFixture<MainMyBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainMyBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainMyBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
