export class MyBusiness {
  _id: string;
  name: string;
  acronimo: string;
  calle: string;
  colonia: string;
  municipio: string;
  estado: string;
  pais: string;
  cp: string;
  tel: string;
  logo: string;
  admin: string;
  active: number;
  extradata: any;
  created_at: Date;

  constructor(model: any = null) {
    if(model._id)
      this._id = model._id;
    this.name = model.name;
    this.acronimo = model.acronimo,
    this.calle = model.calle;
    this.colonia = model.colonia;
    this.municipio = model.municipio;
    this.estado = model.estado;
    this.pais = model.pais;
    this.cp = model.cp;
    this.tel = model.tel;
    this.logo = model.logo;
    this.admin = model.admin;
    this.active = model.active;
    this.extradata = model.extradata;
    this.created_at = model.created_at;
  }
}