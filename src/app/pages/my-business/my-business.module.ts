import { NgModule } from '@angular/core';
import { SharedModule } from '../../core/modules/shared.module';

import { MyBusinessComponent } from './my-business.component';
import { RouterModule, Routes} from '@angular/router';
import { IdToRolePipeModule } from '../../pipes/idtorole.module';
import {  FileSizePipes } from '../../pipes/filesizepipe/filesizepipe.module';

import { MyUsersComponent } from './my-users-list/my-users-list.component';
import { FileManagerComponent } from './file-manager/file-manager.component';

// Custom libraries
import { TreeModule } from 'angular-tree-component';
import { MyUserFormComponent } from './my-users-list/my-user-form/my-user-form.component';

@NgModule({
    declarations: [
         MyUsersComponent,
         FileManagerComponent,
         MyBusinessComponent,
         MyUserFormComponent
         
        
    ],
    imports     : [
       IdToRolePipeModule,
        FileSizePipes,
        SharedModule,
        RouterModule,
        TreeModule
        
    ],
    exports     : [
        MyUserFormComponent
    ],
    entryComponents: [MyUserFormComponent]
})

export class MyBusinessModule
{

}
