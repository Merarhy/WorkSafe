import {Component, OnInit, ViewChild, Pipe, TemplateRef, PipeTransform} from '@angular/core';
import { MyUserFormComponent } from './my-user-form/my-user-form.component';
import {MyUser} from '../my-users.model';
import {MyUsersService} from '../my-users.service';
import {MatSnackBar} from '@angular/material';
import * as _ from 'lodash';
import { MatDialog, MatDialogRef } from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {LoginService} from '../../../pages/login/login.service';
import {MyRolesService} from '../my-roles.service';
import {MyRole} from '../my-roles.model';
import { fuseAnimations } from '../../../core/animations';
import { environment } from '../../../../environments/environment';
import { FormControl, FormGroup } from '@angular/forms';
import { MatTableDataSource , MatPaginator, MatSort} from '@angular/material';
import { FuseConfirmDialogComponent } from '../../../core/components/confirm-dialog/confirm-dialog.component';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';


@Component({
  selector: 'fuse-my-users-list',
  templateUrl: './my-users-list.component.html',
  styleUrls: ['./my-users-list.component.scss'],
  providers: [MyUsersService, MyRolesService],
  animations: fuseAnimations
})
export class MyUsersComponent implements OnInit {

  filterBy: String = '';
  searchInput: FormControl;
  
  @ViewChild('dialogContent') dialogContent: TemplateRef<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  displayedColumns = ['firstname', 'email'];
  dataSource = new MatTableDataSource();

  dialogTitle: any;
  dialogRef: any;
  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
  


  users: MyUser[];

  userQuery: String = '';
  edit_mode: Boolean = false;
  edit_user_mode: Boolean = false;
  avatarPath: String = environment.apiUrl + 'avatars/';
  
  current_users: MyUser[];

  current_user: MyUser;
  errorMessage: String = '';
  params: any = {};
  selectedIndex: Number = 0;
  contentLoading: Boolean = true;

  roles: MyRole[];
  
  business: any;

  constructor(
    private usersService: MyUsersService,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private rolesService: MyRolesService,
    private router: Router,
    public dialog: MatDialog,
  ) {
    this.searchInput = new FormControl('');
    this.business = this.loginService.getIdentity().business._id;
  } 

  ngOnInit() {

    this.usersService.getAll().subscribe(
      users => {
        this.users = users.filter(el => el.business === this.business );
        this.dataSource.data = this.users;
        this.setFirstUser(); this.contentLoading = false;
        if (this.params['seccion'] && this.params['seccion'] === 'usuarios') {
          setTimeout( () => {
            this.selectedIndex = 1;
          }, 1000);
          if (this.params['id_usuario']) {
            if ( this.params['id_usuario'] === 'nuevo' ){
              this.newUser();
            } else {
              const user = this.users.find( el => el._id === this.params['id_usuario']);
              this.setActiveUser(user);
            }
          }
        }
      },
      e => { alert('Error loading business data'); }
    );

    this.rolesService.getAll().subscribe(
      roles => { this.roles = roles; this.contentLoading = false;},
      error => { alert('Error loading profiles'); }
    );

  }

  newContact() {
      this.dialogRef = this.dialog.open(MyUserFormComponent, {
          panelClass: 'contact-form-dialog',
          data      : {
              action: 'new',
              roles: this.roles
          }
      });

      this.dialogRef.afterClosed()
          .subscribe((response: any) => {
              if ( !response )
              {
                  return;
              }
              this.saveUser( response );
          });

  }


  editContact(contact) {
      this.dialogRef = this.dialog.open(MyUserFormComponent, {
          panelClass: 'contact-form-dialog',
          data      : {
              contact: contact,
              roles: this.roles,
              action : 'edit'
          }
      });

      this.dialogRef.afterClosed()
          .subscribe(response => {
              if ( !response )
              {
                  return;
              }
              const actionType: string = response[0];
              const formData: any = response[1];
              switch ( actionType )
              {
                  /**
                   * Save
                   */
                  case 'save':
                      this.saveUser( formData );
                      break;
                  /**
                   * Delete
                   */
                  case 'delete':
                      
                      this.deleteUser( formData );
                      break;

                  case 'restorepass':
                      
                      this.restorePassword( formData );
                      break;
                      
              }
          });
  }







  saveUser(userData){
    
    this.usersService
    .saveOrUpdate(userData)
    .subscribe(
      (res) => {
        if ( res.message ){
          this.snackBar.open(res.message, 'Cerrar', {
            duration: 3000
          });
        } else {
          if ( !this.users.find(el => el._id === res._id ) ){
            this.users.push(res);
          } else {
            this.users[this.users.map( el => el._id ).indexOf(userData._id)] = res;
          }
          this.dataSource.data = this.users;
          this.setActiveUser(res);
          this.snackBar.open('El usuario ha sido guardado', 'Cerrar', {
            duration: 3000
          });
        }
      }
    );
  }

  deleteUser(userData){
    if (confirm('¿Estás seguro de borrar el usuario?')){
      this.usersService
          .delete(userData)
          .subscribe(
            (res) => {
              if(res.message){
                this.snackBar.open(res.message, 'Cerrar', {
                  duration: 3000
                });
              } else {
                this.users.splice(this.users.map( el => { return el._id; } ).indexOf(this.current_user._id), 1);
                this.setFirstUser();
                this.snackBar.open('El usuario ha sido eliminado', 'Cerrar', {
                  duration: 3000
                });
              }
            }
          );
    }
  }

  restorePassword(userData){
    if (confirm('¿Enviar correo al usuario para restaurar su contraseña?')){
      this.usersService
      .resetUserBusinessPassword(userData._id)
      .subscribe(
        user => {
          if ( user && user._id ) {
            this.current_user = user;
          } else {
            this.snackBar.open('Notificación enviada', 'Cerrar', {
              duration: 3000
            });
          }
        },
        e =>
        {  this.snackBar.open('Error al enviar la solicitud', 'Cerrar',{
            duration: 3000
          }); 
        }
      );

    }
  }

  usersFiltered(){
    if(!this.userQuery)
      return this.users;
    return this.users.filter( (el) => {
      return ( (el.firstname + ' ' + el.lastname).toLowerCase().indexOf(this.userQuery.toLowerCase())!=-1 || el.email.indexOf(this.userQuery.toLocaleLowerCase())==0)
    });
  }

  setActiveUser(user) {
    this.edit_user_mode = false;
    this.current_user = _.cloneDeep(user);
  }
  setFirstUser(){
      if (this.users.length > 0){
        this.current_user = _.cloneDeep(this.users[0]);
      }else{
        this.newUser();
      }
  }
  newUser(){
    this.current_user = new MyUser({
      firstname: '',
      lastname: '',
      email: '',
      role: ''
    });
    this.editUserMode(true);
  }

  editUserMode(active){
    if (!active && !this.current_user){
      this.setFirstUser();
    }
    this.edit_user_mode = active;
  }

  editMode(active){
    if(!active){
      
    }
    this.edit_mode = active;
    this.edit_user_mode = active;
  }


}
