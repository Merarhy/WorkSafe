import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CalendarEvent } from 'angular-calendar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MyUser } from '../../my-users.model';
import { MyRole } from '../../my-roles.model';
import { LoginService } from '../../../../pages/login/login.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-my-user-form',
  templateUrl: './my-user-form.component.html',
  styleUrls: ['./my-user-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MyUserFormComponent implements OnInit {
  

    avatarPath: String = environment.apiUrl + 'avatars/';

    event: CalendarEvent;
    dialogTitle: string;
    contactForm: FormGroup;
    action: string;
    contact: MyUser = new MyUser({});
    roles: MyRole[];
    business: any;
    constructor(
        public dialogRef: MatDialogRef<MyUserFormComponent>,
        @Inject(MAT_DIALOG_DATA) private data: any,
        private formBuilder: FormBuilder,
        private loginService: LoginService,
    )
    {
        this.roles = data.roles;
        this.action = data.action;
        this.business = this.loginService.getIdentity().business._id;

        if ( this.action === 'edit' )
        {
            this.dialogTitle = `Editar Usuario Empresa`;
            this.contact = data.contact;
        }
        else
        {
            this.dialogTitle = `Nuevo Usuario Empresa`;
            this.contact = new MyUser({});
        }

       
        this.contactForm = this.createContactForm();
    }

    ngOnInit()
    {
    }

    createContactForm()
    {
        
        return this.formBuilder.group({
            _id: [this.contact._id],
            firstname: [this.contact.firstname],
            lastname: [this.contact.lastname],
            email: [this.contact.email],
            phone: [this.contact.phone],
            role: [this.contact.role],
            externo: [this.contact.externo],
            extradata: [this.contact.extradata],
        });
    }

    addExtradata(){
        if ( !this.contact.extradata ) {
            this.contact.extradata = [];
        }
        this.contact.extradata.push({});
    
    }

    removeExtradata(index){
        this.contact.extradata.splice(index, 1);
    }

    tryDeleteUser( userdata ) {
        if ( confirm('¿Estás seguro de borrar el usuario?') ){
            this.dialogRef.close(['delete', userdata ]);
        }
    }

    restorePassword( userdata ){
        if (confirm('¿Enviar correo al usuario para restaurar su contraseña?')){
            this.dialogRef.close(['restorepass', userdata ]);
        }
    }

}
