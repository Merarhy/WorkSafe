import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyUserFormComponent } from './my-user-form.component';

describe('MyUserFormComponent', () => {
  let component: MyUserFormComponent;
  let fixture: ComponentFixture<MyUserFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyUserFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyUserFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
