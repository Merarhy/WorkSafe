import { Injectable } from '@angular/core';
import { MyRole } from './my-roles.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class MyRolesService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    getAll() : Observable<MyRole[]> {
        return this.http.get(`${environment.apiUrl}my-businessroles`, this.requestOptions)
            .map( mapRoles );
    }
    
}

function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toRole);
    } catch (error) {}
    return body || { };
}


function mapRoles(response:Response): MyRole[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toRole);
}


function toRole(r:any): MyRole{
    let role = <MyRole>({
        _id: r._id,
        name: r.name,
        slug: r.slug,
        permissions: r.permissions,
    });
    return role;
}