import { Component, OnInit, ViewEncapsulation,  ViewChild, Pipe, PipeTransform } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import {ActivatedRoute} from '@angular/router';
import {MatSnackBar} from "@angular/material";
import {MyBusiness} from "./my-business.model";
import {MyBusinessService} from './my-business.service';
import {MyUser} from "./my-users.model";
import {MyUsersService} from './my-users.service';
import {LoginService} from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';
import * as _ from 'lodash';
import * as moment from 'moment';
import { fuseAnimations } from '../../core/animations';


// Custom libraries
import { TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions } from 'angular-tree-component';
import { TreeComponent } from 'angular-tree-component';
import { routeAnimation } from "../../route.animation";

@Component({
  selector: 'fuse-main-my-business-content',
  templateUrl: './my-business.component.html',
  styleUrls: ['./my-business.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None,
  providers: [MyBusinessService, MyUsersService, ]
})
export class MyBusinessComponent implements OnInit {

  avatarPath: String = environment.apiUrl + 'avatars/';

  private sub: any;

  business: MyBusiness;
  users: MyUser[];

  current_user : MyUser;
  section: String = 'detalle';
  userQuery: string = '';
  edit_mode: boolean = false;
  edit_user_mode: boolean = false;

  
  current_users : MyUser[];
  console_types : any = {};
  
  errorMessage: string = '';
  url_fileserve: string = '';

  selectedIndex: number = 0;
  contentLoading: boolean = true;

  
  create_new_file: Boolean;
  new_file : any;
  
  params: any = {};

  @ViewChild(TreeComponent)
  private tree: TreeComponent;

  constructor(
    private route: ActivatedRoute,
    private businessService: MyBusinessService,
    private usersService: MyUsersService,
    private snackBar: MatSnackBar,
    private loginService: LoginService,
    private router: Router
    ) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      this.params = params;
    });



    this.businessService.getBusiness().subscribe(
      bus => {this.business = bus;
       this.contentLoading = false;},
      e => { alert('Error loading business data'); }
    );
    
    
    

  }

 

  saveBusiness(){
    this.businessService
    .updateBusiness(this.business)
    .subscribe(
      (res) => {
         // Error, business not stored
        if(res.message){
          this.snackBar.open('Ha ocurrido un error al borrar la carpeta', 'Cerrar', {
            duration: 3000
          });
        } else {
          this.setActiveBusiness();
          this.snackBar.open('La empresa ha sido actualizada','Cerrar',{
            duration:3000
          });
    
        }
      }
    );
  }

  addExtradata(){
    if ( !this.business.extradata ) {
      this.business.extradata = [];
    }
    this.business.extradata.push({});
    
  }

  removeExtradata(index){
    this.business.extradata.splice(index, 1);
  }



  editMode(active){
    if(!active && !this.business){
      
    }
    this.edit_mode = active;
    this.edit_user_mode = active;
  }

  
  
  setActiveBusiness() {
    this.edit_mode = false;
    this.business = _.cloneDeep(this.business);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  navigateFiles(){
  
    this.router.navigate(['my-business/my-files']);
    
  }
}
