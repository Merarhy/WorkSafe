import { Injectable } from '@angular/core';
import { MyBusiness } from './my-business.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class MyBusinessService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }
    getBusiness() : Observable<any> {
        return this.http.get(`${environment.apiUrl}my-business`, this.requestOptions)
            .map( extractData );
    }

    updateBusiness(business:MyBusiness) : Observable<any> {
        return this.http.patch(`${environment.apiUrl}my-business`,business,this.requestOptions).map( extractData );
    }    

    /*
    getAll() : Observable<MyBusiness[]> {
        return this.http.get(`${environment.apiUrl}business`, this.requestOptions)
            .map( mapBusiness );
    }
    
    
    saveOrUpdate(business: MyBusiness) : Observable<any> {
        if(business._id)
            return this.http.patch(`${environment.apiUrl}business/${business._id}`, business, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}business`, business, this.requestOptions).map( extractData );
    }

    
    delete(business: MyBusiness) : Observable<any> {
        if(business._id)
            return this.http.delete(`${environment.apiUrl}business/${business._id}`, this.requestOptions).map( extractData );
    }
    */
    
}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toBusiness);
    } catch (error) {}
    return body || { };
}


function mapBusiness(response:Response): MyBusiness[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toBusiness);
}


function toBusiness(r:any): MyBusiness{
    let business = <MyBusiness>({
        _id: r._id,
        name: r.name,
        acronimo : r.acronimo,
        calle: r.calle,
        colonia: r.colonia,
        municipio: r.municipio,
        estado: r.estado,
        pais: r.pais,
        cp: r.cp,
        tel: r.tel,
        admin: r.admin,
        active: r.active,
        extradata: r.extradata,
          });
    return business;
}