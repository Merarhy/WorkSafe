import { Injectable } from '@angular/core';
import { MyUser } from './my-users.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';

@Injectable()
export class MyUsersService {

    requestOptions: RequestOptions;

    constructor(private http: Http, loginService: LoginService){
        const headers = new Headers({'Authorization': loginService.getToken()});
        this.requestOptions = new RequestOptions({headers: headers});
    }

    getAll(): Observable<MyUser[]> {
        return this.http.get(`${environment.apiUrl}my-businessusers`, this.requestOptions)
            .map( mapUsers );
    }


    getUserBusinnessById(_id: any){
        return this.http.get(`${environment.apiUrl}my-businessusers/users/${_id}`, this.requestOptions)
            .map( mapUsers );
    }    


    saveOrUpdate(user: MyUser): Observable<any> {
        if(user._id)
            return this.http.patch(`${environment.apiUrl}my-businessusers/${user._id}`, user, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}my-businessusers/`, user, this.requestOptions).map( extractData );
    }
    
    delete(user: MyUser): Observable<any> {
        if(user._id)
            return this.http.delete(`${environment.apiUrl}my-businessusers/${user._id}`, this.requestOptions).map( extractData );
    }

    getCurrentUser(): Observable<any> {
        return this.http.get(`${environment.apiUrl}my-business/account`, this.requestOptions)
           .map( extractData );
    }
  
    updateCurrentUserPassword(user: any): Observable<any> {
        return this.http.patch(`${environment.apiUrl}my-business/account/password`, user, this.requestOptions).map( extractData );
    }

    addCurrentUserDevice(user: any): Observable<any> {
        return this.http.post(`${environment.apiUrl}my-business/account/device`, user, this.requestOptions).map( extractData );
    }

    deleteCurrentUserDevice(device_id: string): Observable<any> {
        return this.http.delete(`${environment.apiUrl}my-business/account/device/${device_id}`, this.requestOptions).map( extractData );
    }

    resetUserBusinessPassword(_id: string):Observable<any>{
        return this.http.get(`${environment.apiUrl}/my-businessusers/${_id}/resetpassword`, this.requestOptions).map( extractData );
    }

    getCurrentUserDatacenterRequests(): Observable<any> {
        return this.http.get(`${environment.apiUrl}my-business/account/datacenter-requests`, this.requestOptions).map( extractData );
    }
    addCurrentUserDatacenterRequest(req: any): Observable<any> {
        return this.http.post(`${environment.apiUrl}my-business/account/datacenter-requests`, req, this.requestOptions).map( extractData );
    }
    getUsersDatacenterRequests(): Observable<any> {
        return this.http.get(`${environment.apiUrl}my-business/datacenter-requests`, this.requestOptions).map( extractData );
    }
    authorizeUserDatacenterRequest(req_id: string): Observable<any> {
        return this.http.get(`${environment.apiUrl}my-business/datacenter-requests/${req_id}/authorize`, this.requestOptions).map( extractData );
    }
    rejectUserDatacenterRequest(req_id: string): Observable<any> {
        return this.http.get(`${environment.apiUrl}my-business/datacenter-requests/${req_id}/reject`, this.requestOptions).map( extractData );
    }

}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toUser);
    } catch (error) {}
    return body || { };
}


function mapUsers(response: Response): MyUser[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toUser);
}


function toUser(r: any): MyUser{
    const user = <any>({
        _id: r._id,
        firstname: r.firstname,
        lastname: r.lastname,
        email: r.email,
        phone: r.phone,
        consoles: r.consoles,
        role: r.role,
        externo: r.externo,
        avatar: r.avatar,
        extradata: r.extradata,
        business: r.business
    });
    return user;
}