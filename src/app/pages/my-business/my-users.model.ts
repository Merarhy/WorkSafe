export class MyUser {
  _id: string;
  firstname: string;
  lastname: string;
  email: string;
  phone: string;
  role: string;
  externo: Boolean;
  consoles: string[];
  extradata: any;
  created_at: Date;
  business: any;

  constructor(model: any = null) {
   
    if ( model._id ) {
      this._id = model._id;
    }
    this.firstname = model.firstname;
    this.lastname = model.lastname,
    this.email = model.email;
    this.phone = model.phone;
    this.role = model.role;
    this.externo = model.externo;
    this.consoles = model.consoles;
    this.created_at = model.created_at;
    this.extradata = model.extradata;
    this.business = model.business;
  }

}
