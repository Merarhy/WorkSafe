import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { FileManagerComponent } from './file-manager.component';
import { FileManagerService } from './file-manager.service';
import { IdToRolePipeModule } from '../../../pipes/idtorole.module';
import {  FileSizePipes } from '../../../pipes/filesizepipe/filesizepipe.module';

const routes: Routes = [
    {
        path     : '**',
        component: FileManagerComponent,
        children : [],
        resolve  : {
            files: FileManagerService
        }
    }
];

@NgModule({
    imports     : [
        IdToRolePipeModule,
        FileSizePipes,
        SharedModule,
        RouterModule
    ],
    declarations: [
        FileManagerComponent,
    ],
    providers   : [
        FileManagerService
    ]
})
export class FileManagerModule
{
}
