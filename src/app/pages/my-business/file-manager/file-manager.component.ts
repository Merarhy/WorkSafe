import { Component, OnInit, ViewEncapsulation,  ViewChild, Pipe, PipeTransform } from '@angular/core';
import { MatTableDataSource , MatPaginator, MatSort} from '@angular/material';
import { DataSource } from '@angular/cdk/collections';
import {Router,ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import * as _ from 'lodash';
import {MatSnackBar} from "@angular/material";
import { fuseAnimations } from './../../../core/animations';


import {LoginService} from './../../../pages/login/login.service';
import { DirectoryManagerService  } from './directory-manager.service';
import { FileManagerService } from './file-manager.service';
import {  UsersService } from '../../business/users.service';
import { MyFile } from './file-manager.model';
import { MyDirectory } from './directory-manager.model';


@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.scss'],
  providers: [FileManagerService, DirectoryManagerService],
  encapsulation: ViewEncapsulation.None,
  animations   : fuseAnimations
})
export class FileManagerComponent implements OnInit {
  
  
  dataSource: any;
  displayedColumns = ['icon', 'name', 'type', /*'owner',*/ 'size', 'created', 'detail-button'];
  selected2: any;
  selected: any; // current item selected
  errorMessage: string = '';
  current_files : any;
  current_dirs : any;
  new_folder: MyDirectory = <MyDirectory >({
    name: '',
    _parent: ''
  });
  new_file : any;
  file : any;
  details: boolean = false;
  url_fileserve: string = '';
  filtered: boolean = false;
  current_list: any;

    constructor(
      private fileManagerService: FileManagerService,
      private directoryManagerService: DirectoryManagerService,
      private loginService: LoginService,
      private router: Router,
      private route: ActivatedRoute,
      private snackBar: MatSnackBar,
    )
    {
     
      interface Element {
        name:      string;
        type     : string;
        owner    : string;
        size     : string;
        mine     : string;
        opened   : string;
        created  : string;
      };




    }

    ngOnInit()
    {
      this.dataSource = new MatTableDataSource();
      this._getBusinesFiles();
    }

    _getBusinesFiles() {
      this.directoryManagerService.getAll().subscribe(
        dirs => {
          this.current_dirs = dirs;
          this.fileManagerService.getAll().subscribe(
            files => {
              this.current_files = files;
              this.filteredFiles();
            },
            e => { this.errorMessage = e; }
          );
        },
        e => { this.errorMessage = e; }
      );
    }

    filteredFiles(){
      var dirs = [];
      var files = [];

      if ( this.selected2 ) {

        var parent = [];

        if ( this.selected2._parent ) {
          //console.log("estamos en parent")
          parent.push({
            _id: this.selected2._parent,
            name: '..',
            type: 'folder',
          });

          dirs = parent.concat( this.current_dirs.filter( el => { return el._parent == this.selected2._id  } ) );
         
        }else{
         // console.log("estamos en !parent")
            parent.push({
              _id: this.selected2._parent,
              name: '..',
              type: 'folder',
            });

          dirs = parent.concat( this.current_dirs.filter( el => { return el._parent == this.selected2._id   } ) );
        }
         //dirs[0]
         files = this.current_files.filter( el => { return el._directory == this.selected2._id } );
        
      } else {
         // Get root files & folders
         dirs = this.current_dirs.filter( el => { return !el._parent  } );
         files = this.current_files.filter( el => { return !el._directory } );
      }

      
      this.current_list = dirs.concat(files);
      this.selected = this.current_list
      this.dataSource.data = this.current_list;
     
    }



  /**********
    *
    * Event Click
    * 
    * *********/

   itemDoubleClick(row){
    //console.log("Evento dbclick")
    if ( row.type == 'folder' ) {
      this.selected2 = row;   
      this.filteredFiles();
    }

  }

  itemClick(row){
    //console.log('on select');
    this.details = true;
    this.selected = row;

    if(this.selected){
      var business_id =  this.selected._business
      var file = this.selected.file
      this.url_fileserve= this.fileManagerService.fileServeUrl(business_id,file);
    }
  }
  
  
  newDirectory(){
    this.details = false;
  }


    /**********
    *
    * Function Directory
    * 
    * *********/

    saveDirectory(){
  
      


      if(!this.selected2){
        this.new_folder._parent == '';
      }else{
        if(this.selected2._id){
          this.new_folder._parent = this.selected2._id
        }else{
          this.new_folder._parent == '';
        }
      }

     
      this.directoryManagerService.addDirectory(this.new_folder).subscribe(
        dir => {
          if(dir._id){
              this._getBusinesFiles();
            this.snackBar.open('La carpeta ha sido creada', 'Cerrar', {
              duration: 3000
            });
          } else {
            this.snackBar.open('Ha ocurrido un error al crear la carpeta', 'Cerrar', {
              duration: 3000
            });
          
          }
        },
        e => { this.errorMessage = e;}
      );
 
    }


    removeDirectory(){
      
      if(this.selected.name == '..'){

        alert("Este directorio no se puede eliminar");

      }else{

        if(!confirm('¿Deseas borrar '+this.selected.name+'?'))
        return;
        
        this.directoryManagerService.removeDirectory(this.selected._id).subscribe(
          res => {
            if(res.message){
              this.snackBar.open('Ha ocurrido un error al borrar la carpeta','Cerrar',{
                duration: 3000
              });
            } else {
              this._getBusinesFiles();
            }
          },
          e => { this.errorMessage = e;}
        );
      }
    }


    /**********
    *
    * Function Files
    * 
    * *********/



    saveFile(event: EventTarget) {
      
      let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
      let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
      let files: FileList = target.files;
      this.file = files[0];
    
       
      if(this.selected2){
        if(this.selected2._id){
        var directory_id =  this.selected2._id
        }else{
          directory_id = 0;
        }
      }else{
        directory_id = 0;
      }

      
      if(this.file.type == 'image/jpeg' || this.file.type == 'image/png' || this.file.type == 'application/pdf'){

      
        this.fileManagerService.addFile(directory_id, this.file).subscribe(
          fi => {
            if(fi._id){ 
              this._getBusinesFiles();
              this.snackBar.open('Se ha subido el archivo correctamente', 'Cerrar', {
                duration: 3000
              });
            } else {
              this.snackBar.open('Ha ocurrido un error al subir el archivo', 'Cerrar', {
                duration: 3000
              });
            }
          },
          e => { this.errorMessage = e;  console.log(e);}
        );

      }else{
        this.snackBar.open('Formato invalido', 'Cerrar', {
          duration: 3000
        });
      }
     
      
    }

   

    removeFile(file){
      if(!confirm('¿Deseas eliminar el archivo?')){
        return;
      }
      this.fileManagerService.delete(file.file).subscribe(
        fi => {
          if(!fi.message){
            this._getBusinesFiles();
          } else {
            this.snackBar.open(fi.message, 'Cerrar', {
              duration: 3000
            });
          }
        },
        e => { this.errorMessage = e;}
      );
    }

    


    /**********
    *
    * Filter Data
    * 
    * *********/


    showSearch(){

      if(this.filtered == false){
            this.filtered = true;
      }else{
        this.filtered = false;
      }

    }

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }






}

    
