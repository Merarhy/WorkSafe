import { Injectable } from '@angular/core';
import { Device } from '../../business/device.model';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginService } from '../../../pages/login/login.service';
import { environment } from '../../../../environments/environment';

@Injectable()
export class MyDevicesService {

    requestOptions : RequestOptions;

    constructor(private http : Http, loginService : LoginService){
        let headers = new Headers({'Authorization':loginService.getToken()});
        this.requestOptions = new RequestOptions({headers : headers});
    }

    getDevices(): Observable<Object> {
        return this.http.get(`${environment.apiUrl}my-business/devices`, this.requestOptions)
            .map( extractData );
    }

    saveOrUpdate(device: Device) : Observable<any> {
        if(device._id)
            return this.http.patch(`${environment.apiUrl}my-business/devices/${device._id}`, device, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}my-business/devices/`, device, this.requestOptions).map( extractData );
    }

    /*
    getAll() : Observable<MyBusiness[]> {
        return this.http.get(`${environment.apiUrl}business`, this.requestOptions)
            .map( mapBusiness );
    }
    
    
    saveOrUpdate(business: MyBusiness) : Observable<any> {
        if(business._id)
            return this.http.patch(`${environment.apiUrl}business/${business._id}`, business, this.requestOptions).map( extractData );
        else
            return this.http.post(`${environment.apiUrl}business`, business, this.requestOptions).map( extractData );
    }

    
    delete(business: MyBusiness) : Observable<any> {
        if(business._id)
            return this.http.delete(`${environment.apiUrl}business/${business._id}`, this.requestOptions).map( extractData );
    }
    */
    
}


function extractData(res: Response): Object {
    let body = res.json();
    try {
        body = body.map(toDevice);
    } catch (error) {}
    return body || { };
}


function mapDevice(response:Response): Device[]{
    // The response of the API has a results
    // property with the actual results
    return response.json().data.map(toDevice);
}


function toDevice(r:any): Device{
    var device = new Device(r);
    return device;
}