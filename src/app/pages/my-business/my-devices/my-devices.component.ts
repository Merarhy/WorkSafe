import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from "@angular/material";
import {Device} from '../../business/device.model';
import {MyDevicesService} from './my-devices.service';
//import {SettingsCatalogsService} from '../../settings/settings-catalogs/settings-catalogs.service';
import {LoginService} from '../../../pages/login/login.service';

import * as moment from 'moment';

@Component({
  selector: 'ms-my-devices',
  templateUrl: './my-devices.component.html',
  styleUrls: ['./my-devices.component.scss'],
  providers: [MyDevicesService]
})
export class MyDevicesComponent implements OnInit {

  deviceTypes: any;
  devices: any;
  current_device: Device;
  current_device_type: any;
  device_details: Device;

  edit_mode: Boolean = false;

  constructor(
    private snackBar: MatSnackBar,
    private myDevicesService: MyDevicesService,
   // private settingsCatalogsService: SettingsCatalogsService,
    public loginService: LoginService
  ) { }

  ngOnInit() {
    // Get catalogs
  /*  this.settingsCatalogsService.getCollection('infra', 'Devicetype').subscribe(
      deviceTypes => {
        this.deviceTypes = deviceTypes;
        this.setFirstDevice();
      },
      err => { console.log(err); }
    );
*/
    this.myDevicesService.getDevices().subscribe(
      devices => {
        if (devices && devices['data']) {
          this.devices = devices['data'];
        }
      },
      err => { console.log(err); }
    );

  }

  setFirstDevice() {
    this.current_device_type = this.deviceTypes[0];
  }

  setActiveDeviceType(dt) {
    this.device_details = undefined;
    this.current_device_type = dt;
  }

  filteredDevices() {
    if ( !this.devices || !this.current_device_type ) {
      return [];
    }
    return this.devices.filter( el => el._devicetype === this.current_device_type._id );
  }

  newDevice() {
    this.current_device = new Device;
    this.editMode(true);
  }
  editMode(edit) {
    this.device_details = undefined;
    this.edit_mode = edit;
  }

  saveDevice() {
    this.myDevicesService.saveOrUpdate(this.current_device).subscribe(
      device => {
        if (device && device._id) {
          this.devices.push(device);
          this.edit_mode = false;
          this.setActiveDeviceType(this.deviceTypes.find( el => el._id === device._devicetype ));
          this.snackBar.open('El dispositivo ha sido guardado.', 'Cerrar', {
            duration: 3000
          });
        }
      },
      err => {
        this.snackBar.open('Ha ocurrido un error al guardar el dispositivo: (' + err.error + ')', 'Cerrar', {
          duration: 3000
        });
      }
    );
  }

  showDevice(device) {
    this.device_details = device;
  }

  humanDate( currentDate ) {
    return moment( currentDate ).format( 'D MMM YYYY');
  }

}
