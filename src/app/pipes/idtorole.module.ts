import { NgModule } from "@angular/core"

import { IdToRolePipe  } from './idtorole';

@NgModule({
  declarations: [IdToRolePipe],
  exports: [IdToRolePipe],
  imports: []
})
export class IdToRolePipeModule {
}