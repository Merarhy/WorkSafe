import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 |  exponentialStrength:10}}
 *   formats to: 1024
*/
@Pipe({name: 'idToRole'})
export class IdToRolePipe implements PipeTransform {
  transform(value: string, roles: any): string {
    if(!value || typeof(roles)=='undefined') return '';
    let role = roles.find(r => { return r._id == value; });
    return (!role || !role['name']) ? 'Usuario sin perfil' : role['name'];
  }
}