import { NgModule } from "@angular/core"
import { FileSizePipe } from './filepipe';

@NgModule({
  declarations: [
    //...
    FileSizePipe,
    
  ],
  exports: [FileSizePipe],
  imports: []
  
})
export class FileSizePipes {}