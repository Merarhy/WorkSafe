import { Component } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { FuseConfigService } from '../../core/services/config.service';
import { TranslateService } from '@ngx-translate/core';
import {LoginService} from '../../pages/login/login.service';
import { environment } from '../../../environments/environment';
@Component({
    selector   : 'fuse-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls  : ['./toolbar.component.scss']
})

export class FuseToolbarComponent
{
    user =  this.loginService.identity;
    avatarPath: String = environment.apiUrl + 'avatars/';
    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService,
        private loginService: LoginService,
        private translate: TranslateService
    ){}
    
    logout() {
        this.loginService.doLogout();
      }
  

}
