import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FuseMainComponent } from './main/main.component';
import { AuthGuard } from './auth.guard';
// pages
import { LoginComponent } from './pages/login/login.component';
import { ResetPasswordComponent } from './pages/login/reset-password/reset-password.component';
import { UsersComponent} from './pages/users/users.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RolesUsersComponent } from './pages/users/roles-users/roles-users.component';
import { FuseProjectComponent } from './pages/dashboard/project.component';
import { ForgotPasswordComponent } from './pages/login/forgot-password/forgot-password.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { KioskoCatalogoIdComponent } from './pages/kiosko/kiosko-catalogo-id/kiosko-catalogo-id.component';



import { KioskoComponent } from './pages/kiosko/kiosko.component';
import { CatalogoSiteIdComponent } from './pages/kiosko/catalogo-site-details/catalogo-site-id.component';

const routes: Routes = [
    {
        path: '',
        component: LoginComponent,
    },
    {
        path: 'reset-password/:token',
        component: ResetPasswordComponent
    },
    {
        path: 'forgot-password',
        component: ForgotPasswordComponent
    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard] 
    },
    {
        path: 'admin',
        component: FuseProjectComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'kiosko',
        component: KioskoComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'users',
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                component: UsersComponent,
            },
            {
                path: 'roles',
                component: RolesUsersComponent,
            }
        ]
    },
    
   {
    path: 'kiosko/catalogo/:id',
    component: KioskoCatalogoIdComponent,
    canActivate: [AuthGuard]
   },
    {
        path: 'catalogo/site/:id', 
        component: CatalogoSiteIdComponent,
        canActivate: [AuthGuard]
    }
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [AuthGuard]
  })
export class RoutingModule { }
