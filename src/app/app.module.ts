import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import 'hammerjs';

import { SharedModule } from './core/modules/shared.module';
import { AppComponent } from './app.component';
import { FuseMainModule } from './main/main.module';
import { FuseSplashScreenService } from './core/services/splash-screen.service';
import { FuseConfigService } from './core/services/config.service';
import { FuseNavigationService } from './core/components/navigation/navigation.service';
import { TranslateModule } from '@ngx-translate/core';
import { LoadingOverlayComponent } from './core/components/loading-overlay/loading-overlay.component';
// pages
import { LoginModule } from './pages/login/login.module';
import { ResetPasswordModule } from './pages/login/reset-password/reset-password.module';
import { ProfileModule } from './pages/profile/profile.module';


import { UsersModule } from './pages/users/users.module';
import { FuseProjectDashboardModule } from './pages/dashboard/project.module';
import { RolesUsersModule } from './pages/users/roles-users/roles-users.module';



import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, 
    MatSortModule, MatTableModule } from '@angular/material';

// Custom services
import { LoginService } from './pages/login/login.service';

import { RoutingModule } from './app-routing.module';
import { IdToRolePipeModule } from './pipes/idtorole.module';
import { WebCamModule } from 'ack-angular-webcam';

// Example Files
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FuseFakeDbService } from './fuse-fake-db/fuse-fake-db.service';
import { KioskoComponent } from './pages/kiosko/kiosko.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { KioskoCatalogoIdComponent } from './pages/kiosko/kiosko-catalogo-id/kiosko-catalogo-id.component';
import { CatalogoSiteIdComponent } from './pages/kiosko/catalogo-site-details/catalogo-site-id.component';
import { AgmCoreModule } from '@agm/core';

import { CatalogComponent } from './pages/settings/catalog/catalog.component';
import { KpiComponent } from './pages/settings/kpi/kpi.component';


@NgModule({
    declarations: [
        AppComponent,
        LoadingOverlayComponent,
        KioskoComponent,
        SettingsComponent,
        KioskoCatalogoIdComponent,
        CatalogoSiteIdComponent,
        CatalogComponent,
        KpiComponent,
    ],
    imports     : [
        BrowserModule,
        HttpClientModule,
        HttpModule,
        FormsModule,
        BrowserAnimationsModule,
        SharedModule,
        TranslateModule.forRoot(),
        FuseMainModule,
        LoginModule,
        ResetPasswordModule,
        UsersModule,
        ProfileModule,
        RoutingModule,
        FuseProjectDashboardModule,
        RolesUsersModule,
        IdToRolePipeModule,
        WebCamModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyDC2w9eLTnkF1f4mycaRQEeO-VdgtJAQhc'
        }),
        
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        InMemoryWebApiModule.forRoot(FuseFakeDbService, {
            delay             : 0,
            passThruUnknownUrl: true
        }),
        
    ],
    providers   : [
        FuseSplashScreenService,
        FuseConfigService,
        FuseNavigationService,
        LoginService,
        
        
    ],
    bootstrap   : [
        AppComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule
{
}
